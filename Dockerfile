FROM node:carbon-alpine
WORKDIR /app
COPY compiler/lexer/package.json .
RUN ["npm", "install"]
COPY compiler/lexer .
CMD node lexer