'use strict'
const { doLexicalAnalysis } = require('./lexer')
const { inputContent, writeOutput } = require('./config')
const { printClassificationTable, defaultColumns } = require('../common/printClassificationTable')
try {
  console.time('Tempo total')
  const result = doLexicalAnalysis(inputContent)
  console.timeEnd('Tempo total')
  console.log(`\nLexer Result:\n\n`)
  delete defaultColumns[5]
  delete defaultColumns[4]
  printClassificationTable(result, defaultColumns)
  writeOutput(result)
} catch (e) {
  console.error(e)
}
