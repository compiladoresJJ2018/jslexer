'use strict'
// Implementação do filtro ASCII (níveis 2, 3 e 4 da abstração).
/** @typedef {import('../common/constants').CompilerClassification} CompilerClassification */
const { ASCII_CLASSIFICATIONS } = require('../common/constants')

const isAsciiControl = charCode => charCode < 0x20 || charCode === 0x7F
const isAsciiDelimiter = charCode => charCode === 0x20
const isAsciiSpecial = (charCode) => {
  return (charCode > 0x20 && charCode < 0x30) ||
         (charCode > 0x39 && charCode < 0x41) ||
         (charCode > 0x5A && charCode < 0x61) ||
         (charCode > 0x7A && charCode < 0x7F)
}
const isAsciiDigit = charCode => charCode > 0x2F && charCode < 0x3A
const isAsciiLetter = (charCode) => {
  return (charCode > 0x40 && charCode < 0x5B) || // Uppercase alphabet
         (charCode > 0x60 && charCode < 0x7B) || // Lowercase alphabet
         (charCode > 0x7F && charCode < 0x100) // Extended ASCII
}

/**
 * Mantém apenas os caracteres ASCII usados pelo BASIC. Classifica os caracteres em controle,
 * letra, digito, especial ou delimitador. Organizado em uma etapa por sugestão prática dos slides.
 * Mantem tracking da linha e da coluna original para fins de error handling.
 * @param {String} text
 * @return {CompilerClassification[]}
 */
const filterAndCategorizeASCII = (text) => {
  /** @type {CompilerClassification[]} */
  const result = []
  const { control, delimiter, special, digit, letter } = ASCII_CLASSIFICATIONS
  const normalizedText = text.normalize('NFC') // Javascript usa UC2: Normalização para caracteres ambíguos
  const lines = normalizedText.split('\n')
  for (let i = 0; i < lines.length; i++) {
    let column = 1
    let line = i + 1
    for (const symbol of lines[i]) {
      const charCode = symbol.charCodeAt(0)
      if (isAsciiControl(charCode)) {
        result.push({ classification: control, equivalentClassifications: [control], symbol, line, column })
      } else if (isAsciiDelimiter(charCode)) {
        result.push({ classification: delimiter, equivalentClassifications: [delimiter], symbol, line, column })
      } else if (isAsciiSpecial(charCode)) {
        result.push({ classification: special, equivalentClassifications: [special], symbol, line, column })
      } else if (isAsciiDigit(charCode)) {
        result.push({ classification: digit, equivalentClassifications: [digit], symbol, line, column })
      } else if (isAsciiLetter(charCode)) {
        result.push({ classification: letter, equivalentClassifications: [letter], symbol, line, column })
      }
      column++
    }
    result.push({ classification: control, equivalentClassifications: [control], symbol: '\u{A}', line, column }) // Adiciona nova linha
  }
  return result
}

module.exports = { filterAndCategorizeASCII }
