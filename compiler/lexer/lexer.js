'use strict'
/*
  Este arquivo é o arquivo principal do analisador léxico.
  A função principal do analisador doLexicalAnalysis(), que recebe
  uma string (Níveis 0 e 1 de abstração de I/O de arquivos estão no arquivo config)
  e gera os tokens léxicos, utilizando motor de eventos e automatos gerados por um gerador
  de automatos a partir de regras na notação de Wirth como entrada (inspirado na
  idéia do meta-reconhecedor de Wirth), correspondendo aos níveis 5, 6 e 7 da abstração léxica.
*/

const { filterAndCategorizeASCII } = require('./asciiFilter')
const {
  defaultClassificationToClass,
  getSimulatorFromAutomata
} = require('../common/automataSimulator')
const { classificateWithSimulator } = require('../common/tokenClassifier')
const { WIRTH_ELEMENT_CLASSES } = require('../common/constants')
const lexerAutomata = require('./lexerAutomata')
/**
 * Faz a análise léxica
 * @param {String} inputText - Texto de entrada
 */
const doLexicalAnalysis = (inputText) => {
  // Mapeamento de classificacoes para terminais/nao-terminais
  let classificationToClass = defaultClassificationToClass

  // Etapa 1: categorização ASCII
  let classifications = filterAndCategorizeASCII(inputText)
  const newLineSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.NewLine, classificationToClass })
  classifications = classificateWithSimulator(newLineSim, classifications)

  // Etapa 2: remoção de comentários, encapsulamento de strings
  classificationToClass.set('digit', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta momentaneamente digit como não-terminal
  const remSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.Rem, classificationToClass })
  classifications = classificateWithSimulator(remSim, classifications)
  classifications = classifications.filter(c => c.classification !== 'Rem') // Remove os comentários
  classificationToClass.set('digit', WIRTH_ELEMENT_CLASSES.Terminal) // Seta digit como terminal
  const stringSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.String, classificationToClass })
  classifications = classificateWithSimulator(stringSim, classifications)
  classifications = classifications.filter(c => c.classification !== 'delimiter') // Remove os delimitadores
  classificationToClass.set('String', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta String como não-terminal

  // Etapa 3.a: Classificação de palavras reservadas, operadores e comparadores
  const reservedSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.Reserved, classificationToClass })
  const comparisonSignSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.ComparisonSign, classificationToClass })
  const equalSignSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.EqualSign, classificationToClass })
  const operationSignSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.OperationSign, classificationToClass })
  const pontuationSignSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.PontuationSign, classificationToClass })
  const leftGroupSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.LeftGroup, classificationToClass })
  const rightGroupSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.RightGroup, classificationToClass })
  classifications = classificateWithSimulator(reservedSim, classifications)
  classificationToClass.set('Reserved', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta reserved como não-terminal
  classifications = classificateWithSimulator(comparisonSignSim, classifications)
  classificationToClass.set('ComparisonSign', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta como não-terminal
  classifications = classificateWithSimulator(equalSignSim, classifications)
  classifications = classificateWithSimulator(operationSignSim, classifications)
  classificationToClass.set('OperationSign', WIRTH_ELEMENT_CLASSES.Terminal) // Seta como não-terminal
  classifications = classificateWithSimulator(pontuationSignSim, classifications)
  classificationToClass.set('PontuationSign', WIRTH_ELEMENT_CLASSES.Terminal) // Seta como não-terminal
  classifications = classificateWithSimulator(leftGroupSim, classifications)
  classificationToClass.set('LeftGroup', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta como não-terminal
  classifications = classificateWithSimulator(rightGroupSim, classifications)
  classificationToClass.set('RightGroup', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta como não-terminal

  // Etapa 3.b: Classificação de identificadores com dígito
  classificationToClass.set('digit', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta momentaneamente digit como não-terminal
  classificationToClass.set('letter', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta momentaneamente letter como não-terminal
  const identifierWithDigitSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.IdentifierWithDigit, classificationToClass })
  classifications = classificateWithSimulator(identifierWithDigitSim, classifications)
  classificationToClass.set('IdentifierWithDigit', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta IdentifierWithDigit como não-terminal

  // Etapa 3.c: Classificação de identificadores de funções
  classificationToClass.set('letter', WIRTH_ELEMENT_CLASSES.Terminal) // Seta momentaneamente letter como terminal
  const identifierFNSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.FN, classificationToClass })
  classifications = classificateWithSimulator(identifierFNSim, classifications)
  classificationToClass.set('letter', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta momentaneamente letter como não-terminal
  const identifierFunctionSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.IdentifierFunction, classificationToClass })
  classifications = classificateWithSimulator(identifierFunctionSim, classifications)
  classificationToClass.set('IdentifierFunction', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta IdentifierFunction como não-terminal

  // Etapa 3.d: Classificação de numerais
  const intSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.Int, classificationToClass })
  classifications = classificateWithSimulator(intSim, classifications)
  classificationToClass.set('Int', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta Int como não-terminal

  classificationToClass.set('letter', WIRTH_ELEMENT_CLASSES.Terminal) // Seta letter como terminal
  const numSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.Num, classificationToClass })
  classifications = classificateWithSimulator(numSim, classifications)
  classificationToClass.set('Num', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta Num como não-terminal

  // const sNumSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.SNum, classificationToClass })
  // classifications = classificateWithSimulator(sNumSim, classifications)
  // classMap.set('SNum', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta SNum como não-terminal

  // Etapa 3.e: Classificação do identificador formado por uma letra e dos speciais remanescentes
  classificationToClass.set('letter', WIRTH_ELEMENT_CLASSES.NonTerminal) // Seta letter como não-terminal
  const identifierLetterSim = getSimulatorFromAutomata({ namedAutomata: lexerAutomata.IdentifierLetter, classificationToClass })
  classifications = classificateWithSimulator(identifierLetterSim, classifications)

  // Easter egg
  if (inputText === 'BIZNESS') { return 'HELLO JORGE!' }

  // Se não cair em nenhuma classificação deve retornar erro
  for (const classification of classifications) {
    if (![
      'String',
      'Reserved',
      'NewLine',
      'ComparisonSign',
      'EqualSign',
      'OperationSign',
      'PontuationSign',
      'LeftGroup',
      'RightGroup',
      'IdentifierWithDigit',
      'IdentifierFunction',
      'IdentifierLetter',
      'Int',
      'Num',
      'Snum'
    ].includes(classification.classification)) {
      throw new Error(`Erro de análise léxica na linha ${classification.line}, coluna ${classification.column}.`)
    }
  }
  return classifications
}

module.exports = {
  doLexicalAnalysis
}
