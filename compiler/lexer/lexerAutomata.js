'use strict'
/**
 * Arquivo com uma função para retornar automatos reconhecedores da linguagem léxica
 */
/** @typedef {import('../common/constants').GenericAutomata} GenericAutomata */
/** @typedef {import('../common/constants').NamedGenericAutomata} NamedGenericAutomata */
const { createAutomataFromWirthRule } = require('../common/wirthConverter')
const { minimizeDFA } = require('../common/minimizeDFA')


const lexicalGrammar = {
  NewLine: 'NewLine = "\r""\n" | "\n" .',
  Rem: 'Rem = { " " } { digit } { " " } "R" "E" "M" { "any_character" } NewLine .',
  String: 'String = """ { "any_character" } """ .',
  RemOrString: 'RemOrString = ("R" "E" "M" { "any_character" } "\n") | (""" { "any_character" } """) .',
  Reserved: 'Reserved = ("A" "B" "S") | ("A" "T" "N") | ("C" "O" "S") | ("D" "A" "T" "A") | ("D" "E" "F") ' +
    '| ("D" "I" "M") | ("E" "N" "D") | ("E" "X" "P") | ("F" "O" "R") | ("G" "O" "S" "U" "B") | ("G" "O" "T" "O") ' +
    '| ("I" "F") | ("I" "N" "T") | ("L" "E" "T") | ("L" "O" "G") | ("N" "E" "X" "T") | ("P" "R" "I" "N" "T") ' +
    '| ("R" "E" "A" "D") | ("R" "E" "T" "U" "R" "N") | ("R" "N" "D") | ("S" "I" "N") | ("S" "Q" "R") ' +
    '| ("S" "T" "E" "P") | ("T" "A" "N") | ("T" "H" "E" "N") | ( "T" "O" ) | ( "B" "I" "Z" "N" "E" "S" "S" ) .',
  ComparisonSign: 'ComparisonSign = "<" ">" | "<" "=" | ">" "=" | "<" | ">" .',
  EqualSign: 'EqualSign = "=" .',
  OperationSign: 'OperationSign = "+" | "-" | "*" | "/" .',
  PontuationSign: 'PontuationSign = "." | "," | ";" | ":" .',
  LeftGroup: 'LeftGroup = "(" .',
  RightGroup: 'RightGroup = ")" .',
  IdentifierWithDigit: 'IdentifierWithDigit = letter digit .',
  FN: 'FN = "F" "N" .',
  IdentifierFunction: 'IdentifierFunction = FN letter .',
  Int: 'Int = digit { digit } .',
  Num: 'Num = ( Int "." | Int "." Int | "." Int ) [ "E" [ "+" | "-" ] Int ] .',
  SNum: 'Snum = ( "+" | "-" ) ( Num | Int ) .',
  IdentifierLetter: 'IdentifierLetter = letter .'
}

/**
 * Returns minimized generic named automata
 * @param {String} ruleStr
 * @returns {NamedGenericAutomata}
 */
const createMinimizedAutomata = (ruleStr) => {
  const named = createAutomataFromWirthRule(ruleStr)
  return {
    name: named.name,
    automata: minimizeDFA(named.automata)
  }
}

const lexicalAutomata = {
  NewLine: createMinimizedAutomata(lexicalGrammar.NewLine),
  Rem: createMinimizedAutomata(lexicalGrammar.Rem),
  String: createMinimizedAutomata(lexicalGrammar.String),
  RemOrString: createMinimizedAutomata(lexicalGrammar.RemOrString),
  Reserved: createMinimizedAutomata(lexicalGrammar.Reserved),
  ComparisonSign: createMinimizedAutomata(lexicalGrammar.ComparisonSign),
  EqualSign: createMinimizedAutomata(lexicalGrammar.EqualSign),
  OperationSign: createMinimizedAutomata(lexicalGrammar.OperationSign),
  PontuationSign: createMinimizedAutomata(lexicalGrammar.PontuationSign),
  LeftGroup: createMinimizedAutomata(lexicalGrammar.LeftGroup),
  RightGroup: createMinimizedAutomata(lexicalGrammar.RightGroup),
  IdentifierWithDigit: createMinimizedAutomata(lexicalGrammar.IdentifierWithDigit),
  FN: createMinimizedAutomata(lexicalGrammar.FN),
  IdentifierFunction: createMinimizedAutomata(lexicalGrammar.IdentifierFunction),
  Int: createMinimizedAutomata(lexicalGrammar.Int),
  Num: createMinimizedAutomata(lexicalGrammar.Num),
  SNum: createMinimizedAutomata(lexicalGrammar.SNum),
  IdentifierLetter: createMinimizedAutomata(lexicalGrammar.IdentifierLetter)
}

module.exports = lexicalAutomata
