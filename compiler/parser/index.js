
'use strict'
const { doSintaticalAnalysis, checkErrors } = require('./parser')
const { inputContent, writeOutput, test } = require('./config')
const { printClassificationTable, defaultColumns } = require('../common/printClassificationTable')
try {
  console.time('Tempo total')
  const result = doSintaticalAnalysis(inputContent)
  if (!test) checkErrors(result)
  console.timeEnd('Tempo total')
  console.log(`\nParser Result:\n\n`)
  if (!test) {
    delete defaultColumns[4]
    delete defaultColumns[3]
  }
  printClassificationTable(result, defaultColumns)
  writeOutput(result)
} catch (e) {
  console.error(e)
}
