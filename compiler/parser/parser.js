'use strict'
/*
  Este arquivo é o arquivo principal do analisador sintático.
*/
/** @typedef {import('../common/constants').CompilerClassification} CompilerClassification */

const { doLexicalAnalysis } = require('../lexer/lexer')
const {
  defaultClassificationToClass,
  getSimulatorFromAutomata
} = require('../common/automataSimulator')
const { classificateWithSimulator, recategorizeTopClassification } = require('../common/tokenClassifier')
const { WIRTH_ELEMENT_CLASSES } = require('../common/constants')
const parserAutomata = require('./parserAutomata')

/**
 * Faz uma verificação de erros
 * @param {CompilerClassification[]} classifications
 */
const checkErrors = (classifications) => {
  const incompleteStatements = classifications.filter(c => c.classification !== 'BStatement')
  if (incompleteStatements.length > 0) {
    let unexpectedToken = incompleteStatements[0]
    const otherTokens = incompleteStatements.filter(t => t.classification !== 'Int')
    if (otherTokens.length > 0) {
      unexpectedToken = otherTokens[0]
    }
    throw new Error(`Parsing error: Unexpected token ${unexpectedToken.symbol} at line ${unexpectedToken.line}, column ${unexpectedToken.column}`)
  }

  const endStatements = classifications.filter(c => c.children[1].classification === 'End')
  if (endStatements.length === 0) {
    throw new Error('Parsing error: Missing End statement.')
  } else if (endStatements.length === 1) {
    // if (classifications[classifications.length - 1] !== endStatements[0]) {
    //   throw new Error(`Parsing error: End statement at ${endStatements[0].line}:${endStatements[0].children[1].column} must be the last one.`)
    // }
  } else if (endStatements.length > 1) {
    throw new Error(`Parsing error: Found multiple End statements at ${endStatements[1].line}:${endStatements[1].children[1].column}.`)
  }
}

/**
 * Faz a análise sintatica
 * @param {String} inputText - Texto de entrada
 * @return {CompilerClassification[]}
 */
const doSintaticalAnalysis = (inputText) => {
  // Mapeamento de classificacoes para terminais/nao-terminais
  let classificationToClass = defaultClassificationToClass

  // Categorização léxica
  let classifications = doLexicalAnalysis(inputText)

  classificationToClass.set('Reserved', WIRTH_ELEMENT_CLASSES.Terminal)
  classificationToClass.set('PontuationSign', WIRTH_ELEMENT_CLASSES.Terminal)
  classificationToClass.set('OperationSign', WIRTH_ELEMENT_CLASSES.Terminal)

  const dimSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Dim, classificationToClass })
  const functionHeaderSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.FunctionHeader, classificationToClass })

  const predefSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Predef, classificationToClass })
  const gotoSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Goto, classificationToClass })
  const gosubSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Gosub, classificationToClass })
  const dataSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Data, classificationToClass })
  const returnSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Return, classificationToClass })
  const endSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.End, classificationToClass })

  const varSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Var, dependencyAutomatas: [ parserAutomata.Exp, parserAutomata.Eb ], classificationToClass })
  const expSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Exp, dependencyAutomatas: [ parserAutomata.Eb, parserAutomata.Var ], classificationToClass })

  const assignSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Assign, classificationToClass })
  const readSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Read, classificationToClass })

  const ifSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.If, classificationToClass })
  const forSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.For, classificationToClass })
  const nextSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Next, classificationToClass })
  const defSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Def, classificationToClass })

  const pitemSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Pitem, classificationToClass })
  const printSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Print, classificationToClass })
  const biznessSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.Bizness, classificationToClass })

  const bStatementSim = getSimulatorFromAutomata({ namedAutomata: parserAutomata.BStatement, classificationToClass })

  // Classificação de Dim, Predef, Gosub, Goto, Data, Return, FunctionHeader (classificação temporária para desambiguação referente a Exp) e End
  classifications = classificateWithSimulator(dimSim, classifications)
  classifications = classificateWithSimulator(functionHeaderSim, classifications)
  classifications = classificateWithSimulator(predefSim, classifications)
  classifications = classificateWithSimulator(gotoSim, classifications)
  classifications = classificateWithSimulator(gosubSim, classifications)
  classifications = classificateWithSimulator(dataSim, classifications)
  classifications = classificateWithSimulator(returnSim, classifications)
  classifications = classificateWithSimulator(endSim, classifications)

  // Classificação de expressões aritméticas
  classifications = classificateWithSimulator(varSim, classifications)
  classifications = classificateWithSimulator(expSim, classifications)
  for (let i = classifications.length - 1; i > -1; i--) {
    const c = classifications[i]
    if (c.classification === 'OperationSign') {
      throw new Error(`Unexpected token ${c.symbol} at line ${c.line}, column ${c.column}`)
    }
  }
  for (let i = classifications.length - 1; i > -1; i--) {
    const c = classifications[i]
    if (c.classification === 'LeftGroup') {
      throw new Error(`Unexpected token ${c.symbol} at line ${c.line}, column ${c.column}`)
    }
    if (c.classification === 'RightGroup') {
      throw new Error(`Unexpected token ${c.symbol} at line ${c.line}, column ${c.column}`)
    }
  }
  // classifications.map(c => console.log(c.classification))
  classifications = recategorizeTopClassification(classifications)

  // Classificação de Assign, Read
  classifications = classificateWithSimulator(assignSim, classifications)
  classifications = classificateWithSimulator(readSim, classifications)

  // Classificação de If, For, Next e Def
  classifications = classificateWithSimulator(ifSim, classifications)
  classifications = classificateWithSimulator(forSim, classifications)
  classifications = classificateWithSimulator(nextSim, classifications)
  classifications = classificateWithSimulator(defSim, classifications)

  for (let i = classifications.length - 1; i > -1; i--) {
    const c = classifications[i]
    if (['ComparisonSign', 'EqualSign'].includes(c.classification)) {
      throw new Error(`Unexpected token ${c.symbol} at line ${c.line}, column ${c.column}`)
    }
  }
  // Classificação de Pitem, Print
  classifications = classificateWithSimulator(pitemSim, classifications)
  classifications = classificateWithSimulator(printSim, classifications)
  classifications = classificateWithSimulator(biznessSim, classifications)
  classifications = recategorizeTopClassification(classifications)

  // Classificação de BStatement
  classifications = classificateWithSimulator(bStatementSim, classifications)

  classifications = classifications.filter(c => c.classification !== 'NewLine')

  return classifications
}

module.exports = {
  doSintaticalAnalysis,
  checkErrors
}
