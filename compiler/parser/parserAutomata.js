'use strict'
/**
 * Arquivo com uma função para retornar automatos reconhecedores da linguagem sintática
 */
/** @typedef {import('../common/constants').GenericAutomata} GenericAutomata */
/** @typedef {import('../common/constants').NamedGenericAutomata} NamedGenericAutomata */
const { createAutomataFromWirthRule } = require('../common/wirthConverter')
const { minimizeDFA } = require('../common/minimizeDFA')

const primitives = {
  Eb: 'LeftGroup Exp RightGroup | Int | Num | Var | ( IdentifierFunction | Predef ) LeftGroup Exp RightGroup',
  Var: 'IdentifierWithDigit | IdentifierLetter [ LeftGroup Exp { "," Exp } RightGroup ]',
  Exp: '[ "+" | "-" ] Eb { ( OperationSign ) Eb }'.replace(/OperationSign/gu, ' ( "+" | "-" | "*" | "/" ) ')
}

const parserGrammar = {
  // Comandos e classificações independentes
  Dim: 'Dim = "DIM" IdentifierLetter LeftGroup Int { "," Int } RightGroup { "," IdentifierLetter LeftGroup Int { "," Int } RightGroup } .',
  FunctionHeader: 'FunctionHeader = "DEF" IdentifierFunction LeftGroup ( IdentifierWithDigit | IdentifierLetter ) RightGroup .',
  Predef: 'Predef = "SIN" | "COS" | "TAN" | "ATN" | "EXP" | "ABS" | "LOG" | "SQR" | "INT" | "RND" .',
  Goto: 'Goto = "GOTO" Int .',
  Gosub: 'Gosub = "GOSUB" Int .',
  Next: 'Next = "NEXT" ( IdentifierWithDigit | IdentifierLetter ) .',
  Data: 'Data = "DATA" ( [ "+" | "-" ] ( Num | Int ) ) { "," ( [ "+" | "-" ] ( Num | Int ) ) } .',
  Return: 'Return = "RETURN" .',
  End: 'End = "END" .',
  Exp: 'Exp = ' + primitives.Exp + ' .',
  Var: 'Var = ' + primitives.Var + ' .',
  Eb: 'Eb = ' + primitives.Eb + ' .',
  // Program = BStatement { BStatement } Int "END" .
  Assign: 'Assign = "LET" Var EqualSign Exp .',
  Read: 'Read = "READ" Var { "," Var } .',
  If: 'If = "IF" Exp ( ComparisonSign | EqualSign ) Exp "THEN" Int .'.replace(/Exp/gu, '( Exp | Int | Num | Var )'),
  For: 'For = "FOR" ( IdentifierWithDigit | IdentifierLetter ) EqualSign Exp "TO" Exp [ "STEP" Exp ] .'.replace(/Exp/gu, '( Exp | Int | Num | Var )'),
  Def: 'Def = FunctionHeader EqualSign Exp .'.replace(/Exp/gu, '( Exp | Int | Num | Var )'),
  Pitem: 'Pitem = Exp | String [ Exp ] .'.replace(/Exp/gu, '( Exp | Int | Num | Var )'),
  Print: 'Print = "PRINT" [ Pitem { "," Pitem } [ "," ] ] .',
  Bizness: 'Bizness = "BIZNESS" Pitem .',
  BStatement: 'BStatement = Int ( Assign | Read | Data | Print | Goto | If | For | Next | Dim | Def | Gosub | Return | End | Bizness ) .'

}

/**
 * Returns minimized generic named automata
 * @param {String} ruleStr
 * @returns {NamedGenericAutomata}
 */
const createMinimizedAutomata = (ruleStr) => {
  const named = createAutomataFromWirthRule(ruleStr)
  return {
    name: named.name,
    automata: minimizeDFA(named.automata)
  }
}

const parserAutomata = {
  Dim: createMinimizedAutomata(parserGrammar.Dim),
  FunctionHeader: createMinimizedAutomata(parserGrammar.FunctionHeader),
  Predef: createMinimizedAutomata(parserGrammar.Predef),
  Goto: createMinimizedAutomata(parserGrammar.Goto),
  Gosub: createMinimizedAutomata(parserGrammar.Gosub),
  Next: createMinimizedAutomata(parserGrammar.Next),
  Data: createMinimizedAutomata(parserGrammar.Data),
  Return: createMinimizedAutomata(parserGrammar.Return),
  End: createMinimizedAutomata(parserGrammar.End),
  Exp: createMinimizedAutomata(parserGrammar.Exp),
  Eb: createMinimizedAutomata(parserGrammar.Eb),
  Var: createMinimizedAutomata(parserGrammar.Var),
  Assign: createMinimizedAutomata(parserGrammar.Assign),
  Read: createMinimizedAutomata(parserGrammar.Read),
  If: createMinimizedAutomata(parserGrammar.If),
  For: createMinimizedAutomata(parserGrammar.For),
  Def: createMinimizedAutomata(parserGrammar.Def),
  Pitem: createMinimizedAutomata(parserGrammar.Pitem),
  Print: createMinimizedAutomata(parserGrammar.Print),
  Bizness: createMinimizedAutomata(parserGrammar.Bizness),
  BStatement: createMinimizedAutomata(parserGrammar.BStatement)
}

module.exports = parserAutomata
