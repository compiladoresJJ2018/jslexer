'use strict'
/*
  A fim de não gerar um arquivo de código muito grande, o conversor de Wirth foi dividido
  em 4 arquivos principais, separados por funcionalidade.

  - wirthAutomataSimplifier.js: Contém coleção de funções para unir subautomatos em um único
    converter NDFAs para DFAs.
*/
/**
 * @typedef {import('./constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').NamedGenericAutomata} NamedGenericAutomata
 */

const { WIRTH_ELEMENT_CLASSES, EMPTY_SYMBOL } = require('./constants')
const { convertNDFAtoDFA } = require('./NDFAToDFA')
const { minimizeDFA } = require('./minimizeDFA')

const addOffset = (str, offset) => (Number(str) + offset) + ''

/**
 * Replace state names with some offset value to add
 * @param {GenericAutomata} automata
 * @param {Number} offset
 * @returns {GenericAutomata}
 */
const replaceStateNames = (automata, offset) => {
  /**
   * @type {GenericAutomata}
  */
  const newAutomata = {
    alphabet: new Set([...automata.alphabet]),
    initialState: addOffset(automata.initialState, offset),
    finalStates: new Set([...automata.finalStates].map(s => addOffset(s, offset))),
    states: new Set([...automata.states].map(s => addOffset(s, offset))),
    transitions: new Map(),
    isDFA: automata.isDFA,
    isMinimized: automata.isMinimized
  }
  for (const stateTransitions of automata.transitions) {
    const newStateTransitions = new Map()
    for (const transition of stateTransitions[1]) {
      newStateTransitions.set(transition[0], new Set([...transition[1]].map(ns => addOffset(ns, offset))))
    }
    newAutomata.transitions.set(addOffset(stateTransitions[0], offset), newStateTransitions)
  }
  return newAutomata
}

/**
 * Returns new DFA from father and son combined, replacing some nt ocorrence.
 * If no substitution is needs to be made, returns undefined.
 * If only one NT is need to be replaced removes the NT from the alphabet
 * Supposes both father and son DFA for simplier code.
 * @param {GenericAutomata} father
 * @param {GenericAutomata} son
 * @param {String} nt
 * @returns {GenericAutomata}
 */
const replaceFirstNTWithAutomata = (father, son, nt) => {
  if (!father.alphabet.has(nt)) return undefined
  for (const fatherStateTransitions of father.transitions) {
    for (const fatherTransition of fatherStateTransitions[1]) {
      if (fatherTransition[0] === nt) {
        let offset = 0
        for (const fatherState of father.states) {
          if (Number(fatherState) >= offset) offset = Number(fatherState) + 1
        }

        const newSon = replaceStateNames(son, offset)
        /**
         * @type {GenericAutomata}
         */
        const newAutomata = {
          alphabet: new Set([...father.alphabet, ...newSon.alphabet]),
          initialState: father.initialState,
          finalStates: new Set(father.finalStates),
          states: new Set([...father.states, ...newSon.states]),
          transitions: new Map([
            ...father.transitions,
            ...newSon.transitions
          ]),
          isDFA: false,
          isMinimized: false
        }

        // Generates empty transition from father to son
        newAutomata.transitions.get(fatherStateTransitions[0]).set(EMPTY_SYMBOL, new Set([newSon.initialState]))

        // Generates empty transitions for each final states of son to father toStates of the NT original transition
        newSon.finalStates.forEach(s => {
          if (!newAutomata.transitions.get(s)) {
            newAutomata.transitions.set(s, new Map())
          }
          newAutomata.transitions.get(s).set(EMPTY_SYMBOL, new Set(fatherTransition[1]))
        })

        // Removes the father original transition to son on new automata
        newAutomata.transitions.get(fatherStateTransitions[0]).delete(fatherTransition[0])

        // Removes the alphabet nt if no nt is left
        let stillHaveNT = false
        for (const transition of newAutomata.transitions) {
          if (transition[1].has(nt)) {
            stillHaveNT = true
            break
          }
        }
        if (!stillHaveNT) newAutomata.alphabet.delete(nt)

        // Convert to dfa
        return minimizeDFA(convertNDFAtoDFA(newAutomata))
      }
    }
  }

  return undefined
}

/**
 * Replaces all nt ocurrences
 * @param {GenericAutomata} father
 * @param {GenericAutomata} son
 * @param {String} nt
 * @returns {GenericAutomata}
 */
const replaceAllNtOcurrences = (father, son, nt) => {
  let automata = father
  while (automata) {
    const changedAutomata = replaceFirstNTWithAutomata(automata, son, nt)
    if (changedAutomata) {
      automata = changedAutomata
    } else break
  }
  return automata
}

/**
 * Replaces a list of named automatas
 * @param {GenericAutomata} father
 * @param {NamedGenericAutomata[]} nts
 * @param {GenericAutomata}
 */
const replaceListOfNt = (father, nts) => {
  let i = 0
  let automata = father
  while (i < nts.length) {
    automata = replaceAllNtOcurrences(automata, nts[i].automata, JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: nts[i].name }))
    i += 1
  }
  return automata
}

module.exports = {
  replaceStateNames,
  replaceFirstNTWithAutomata,
  replaceListOfNt
}
