/**
 * CLI (Command Line Interface) para o gerar arvores de classificacoes.
 * Salva a arvore no formato .gv (https://en.wikipedia.org/wiki/DOT_(graph_description_language))
 *  Tais diagramas, podem por exemplo, ser gerados no website: https://graphs.grevian.org/graph
 * Le um arquivo relativo a pasta partials, e gera na pasta trees
 */
/** @typedef {import('./constants').CompilerClassification} CompilerClassification */

const path = require('path')
const fs = require('fs')
const { download } = require('./graphDownloader')
/**
 * Converte uma classificacao em notação DOT
 * @param {CompilerClassification} classification
 * @param {Number} prefix
 * @returns {String}
 */
const getChildrenDotStatesAndTransitions = (classification, fatherName, resultStr = '') => {
  if (!classification.children) return ''
  for (let i = 0; i < classification.children.length; i++) {
    let c = classification.children[i]
    let childName = `${fatherName}:${i}`
    if (!['special', 'letter', 'digit', 'control', 'Reserved'].includes(c.classification)) {
      resultStr += `  "${childName}"  [label="${c.symbol}"] [shape=box]\n`
      resultStr += `  "${fatherName}" -> "${childName}"\n`
    }
    if ([
      'SimpleUnsignedDivExp', 'SimpleSignedDivExp', 'SimpleUnsignedMultExp',
      'SimpleSignedMultExp', 'Snum', 'SimpleSignedPredefExp', 'SimplePredefExp',
      'SimpleClosedSignedExp', 'SimpleClosedUnsignedExp'
    ].includes(c.classification) && c.children) {
      resultStr += getChildrenDotStatesAndTransitions(c, childName)
    }
  }
  return resultStr
}

/**
 * Converte uma classificacao em notação DOT
 * @param {CompilerClassification[]} classifications
 * @returns {String}
 */
const convertClassificationTreeToDotNotation = (classifications) => {
  let labelRoot = classifications.map(c => c.symbol).join('').replace(/\n/gu, '\\n')
  if (labelRoot.length > 20) labelRoot = 'root'
  let resultStr = `digraph dfa {
  "classificationArray" [label="${labelRoot}"] [shape=box]
`
  for (let i = 0; i < classifications.length; i++) {
    let c = classifications[i]
    resultStr += `  "${i}" [label="${c.symbol.replace(/\n/gu, '\\n')}"] [shape=box]\n`
    resultStr += getChildrenDotStatesAndTransitions(c, `${i}`)
  }

  for (let i = 0; i < classifications.length; i++) {
    resultStr += `  "classificationArray" -> "${i}"\n`
  }
  resultStr += '}'
  return resultStr
}

const saveContentToFile = (content, file) => {
  try {
    fs.writeFileSync(file, content)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

const loadContentFromFile = (file) => {
  try {
    return fs.readFileSync(file, { encoding: 'utf8' })
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

if (require.main === module) {
  const generate = async () => {
    let mainPath = path.dirname(require.main.filename)

    let inputFile = process.env.CLASSIFICATIONS_FILE || process.argv[2]

    if (!inputFile) {
      console.error(new Error('Nenhum arquivo de classificação foi fornecido'))
      process.exit(1)
    }

    let inputContent
    try {
      const rawContent = fs.readFileSync(path.join(mainPath, '..', 'partials/', inputFile), 'utf8')
      inputContent = JSON.parse(rawContent)
    } catch (err) {
      console.error(err)
      process.exit(1)
    }

    console.log('#### ClassificationsToTreeCLI: CLI para gerar diagramas para árvores de classificações ####')
    console.log(`Path       : ${mainPath}`)
    console.log(`Input file : ${inputFile}`)

    const treesFolder = path.join(mainPath, '..', 'trees')
    if (!fs.existsSync(treesFolder)) {
      fs.mkdirSync(treesFolder)
    }

    let dotDiagram = convertClassificationTreeToDotNotation(inputContent)
    const dotDiagramFile = path.join(treesFolder, `${inputFile}.gz`)
    saveContentToFile(dotDiagram, dotDiagramFile)
    dotDiagram = loadContentFromFile(dotDiagramFile)
    const imageContent = await download(dotDiagram)
    const imageDiagramFile = path.join(treesFolder, `${inputFile}.png`)
    saveContentToFile(imageContent, imageDiagramFile)
  }
  generate()
}

module.exports = {
  convertClassificationTreeToDotNotation
}
