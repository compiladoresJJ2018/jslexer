'use strict'
/*
  A fim de não gerar um arquivo de código muito grande, o conversor de Wirth foi dividido
  em 4 arquivos principais, separados por funcionalidade.

  - wirthBlockConverter.js: Contém uma coleção de funções responsáveis por gerar automatos
  para blocos simples, sejam estes blocos:
    - Bloco simples com apenas concatenação de não-terminais ou terminais (input wirth array);
    - Bloco simples com apenas alternância de não-terminais ou terminais (input wirth array);
    - Bloco no formato [ S ], ou seja um subautomato presente ou omitido (input subautomato);
    - Bloco no formato ( S ), ou seja um subautomato presente (input subautomato);
    - Bloco no formato { S }, ou seja um subautomato 0 ou mais vezes (input subautomato).
*/
/**
 * @typedef {import('./constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 */

const { EMPTY_SYMBOL } = require('./constants')
const { convertNDFAtoDFA } = require('./NDFAToDFA')

/**
 * Create automata from sequential concatenation group. (Eg.: "x" y "z" k)
 * @param {WirthElement[]} elements
 * @returns {GenericAutomata}
 */
const createAutomataFromConcatenationGroup = elements => {
  return {
    alphabet: new Set(elements.map(e => JSON.stringify({ class: e.class, value: e.value }))),
    initialState: '0',
    finalStates: new Set([elements.length + '']),
    states: new Set([...Array(elements.length + 1).keys()].map(k => k + '')),
    transitions: new Map(elements.map((e, i) => [i + '', new Map([[JSON.stringify({ class: e.class, value: e.value }), new Set([i + 1 + ''])]])])),
    isDFA: true,
    isMinimized: false
  }
}

/**
 * Create automata from alternation group (Eg.: "x" | y | "z" | k )
 * Obs.: The OrBar must not be present.
 * @param {WirthElement[]} elements
 * @returns {GenericAutomata}
 */
const createAutomataFromAlternationGroup = elements => {
  return {
    alphabet: new Set(elements.map(e => JSON.stringify({ class: e.class, value: e.value }))),
    initialState: '0',
    finalStates: new Set(['1']),
    states: new Set(['0', '1']),
    transitions: new Map([
      ['0', new Map(elements.map(e => [JSON.stringify({ class: e.class, value: e.value }), new Set(['1'])]))]
    ]),
    isDFA: true,
    isMinimized: false
  }
}

/**
 * Handle properly a top-level automata originated from
 * selection group "( S )"
 * @param {GenericAutomata} automata
 * @returns {GenericAutomata}
 */
const handleAutomataFromSelectionGroup = automata => {
  return automata
}

/**
 * Returns the next valid state
 * @param {Set<String>} states
 * @returns {String}
 */
const getNextValidState = states => {
  let index = 0
  states.forEach(s => {
    let asNumber = Number(s)
    if (asNumber >= index) index = asNumber + 1
  })
  return index + ''
}

/**
 * Creates new final state and adds empty transitions
 * from initial to new final, and from ex-finals to new final.
 * @param {GenericAutomata} automata
 * @returns {String} new final state
 */
const createNewFinalState = automata => {
// Creates new final state
  const nextState = getNextValidState(automata.states)
  automata.states.add(nextState)

  // Creates empty transition from initial to new state
  if (!automata.transitions.get(automata.initialState).has(EMPTY_SYMBOL)) {
    automata.transitions.get(automata.initialState).set(EMPTY_SYMBOL, new Set())
  }
  automata.transitions.get(automata.initialState).get(EMPTY_SYMBOL).add(nextState)

  // Adds empty transition from each final to new final state
  for (const finalState of automata.finalStates) {
    if (!automata.transitions.has(finalState)) automata.transitions.set(finalState, new Map())
    if (!automata.transitions.get(finalState).has(EMPTY_SYMBOL)) automata.transitions.get(finalState).set(EMPTY_SYMBOL, new Set())
    automata.transitions.get(finalState).get(EMPTY_SYMBOL).add(nextState)
  }

  return nextState
}

/**
 * Handle properly a top-level automata originated from
 * an optional group "[ S ]" (It changes the automata).
 * Returns a DFA.
 * @param {GenericAutomata} automata
 * @returns {GenericAutomata}
 */
const handleAutomataFromOptionalGroup = automata => {
  // Creates new final state adding empty transitions from initial and ex-finals to the new one
  const nextState = createNewFinalState(automata)

  // The new final is the only final state
  automata.finalStates = new Set([nextState])

  automata.isDFA = false
  return convertNDFAtoDFA(automata)
}

/**
 * Handle properly a top-level automata originated from
 * an optional group "{ S }" (It changes the automata).
 * Turns it into a NDFA.
 * @param {GenericAutomata} automata
 * @returns {GenericAutomata}
 */
const handleAutomataFromRepetitionGroup = automata => {
  // Creates new final state adding empty transitions from initial and ex-finals to the new one
  const nextState = createNewFinalState(automata)

  // Adds empty transition from final state to initial state
  automata.transitions.set(nextState, new Map())
  automata.transitions.get(nextState).set(EMPTY_SYMBOL, new Set())
  automata.transitions.get(nextState).get(EMPTY_SYMBOL).add(automata.initialState)

  // The new final is the only final state
  automata.finalStates = new Set([nextState])

  automata.isDFA = false
  return convertNDFAtoDFA(automata)
}

module.exports = {
  createAutomataFromAlternationGroup,
  createAutomataFromConcatenationGroup,
  handleAutomataFromSelectionGroup,
  handleAutomataFromOptionalGroup,
  handleAutomataFromRepetitionGroup
}
