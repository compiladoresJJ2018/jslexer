'use strict'
/**
 * @typedef {import('../constants').GenericAutomata} GenericAutomata
 */

const {
  replaceStateNames,
  replaceFirstNTWithAutomata
} = require('../wirthAutomataSimplifier')
const {
  // prettyPrintAutomata,
  serializeAutomata
} = require('../automataUtils')
const { WIRTH_ELEMENT_CLASSES } = require('../constants')

const { expect } = require('chai')

describe('/wirthAutomataSimplifier', () => {
  describe('#replaceStateNames', () => {
    it('Should add properly offset', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'c' })
      const symbolZ = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'z' })
      const automata = {
        alphabet: new Set([symbolA, symbolB]),
        initialState: '0',
        finalStates: new Set(['1']),
        states: new Set(['0', '1']),
        transitions: new Map([
          ['0', new Map([[symbolA, new Set(['1'])], [symbolB, new Set(['1'])], [symbolZ, new Set(['1'])], [symbolC, new Set(['1'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const newAutomata = replaceStateNames(automata, 7)
      const serializedAutomata = serializeAutomata(newAutomata)

      expect(serializedAutomata).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        'initialState': '7',
        'finalStates': [
          '8'
        ],
        'states': [
          '7',
          '8'
        ],
        'transitions': [
          {
            'fromState': '7',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [ '8' ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [ '8' ]
              },
              {
                'consumes': '{"class":"Terminal","value":"z"}',
                'toStates': [ '8' ]
              },
              {
                'consumes': '{"class":"Terminal","value":"c"}',
                'toStates': [ '8' ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': false
      })
    })
  })

  describe('#replaceFirstNTWithAutomata', () => {
    it('Should replace properly the automata', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'c' })
      const symbolZ = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'z' })
      const symbolM = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'm' })
      const symbolN = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'n' })
      const symbolK = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'k' })

      const father = {
        alphabet: new Set([symbolA, symbolB, symbolC, symbolZ]),
        initialState: '0',
        finalStates: new Set(['3']),
        states: new Set(['0', '1', '3', '2']),
        transitions: new Map([
          ['0', new Map([[symbolA, new Set(['1'])]])],
          ['1', new Map([[symbolB, new Set(['2'])], [symbolZ, new Set(['0'])]])],
          ['2', new Map([[symbolC, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const son = {
        alphabet: new Set([symbolM, symbolN, symbolK]),
        initialState: '0',
        finalStates: new Set(['1', '3']),
        states: new Set(['0', '1', '2', '3']),
        transitions: new Map([
          ['0', new Map([[symbolM, new Set(['1'])]])],
          ['1', new Map([[symbolN, new Set(['2'])]])],
          ['2', new Map([[symbolN, new Set(['2'])], [symbolK, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const newAutomata = replaceFirstNTWithAutomata(father, son, symbolB)
      const serializedAutomata = serializeAutomata(newAutomata)
      const expectedAutomata = {
        alphabet: [
          '{"class":"Terminal","value":"a"}',
          '{"class":"Terminal","value":"c"}',
          '{"class":"Terminal","value":"z"}',
          '{"class":"Terminal","value":"m"}',
          '{"class":"Terminal","value":"n"}',
          '{"class":"Terminal","value":"k"}'
        ],
        initialState: '0',
        finalStates: [ '3' ],
        states: [ '0', '1', '2', '3', '4', '5' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"a"}',
                toStates: [ '1' ]
              }
            ]
          },
          {
            fromState: '1',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"z"}',
                toStates: [ '0' ]
              },
              {
                consumes: '{"class":"Terminal","value":"m"}',
                toStates: [ '2' ]
              }
            ]
          },
          {
            fromState: '2',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"c"}',
                toStates: [ '3' ]
              },
              {
                consumes: '{"class":"Terminal","value":"n"}',
                toStates: [ '4' ]
              }
            ]
          },
          {
            fromState: '4',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"n"}',
                toStates: [ '4' ]
              },
              {
                consumes: '{"class":"Terminal","value":"k"}',
                toStates: [ '5' ]
              }
            ]
          },
          {
            fromState: '5',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"c"}',
                toStates: [ '3' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: true
      }
      expect(serializedAutomata).to.be.deep.equal(expectedAutomata)
    })

    it('Should return undefined', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'c' })
      const symbolZ = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'z' })
      const symbolM = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'm' })
      const symbolN = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'n' })
      const symbolK = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'k' })

      const father = {
        alphabet: new Set([symbolA, symbolB, symbolC, symbolZ]),
        initialState: '0',
        finalStates: new Set(['3']),
        states: new Set(['0', '1', '3', '2']),
        transitions: new Map([
          ['0', new Map([[symbolA, new Set(['1'])]])],
          ['1', new Map([[symbolB, new Set(['2'])], [symbolZ, new Set(['0'])]])],
          ['2', new Map([[symbolC, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const son = {
        alphabet: new Set([symbolM, symbolN, symbolK]),
        initialState: '0',
        finalStates: new Set(['1', '3']),
        states: new Set(['0', '1', '2', '3']),
        transitions: new Map([
          ['0', new Map([[symbolM, new Set(['1'])]])],
          ['1', new Map([[symbolN, new Set(['2'])]])],
          ['2', new Map([[symbolN, new Set(['2'])], [symbolK, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }
      const newAutomata = replaceFirstNTWithAutomata(father, son, 'uuu')
      expect(newAutomata).to.be.equal(undefined)
    })
  })
})
