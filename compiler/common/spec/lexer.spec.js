'use strict'

const { doLexicalAnalysis } = require('../lexer')

const { expect } = require('chai')

describe('/lexer', () => {
  it('Should handle the BIZNESS instruction correctly', () => {
    const result = doLexicalAnalysis('BIZNESS')
    expect(result).to.be.equal('HELLO JORGE!')
  })

  it('Should give some array (remove this test)', () => {
    const result = doLexicalAnalysis(`1 N = 6 : GOSUB 4\n2 PRINT Ná\u{1F4A9}`)
    expect(result.length).to.be.equal(20)
  })
})
