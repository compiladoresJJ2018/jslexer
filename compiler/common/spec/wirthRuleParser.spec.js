'use strict'

const {
  createRuleArray,
  getNTDoubleQuotePlaceholder,
  extractNameAndRule } = require('../wirthRuleParser')
// const leftQuote = '“'
// const rightQuote = '”'

const { expect } = require('chai')

describe('/wirthRuleParser', () => {
  describe('#getNTDoubleQuotePlaceholder', () => {
    it('Should give properly placeholder', () => {
      const placeholder = getNTDoubleQuotePlaceholder('abcdef\u{D8}ghi')
      expect(placeholder).to.be.equal('\u{D9}')
    })
  })

  describe('#extractNameAndRule', () => {
    it('Should extract name and rule properly for input \'ruleName = a | b [ c "=" ] {"k"} .\'', () => {
      const { name, rule } = extractNameAndRule('ruleName = a | b [ c "=" ] {"k"} .')
      expect(name).to.be.equal('ruleName')
      expect(rule).to.be.equal('a | b [ c "=" ] {"k"}')
    })

    it('Should extract name and rule properly for input \'ruleName=.', () => {
      const { name, rule } = extractNameAndRule('ruleName=.')
      expect(name).to.be.equal('ruleName')
      expect(rule).to.be.equal('')
    })

    it('Should throw error when \'=\' is missing', (done) => {
      try {
        extractNameAndRule('ruleName.')
        done(new Error('Should give error but didn\'t'))
      } catch (e) {
        done()
      }
    })

    it('Should throw error when \'.\' is missing', (done) => {
      try {
        extractNameAndRule('ruleName=')
        done(new Error('Should give error but didn\'t'))
      } catch (e) {
        done()
      }
    })
  })

  describe('#createRuleArray', () => {
    it('Should throw error when rule has invalid number of double quotes', (done) => {
      try {
        createRuleArray('Assign = "LET Var """ Exp .')
        done(new Error('Expected error'))
      } catch (e) {
        done()
      }
    })
    it('Should generate properly pre rule arrays', () => {
      const rules = [
        // 'Program = BStatement { BStatement } int "END" .',
        'BStatement = int ( Assign | Read | Data | Print | Goto | If | For | Next | Dim | Def | Gosub | Return | Remark ) .',
        'Assign = "LET" Var "=" Exp .',
        // 'Var = letter digit | letter [ "(" Exp { "," Exp } ")" ] .',
        // 'Exp = { "+" | "-" } Eb { ( "+" | "-" | "*" | "/" | "Y" ) Eb } .',
        // 'Eb = "(" Exp ")" | Num | Var | ( "FN" letter | Predef ) "(" Exp ")" .',
        // 'Predef = "SIN" | "COS" | "TAN" | "ATN" | "EXP" | "ABS" | "LOG" | "SQR" | "INT" | "RND" .',
        // 'Read = "READ" Var { "," Var } . Int = digit { digit } .',
        'Data = "DATA" Snum { "," Snum } . ',
        // 'Print = "PRINT" [ Pitem { "," Pitem } [ "," ] ]. [ "E" [ "+" | "-" ] Int ] .',
        // 'Pitem = Exp | """ Character { Character } """ [ Exp ] .',
        // 'Goto = ( "GOTO" | "GO" "TO" ) int .
        // 'If = "IF" Exp ( ">=" | ">" | "<>" | "<" | "<=" | "=" ) Exp "THEN" int .'',
        // 'For = "FOR" letter [ digit ] "=" Exp "TO" Exp [ "STEP" Exp ] .',
        // 'Next = "NEXT" letter [ digit ] .',
        // 'Dim = "DIM" letter "(" int { "," int } ")" { "," letter "(" int { "," int } ")" } .',
        // 'Def = "DEF FN" letter "(" letter [ digit ] ")" "=" Exp .',
        // 'Gosub = "GOSUB" int .',
        // 'Return = "RETURN" .',
        // 'Remark = "REM" { Character } .',
        // 'Int = digit { digit } .',
        // 'Num = ( Int [ "." { digit } ] | "." Int ) [ "E" [ "+" | "-" ] Int ] .',
        // 'Snum = [ "+" | "-" ] Num .',
        // 'Character = letter | digit | special .',
        // 'Lextext= { Int | Num | Snum | Character | letter | digit | special | identifier | reserved | composed } .',
        // 'letter = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U"| "V" | "W" | "X" | "Y" | "Z" | "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z" .',
        // 'digit = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9" .',
        'special = "!" | "@" | "#" | "%" | " ̈" | "&" | "*" | "(" | ")" | "_" | "+" | "-" | "=" | "§" | "{" | "[" | "a" | "}" | "]" | "o" | "?" | "/" | "°" | "`" | "~" | "^" | "~" | "<" | "," | ">" | "." | ":" | ";" | "|" | "\\" | """ | """ .'
        // 'identifier = letter [ digit ] .',
        // 'reserved = "ABS" | "ATN" | "COS" | "DATA" | "DEF" | "DIM" | "END" | "EXP" | "FN" | "FOR" | "GO" | "GOSUB" | "GOTO" | "IF" | "INT" | "LET" | "LOG" | "NEXT" | "PRINT" | "READ" | "REM" | "RETURN" | "RND" | "SIN" | "SQR" | "STEP" | "TAN" | "THEN" | "TO" .',
        // 'composed = "FN" letter | "GO" "TO" | "DEF" "FN" letter | ">" "=" | "<" | ">" | "<" "=" | Snum | "\\"" Character { Character } "\\"" | Remark .',
      ]

      const expectResults = [
        [ { value: 'int', class: 'NonTerminal' },
          { value: '(', class: 'LeftParentheses' },
          { value: 'Assign', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Read', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Data', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Print', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Goto', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'If', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'For', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Next', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Dim', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Def', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Gosub', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Return', class: 'NonTerminal' },
          { value: '|', class: 'OrBar' },
          { value: 'Remark', class: 'NonTerminal' },
          { value: ')', class: 'RightParentheses' } ],
        [ { value: 'LET', class: 'Terminal' },
          { value: 'Var', class: 'NonTerminal' },
          { value: '=', class: 'Terminal' },
          { value: 'Exp', class: 'NonTerminal' } ],
        [ { value: 'DATA', class: 'Terminal' },
          { value: 'Snum', class: 'NonTerminal' },
          { value: '{', class: 'LeftCurlyBracket' },
          { value: ',', class: 'Terminal' },
          { value: 'Snum', class: 'NonTerminal' },
          { value: '}', class: 'RightCurlyBracket' } ],
        [ { value: '!', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '@', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '#', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '%', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ' ̈', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '&', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '*', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '(', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ')', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '_', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '+', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '-', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '=', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '§', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '{', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '[', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: 'a', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '}', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ']', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: 'o', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '?', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '/', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '°', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '`', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '~', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '^', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '~', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '<', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ',', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '>', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '.', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ':', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: ';', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '|', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '\\', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '"', class: 'Terminal' },
          { value: '|', class: 'OrBar' },
          { value: '"', class: 'Terminal' } ]
      ]
      for (let i = 0; i < rules.length; i++) {
        const { ruleArray } = createRuleArray(rules[i])
        expect(ruleArray).to.be.deep.equal(expectResults[i])
      }
    })
  })
})
