'use strict'

const {
  findInnerGroup,
  splitIntoConcatenationGroups,
  createAutomataFromWirthRule
} = require('../wirthConverter')
const {
  // prettyPrintAutomata,
  serializeAutomata
} = require('../automataUtils')

const { expect } = require('chai')

describe('/wirthConverter', () => {
  describe('#findInnerGroup', () => {
    it('Should get innermost group in nested ', () => {
      const innerGroupResult = findInnerGroup([
        { value: '{', class: 'LeftCurlyBracket' },
        { value: 'Example', class: 'Terminal' },
        { value: 'Snum', class: 'NonTerminal' },
        { value: '{', class: 'LeftCurlyBracket' },
        { value: '(', class: 'LeftParentheses' },
        { value: ',', class: 'Terminal' },
        { value: ')', class: 'RightParentheses' },
        { value: '[', class: 'LeftSquareBracket' },
        { value: 'a', class: 'Terminal' },
        { value: ']', class: 'RightSquareBracket' },
        { value: 'Snum', class: 'NonTerminal' },
        { value: '}', class: 'RightCurlyBracket' },
        { value: '}', class: 'RightCurlyBracket' }
      ])
      expect(innerGroupResult).to.be.deep.equal({ begin: 4, end: 6, type: 'ParenthesesGroup' })
    })

    it('Should return undefined when no group is left', () => {
      const innerGroupResult = findInnerGroup([
        { value: 'Example', class: 'Terminal' },
        { value: 'Snum', class: 'NonTerminal' },
        { value: ',', class: 'Terminal' },
        { value: 'a', class: 'Terminal' },
        { value: 'Snum', class: 'NonTerminal' }
      ])
      expect(innerGroupResult).to.be.equal(undefined)
    })
  })

  describe('#splitIntoConcatenationGroups', () => {
    it('Should split properly concatenation groups', () => {
      const concatGroups = splitIntoConcatenationGroups([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' },
        { value: 'c', class: 'Terminal' },
        { value: '|', class: 'OrBar' },
        { value: 'z', class: 'Terminal' },
        { value: '|', class: 'OrBar' },
        { value: 'k', class: 'NonTerminal' },
        { value: 'p', class: 'Terminal' },
        { value: '|', class: 'OrBar' },
        { value: 'y', class: 'NonTerminal' },
        { value: 'e', class: 'Terminal' },
        { value: 's', class: 'Terminal' }
      ])

      const expectedGroups = [
        [ { value: 'a', class: 'Terminal' },
          { value: 'b', class: 'NonTerminal' },
          { value: 'c', class: 'Terminal' } ],
        [ { value: 'z', class: 'Terminal' } ],
        [ { value: 'k', class: 'NonTerminal' },
          { value: 'p', class: 'Terminal' } ],
        [ { value: 'y', class: 'NonTerminal' },
          { value: 'e', class: 'Terminal' },
          { value: 's', class: 'Terminal' } ] ]

      expect(concatGroups).to.be.deep.equal(expectedGroups)
    })

    it('Should split properly concatenation groups when ends with OrBar', () => {
      const concatGroups = splitIntoConcatenationGroups([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' },
        { value: 'c', class: 'Terminal' },
        { value: '|', class: 'OrBar' }
      ])

      const expectedGroups = [
        [ { value: 'a', class: 'Terminal' },
          { value: 'b', class: 'NonTerminal' },
          { value: 'c', class: 'Terminal' } ] ]

      expect(concatGroups).to.be.deep.equal(expectedGroups)
    })
  })

  describe('#createAutomataFromWirthRule', () => {
    it('Should works for Character rule', () => {
      const rule = 'Character = letter | digit | special .'
      const result = createAutomataFromWirthRule(rule)
      expect(result.name).to.be.equal('Character')
      // prettyPrintAutomata(result.automata)
      // TODO: What should we expect????
    })

    it('Should works for Num rule', () => {
      const rule = 'Num = ( Int [ "." { digit } ] | "." Int ) [ "E" [ "+" | "-" ] Int ] .'
      const result = createAutomataFromWirthRule(rule)
      expect(result.name).to.be.equal('Num')
      // prettyPrintAutomata(result.automata)
      // TODO: What should we expect????
    })

    it('Should works for Dim rule', () => {
      const rule = 'Dim = "DIM" letter "(" int { "," int } ")" { "," letter "(" int { "," int } ")" } .'
      const result = createAutomataFromWirthRule(rule)
      expect(result.name).to.be.equal('Dim')
      // prettyPrintAutomata(result.automata)
      // TODO: What should we expect????
    })

    it('Should works for Dummy Concatenation rule', () => {
      const rule = 'Dummy = a b .'
      const result = createAutomataFromWirthRule(rule)
      expect(result.name).to.be.equal('Dummy')
      expect(serializeAutomata(result.automata)).to.be.deep.equal({
        'alphabet': [
          '{"class":"NonTerminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        'initialState': '0',
        'finalStates': [
          '2'
        ],
        'states': [
          '0',
          '1',
          '2'
        ],
        'transitions': [
          {
            'fromState': '0',
            'transitions': [
              {
                'consumes': '{"class":"NonTerminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          },
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })

    it('Should works for Return rule', () => {
      const rule = 'Return = "RETURN" .'
      const result = createAutomataFromWirthRule(rule)
      expect(result.name).to.be.equal('Return')
      expect(serializeAutomata(result.automata)).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"RETURN"}'
        ],
        'initialState': '0',
        'finalStates': [
          '1'
        ],
        'states': [
          '0',
          '1'
        ],
        'transitions': [
          {
            'fromState': '0',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"RETURN"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })
  })
})
