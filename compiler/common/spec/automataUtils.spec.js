'use strict'
/**
 * @typedef {import('../constants').GenericAutomata} GenericAutomata
 */

const { prettyPrintAutomata, serializeAutomata } = require('../automataUtils')
const { WIRTH_ELEMENT_CLASSES } = require('../constants')

const { expect } = require('chai')

const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'b' })
const automata = {
  alphabet: new Set([symbolA, symbolB]),
  initialState: '1',
  finalStates: new Set(['1', '5']),
  states: new Set(['1', '2', '3', '4', '5']),
  transitions: new Map([
    ['1', new Map([[symbolA, new Set(['3'])], [symbolB, new Set(['2'])]])],
    ['2', new Map([[symbolA, new Set(['4'])], [symbolB, new Set(['1'])]])],
    ['3', new Map([[symbolA, new Set(['5'])], [symbolB, new Set(['4'])]])],
    ['4', new Map([[symbolA, new Set(['4'])], [symbolB, new Set(['4'])]])],
    ['5', new Map([[symbolA, new Set(['3'])], [symbolB, new Set(['2'])]])]
  ]),
  isDFA: true,
  isMinimized: false
}

describe('/automataUtils', () => {
  describe('#prettyPrintAutomata', () => {
    it('Should pretty print it correctly', () => {
      prettyPrintAutomata(automata)
    })
  })
  describe('#serializeAutomata', () => {
    it('Should serialize it correctly', () => {
      const expected = {
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"Terminal","value":"b"}'
        ],
        'initialState': '1',
        'finalStates': [
          '1',
          '5'
        ],
        'states': [
          '1',
          '2',
          '3',
          '4',
          '5'
        ],
        'transitions': [
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '3'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '2',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '4'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"b"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          },
          {
            'fromState': '3',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '5'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"b"}',
                'toStates': [
                  '4'
                ]
              }
            ]
          },
          {
            'fromState': '4',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '4'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"b"}',
                'toStates': [
                  '4'
                ]
              }
            ]
          },
          {
            'fromState': '5',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '3'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expected)
    })
  })
})
