'use strict'

/**
 * @typedef {import('../constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('../constants').WirthElement} WirthElement
 * @typedef {import('../constants').GenericAutomata} GenericAutomata
 */

const {
  createAutomataFromConcatenationGroup,
  createAutomataFromAlternationGroup,
  handleAutomataFromOptionalGroup,
  handleAutomataFromRepetitionGroup,
  handleAutomataFromSelectionGroup
} = require('../wirthBlockConverter')

const { WIRTH_ELEMENT_CLASSES } = require('../constants')
const { serializeAutomata } = require('../automataUtils')

const { expect } = require('chai')

describe('/wirthBlockConverter', () => {
  describe('#createAutomataFromConcatenationGroup', () => {
    it('Should create automata from group with 4 elements', () => {
      const automata = createAutomataFromConcatenationGroup([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' },
        { value: 'c', class: 'Terminal' },
        { value: 'z', class: 'Terminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}',
          '{"class":"Terminal","value":"c"}',
          '{"class":"Terminal","value":"z"}'
        ],
        initialState: '0',
        finalStates: [
          '4'
        ],
        states: [ '0', '1', '2', '3', '4' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"a"}',
                toStates: [ '1' ]
              }
            ]
          },
          {
            fromState: '1',
            transitions: [
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [ '2' ]
              }
            ]
          },
          {
            fromState: '2',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"c"}',
                toStates: [ '3' ]
              }
            ]
          },
          {
            fromState: '3',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"z"}',
                toStates: [ '4' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })

    it('Should create automata from group with 2 elements', () => {
      const automata = createAutomataFromConcatenationGroup([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        initialState: '0',
        finalStates: [ '2' ],
        states: [ '0', '1', '2' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"a"}',
                toStates: [ '1' ]
              }
            ]
          },
          {
            fromState: '1',
            transitions: [
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [ '2' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })

    it('Should create automata from group with 1 element', () => {
      const automata = createAutomataFromConcatenationGroup([
        { value: 'b', class: 'NonTerminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"NonTerminal","value":"b"}'
        ],
        initialState: '0',
        finalStates: [ '1' ],
        states: [ '0', '1' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [
                  '1'
                ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })
  })

  describe('#createAutomataFromAlternationGroup', () => {
    it('Should create automata from group with 4 elements', () => {
      const automata = createAutomataFromAlternationGroup([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' },
        { value: 'c', class: 'Terminal' },
        { value: 'z', class: 'Terminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}',
          '{"class":"Terminal","value":"c"}',
          '{"class":"Terminal","value":"z"}'
        ],
        initialState: '0',
        finalStates: [ '1' ],
        states: [ '0', '1' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"a"}',
                toStates: [ '1' ]
              },
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [ '1' ]
              },
              {
                consumes: '{"class":"Terminal","value":"c"}',
                toStates: [ '1' ]
              },
              {
                consumes: '{"class":"Terminal","value":"z"}',
                toStates: [ '1' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })

    it('Should create automata from group with 2 elements', () => {
      const automata = createAutomataFromAlternationGroup([
        { value: 'a', class: 'Terminal' },
        { value: 'b', class: 'NonTerminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        initialState: '0',
        finalStates: [ '1' ],
        states: [ '0', '1' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"Terminal","value":"a"}',
                toStates: [ '1' ]
              },
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [ '1' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }

      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })

    it('Should create automata from group with 1 element', () => {
      const automata = createAutomataFromAlternationGroup([
        { value: 'b', class: 'NonTerminal' }
      ])

      const expectedAutomata = {
        alphabet: [
          '{"class":"NonTerminal","value":"b"}'
        ],
        initialState: '0',
        finalStates: [ '1' ],
        states: [ '0', '1' ],
        transitions: [
          {
            fromState: '0',
            transitions: [
              {
                consumes: '{"class":"NonTerminal","value":"b"}',
                toStates: [ '1' ]
              }
            ]
          }
        ],
        isDFA: true,
        isMinimized: false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })

    it('Should create automata with no transitions from group with 0 elements (to be reviewed)', () => {
      const automata = createAutomataFromAlternationGroup([])

      const expectedAutomata = {
        alphabet: [],
        initialState: '0',
        finalStates: [ '1' ],
        states: [ '0', '1' ],
        transitions: [
          {
            fromState: '0',
            transitions: []
          }
        ],
        isDFA: true,
        isMinimized: false
      }

      expect(serializeAutomata(automata)).to.be.deep.equal(expectedAutomata)
    })
  })

  describe('#handleAutomataFromOptionalGroup', () => {
    it('Should work for problematic example', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'c' })

      const optionalGroupAutomata = {
        alphabet: new Set([symbolA, symbolB, symbolC]),
        initialState: '1',
        finalStates: new Set(['2', '3']),
        states: new Set(['1', '2', '3']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['2'])], [symbolC, new Set(['3'])]])],
          ['2', new Map([[symbolB, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const automata = handleAutomataFromOptionalGroup(optionalGroupAutomata)
      const expected = {
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}',
          '{"class":"NonTerminal","value":"c"}'
        ],
        'initialState': '0',
        'finalStates': [
          '0',
          '1',
          '2'
        ],
        'states': [
          '0',
          '1',
          '2'
        ],
        'transitions': [
          {
            'fromState': '0',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"c"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expected)
    })
  })

  describe('#handleAutomataFromRepetitionGroup', () => {
    it('Should work for problematic example', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'c' })

      const optionalGroupAutomata = {
        alphabet: new Set([symbolA, symbolB, symbolC]),
        initialState: '1',
        finalStates: new Set(['2', '3']),
        states: new Set(['1', '2', '3']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['2'])], [symbolC, new Set(['3'])]])],
          ['2', new Map([[symbolB, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const automata = handleAutomataFromRepetitionGroup(optionalGroupAutomata)
      const expected = {
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}',
          '{"class":"NonTerminal","value":"c"}'
        ],
        'initialState': '0',
        'finalStates': [
          '0',
          '1',
          '2'
        ],
        'states': [
          '0',
          '1',
          '2'
        ],
        'transitions': [
          {
            'fromState': '0',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"c"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"c"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '2',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"c"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': false
      }
      expect(serializeAutomata(automata)).to.be.deep.equal(expected)
    })
  })

  describe('#handleAutomataFromSelectionGroup', () => {
    it('Should update nothing (to be reviewed)', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })
      const symbolC = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'c' })

      const selectionAutomata = {
        alphabet: new Set([symbolA, symbolB, symbolC]),
        initialState: '1',
        finalStates: new Set(['2', '3']),
        states: new Set(['1', '2', '3']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['2'])], [symbolC, new Set(['3'])]])],
          ['2', new Map([[symbolB, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }

      const automata = handleAutomataFromSelectionGroup(selectionAutomata)
      expect(automata).to.be.deep.equal({
        alphabet: new Set([symbolA, symbolB, symbolC]),
        initialState: '1',
        finalStates: new Set(['2', '3']),
        states: new Set(['1', '2', '3']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['2'])], [symbolC, new Set(['3'])]])],
          ['2', new Map([[symbolB, new Set(['3'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      })
    })
  })
})
