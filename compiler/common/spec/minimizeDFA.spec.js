'use strict'
/**
 * @typedef {import('../constants').GenericAutomata} GenericAutomata
 */

const { minimizeDFA } = require('../minimizeDFA')
const { WIRTH_ELEMENT_CLASSES } = require('../constants')
const { serializeAutomata } = require('../automataUtils')
const { createAutomataFromWirthRule } = require('../wirthConverter')
const { expect } = require('chai')

describe('/minimizeDFA', () => {
  describe('#minimizeDFA', () => {
    it('Should work for automata #1 (Old Dominion University)', () => {
      // https://www.cs.odu.edu/~toida/nerzic/390teched/regular/fa/min-fa.html
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })

      const automata = {
        alphabet: new Set([symbolA, symbolB]),
        initialState: '1',
        finalStates: new Set(['1', '5']),
        states: new Set(['1', '2', '3', '4', '5']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['3'])], [symbolB, new Set(['2'])]])],
          ['2', new Map([[symbolA, new Set(['4'])], [symbolB, new Set(['1'])]])],
          ['3', new Map([[symbolA, new Set(['5'])], [symbolB, new Set(['4'])]])],
          ['4', new Map([[symbolA, new Set(['4'])], [symbolB, new Set(['4'])]])],
          ['5', new Map([[symbolA, new Set(['3'])], [symbolB, new Set(['2'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }
      const minimized = minimizeDFA(automata)
      expect(serializeAutomata(minimized)).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        'initialState': '1',
        'finalStates': [
          '1'
        ],
        'states': [
          '1',
          '2',
          '3'
        ],
        'transitions': [
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '3'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '2',
            'transitions': [
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          },
          {
            'fromState': '3',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })

    it('Should work for automata #2 (Old Dominion University)', () => {
      const symbolA = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'a' })
      const symbolB = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: 'b' })

      const automata = {
        alphabet: new Set([symbolA, symbolB]),
        initialState: '1',
        finalStates: new Set(['2', '4', '5', '6']),
        states: new Set(['1', '2', '3', '4', '5', '6']),
        transitions: new Map([
          ['1', new Map([[symbolA, new Set(['2'])], [symbolB, new Set(['3'])]])],
          ['2', new Map([[symbolA, new Set(['2'])], [symbolB, new Set(['4'])]])],
          ['3', new Map([[symbolA, new Set(['3'])], [symbolB, new Set(['3'])]])],
          ['4', new Map([[symbolA, new Set(['6'])], [symbolB, new Set(['3'])]])],
          ['5', new Map([[symbolA, new Set(['5'])], [symbolB, new Set(['3'])]])],
          ['6', new Map([[symbolA, new Set(['5'])], [symbolB, new Set(['4'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }
      const minimized = minimizeDFA(automata)
      expect(serializeAutomata(minimized)).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"a"}',
          '{"class":"NonTerminal","value":"b"}'
        ],
        'initialState': '1',
        'finalStates': [
          '2',
          '4',
          '5',
          '6'
        ],
        'states': [
          '1',
          '2',
          '4',
          '5',
          '6'
        ],
        'transitions': [
          {
            'fromState': '1',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '2'
                ]
              }
            ]
          },
          {
            'fromState': '2',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '2'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '4'
                ]
              }
            ]
          },
          {
            'fromState': '4',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '6'
                ]
              }
            ]
          },
          {
            'fromState': '5',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '5'
                ]
              }
            ]
          },
          {
            'fromState': '6',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"a"}',
                'toStates': [
                  '5'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"b"}',
                'toStates': [
                  '4'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })

    it('Should work for automata #3 (Tutorials Point)', () => {
      // https://www.tutorialspoint.com/automata_theory/dfa_minimization.htm
      const symbol0 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '0' })
      const symbol1 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '1' })

      const automata = {
        alphabet: new Set([symbol0, symbol1]),
        initialState: 'a',
        finalStates: new Set(['c', 'd', 'e']),
        states: new Set(['a', 'b', 'c', 'd', 'e', 'f']),
        transitions: new Map([
          ['a', new Map([[symbol0, new Set(['b'])], [symbol1, new Set(['c'])]])],
          ['b', new Map([[symbol0, new Set(['a'])], [symbol1, new Set(['d'])]])],
          ['c', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])],
          ['d', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])],
          ['e', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])],
          ['f', new Map([[symbol0, new Set(['f'])], [symbol1, new Set(['f'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }
      const minimized = minimizeDFA(automata)
      // console.log(JSON.stringify(serializeAutomata(minimized), undefined, 2))
      expect(serializeAutomata(minimized)).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"0"}',
          '{"class":"Terminal","value":"1"}'
        ],
        'initialState': 'a',
        'finalStates': [
          'c'
        ],
        'states': [
          'a',
          'c'
        ],
        'transitions': [
          {
            'fromState': 'a',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"0"}',
                'toStates': [
                  'a'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"1"}',
                'toStates': [
                  'c'
                ]
              }
            ]
          },
          {
            'fromState': 'c',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"0"}',
                'toStates': [
                  'c'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })

    it('Should work for automata #4 (Tutorials Point with missing transition row)', () => {
      const symbol0 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '0' })
      const symbol1 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '1' })

      const automata = {
        alphabet: new Set([symbol0, symbol1]),
        initialState: 'a',
        finalStates: new Set(['c', 'd', 'e']),
        states: new Set(['a', 'b', 'c', 'd', 'e', 'f']),
        transitions: new Map([
          ['a', new Map([[symbol0, new Set(['b'])], [symbol1, new Set(['c'])]])],
          ['b', new Map([[symbol0, new Set(['a'])], [symbol1, new Set(['d'])]])],
          ['c', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])],
          ['d', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])],
          ['e', new Map([[symbol0, new Set(['e'])], [symbol1, new Set(['f'])]])]
        ]),
        isDFA: true,
        isMinimized: false
      }
      const minimized = minimizeDFA(automata)
      expect(serializeAutomata(minimized)).to.be.deep.equal({
        'alphabet': [
          '{"class":"Terminal","value":"0"}',
          '{"class":"Terminal","value":"1"}'
        ],
        'initialState': 'a',
        'finalStates': [
          'c'
        ],
        'states': [
          'a',
          'c'
        ],
        'transitions': [
          {
            'fromState': 'a',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"0"}',
                'toStates': [
                  'a'
                ]
              },
              {
                'consumes': '{"class":"Terminal","value":"1"}',
                'toStates': [
                  'c'
                ]
              }
            ]
          },
          {
            'fromState': 'c',
            'transitions': [
              {
                'consumes': '{"class":"Terminal","value":"0"}',
                'toStates': [
                  'c'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })

    it('Should work for automata from Character rule', () => {
      const { automata } = createAutomataFromWirthRule('Character = integer | special | letter .')
      const minimized = minimizeDFA(automata)
      const serial = serializeAutomata(minimized)
      expect(serial).to.be.deep.equal({
        'alphabet': [
          '{"class":"NonTerminal","value":"letter"}',
          '{"class":"NonTerminal","value":"special"}',
          '{"class":"NonTerminal","value":"integer"}'
        ],
        'initialState': '0',
        'finalStates': [
          '1'
        ],
        'states': [
          '0',
          '1'
        ],
        'transitions': [
          {
            'fromState': '0',
            'transitions': [
              {
                'consumes': '{"class":"NonTerminal","value":"letter"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"special"}',
                'toStates': [
                  '1'
                ]
              },
              {
                'consumes': '{"class":"NonTerminal","value":"integer"}',
                'toStates': [
                  '1'
                ]
              }
            ]
          }
        ],
        'isDFA': true,
        'isMinimized': true
      })
    })
    it('Should work for automata from insane long letter rule', () => {
      const { automata } = createAutomataFromWirthRule('letter = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" | "J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" | "S" | "T" | "U"| "V" | "W" | "X" | "Y" | "Z" | "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" | "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z" .')
      const minimized = minimizeDFA(automata)
      const serial = serializeAutomata(minimized)
      // console.log(JSON.stringify(serial, undefined, 2))
      expect(serial.states.length).to.be.equal(2)
    })

    it('Should work for automata from complex rule Num', () => {
      const { automata } = createAutomataFromWirthRule('Num = ( Int [ "." { digit } ] | "." Int ) [ "E" [ "+" | "-" ] Int ] .')
      const minimized = minimizeDFA(automata)
      serializeAutomata(minimized)
      // console.log(JSON.stringify(serial, undefined, 2))
    })
  })
})
