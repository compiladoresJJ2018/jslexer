'use strict'
/**
 * @typedef {import('../constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('../constants').WirthElement} WirthElement
 * @typedef {import('../constants').GenericAutomata} GenericAutomata
 */

const { findEclosure, findEclosures, nextStatesNDFA, convertNDFAtoDFA } = require('../NDFAToDFA')
const { WIRTH_ELEMENT_CLASSES, EMPTY_SYMBOL } = require('../constants')
const { eqSet } = require('../setUtils')

const { expect } = require('chai')

const expectSetEquality = (setA, setB) => {
  const isEqual = eqSet(setA, setB)
  if (!isEqual) {
    console.log(setA)
    console.log(setB)
  }
  expect(isEqual).to.be.equal(true)
}

describe('/NDFAToDFA', () => {
  // https://www.quora.com/What-is-epsilon-closure
  const symbol0 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '0' })
  const symbol1 = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: '1' })
  const automata = {
    alphabet: new Set([symbol0, symbol1]),
    initialState: 'A',
    finalStates: new Set(['D']),
    states: new Set(['A', 'B', 'C', 'D']),
    transitions: new Map([
      ['A', new Map([[symbol0, new Set(['A'])], [EMPTY_SYMBOL, new Set(['B'])]])],
      ['B', new Map([[symbol0, new Set(['C'])], [EMPTY_SYMBOL, new Set(['D'])]])],
      ['C', new Map([[symbol1, new Set(['B'])]])],
      ['D', new Map([[symbol0, new Set(['D'])], [symbol1, new Set(['D'])]])]
    ]),
    isDFA: false,
    isMinimized: false
  }
  const expected = {
    'A': new Set(['A', 'B', 'D']),
    'B': new Set(['B', 'D']),
    'C': new Set(['C']),
    'D': new Set(['D'])
  }

  describe('#findEclosure', () => {
    it('Should work for automata', () => {
      expectSetEquality(findEclosure(automata, 'A'), expected['A'])
      expectSetEquality(findEclosure(automata, 'B'), expected['B'])
      expectSetEquality(findEclosure(automata, 'C'), expected['C'])
      expectSetEquality(findEclosure(automata, 'D'), expected['D'])
    })
  })

  describe('#findEclosures', () => {
    it('Should work for automata', () => {
      const eClosures = findEclosures(automata, 'A')
      for (let i = 0; i < eClosures.length; i++) {
        expectSetEquality(eClosures[i].eClosure, expected[eClosures[i].state])
      }
    })
  })

  describe('#nextStatesNDFA', () => {
    it('Should bring next reachable states for given input', () => {
      const result = nextStatesNDFA(automata, 'C', symbol1)
      expectSetEquality(result, new Set(['B', 'D']))
      const resultForEmptyInA = nextStatesNDFA(automata, 'A', EMPTY_SYMBOL)
      expectSetEquality(resultForEmptyInA, new Set(['A', 'B', 'D']))
    })
  })
  describe('#convertNDFAtoDFA', () => {
    it('DONT KNOW, DONT CARE, IT MAY BE WORKING', () => {
      convertNDFAtoDFA(automata)
      // prettyPrintAutomata(dfaAutomata)
    })
  })
})
