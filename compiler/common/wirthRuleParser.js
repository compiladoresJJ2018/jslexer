'use strict'
/*
  A fim de não gerar um arquivo de código muito grande, o conversor de Wirth foi dividido
  em 4 arquivos principais, separados por funcionalidade.

  - wirthRuleParser.js: É o front do conversor, parseia uma string na notação de Wirth
    para gerar um array com terminais, não terminais, parentheses etc classificados.
    A função de maior nível que faz isso é a generateRuleArray(rulesStr: String) : WirthElement[]
*/
/**
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').WirthParserResult} WirthParserResult
 */

const { WIRTH_ELEMENT_CLASSES } = require('./constants')

/**
 * Returns some temporary placeholder for double quotes inside NT
 * @param {String} ruleStr
 * @returns {String}
 */
const getNTDoubleQuotePlaceholder = ruleStr => {
  let guess = '\u{D8}'
  while (ruleStr.indexOf(guess) > -1) {
    guess = String.fromCharCode(guess.charCodeAt(0) + 1)
  }
  return guess
}

/**
 * Trim ruleStr and extract name and formal rule.
 * Do some validations asserting that the rule has the definition and end dot symbols.
 * Returns the name and ruleStr trimmed without the end dot (.) and define symbols (=).
 * @param {String} ruleStr
 */
const extractNameAndRule = ruleStr => {
  const firstEqual = ruleStr.indexOf('=')
  if (firstEqual < 0) throw new Error('Wirth rule string must have definition symbol \'=\'')
  const name = ruleStr.slice(0, firstEqual).trim()
  let rule = ruleStr.slice(firstEqual + 1).trim()
  if (!rule.endsWith('.')) throw new Error('Wirth rule string must end with dot symbol (.)')
  rule = rule.slice(0, rule.length - 2).trim()
  return {
    name,
    rule
  }
}

/**
 * Takes a non ambigue and well formed ruleStr (refering double quotes) and outputs
 * a partial classified rule array, replacing the double quotes placeholder of terminal elements
 * @param {String} ruleStr
 * @param {String} ntDoubleQuotePlaceholder
 * @returns {WirthElement[]}
 */
const encapsulateTerminalElements = (ruleStr, ntDoubleQuotePlaceholder) => {
  const splitedByQuotes = ruleStr.split('"')
  const partialWirthElements = []
  for (let i = 0; i < splitedByQuotes.length; i++) {
    if (i % 2 === 0) {
      const value = splitedByQuotes[i].trim()
      if (value) {
        partialWirthElements.push({
          class: WIRTH_ELEMENT_CLASSES.Unclassified,
          value
        })
      }
    } else {
      partialWirthElements.push({
        class: WIRTH_ELEMENT_CLASSES.Terminal,
        value: splitedByQuotes[i].replace(ntDoubleQuotePlaceholder, '"')
      })
    }
  }
  return partialWirthElements
}

/**
 * Parse unclassified wirth element into a classified wirth element array
 * @param {String} value
 * @returns {WirthElement[]}
 */
const parseUnclassifiedElementValue = value => {
  const reversedEnum = {
    '(': 'LeftParentheses',
    ')': 'RightParentheses',
    '[': 'LeftSquareBracket',
    ']': 'RightSquareBracket',
    '{': 'LeftCurlyBracket',
    '}': 'RightCurlyBracket',
    '|': 'OrBar'
  }
  /**
   * @type {WirthElement[]}
   */
  const wirthElements = []

  // Parse the unclassified part
  let terminal = ''
  for (const symbol of value) {
    if (!['(', ')', '[', ']', '{', '}', '|', ' '].includes(symbol)) {
      terminal += symbol
    } else {
      if (terminal.trim()) {
        wirthElements.push({
          class: WIRTH_ELEMENT_CLASSES.NonTerminal,
          value: terminal.trim()
        })
        terminal = ''
      }
      if (symbol.trim()) {
        wirthElements.push({
          class: WIRTH_ELEMENT_CLASSES[reversedEnum[symbol]],
          value: symbol
        })
      }
    }
  }
  if (terminal.trim()) {
    wirthElements.push({
      class: WIRTH_ELEMENT_CLASSES.NonTerminal,
      value: terminal.trim()
    })
  }
  return wirthElements
}

/**
 * Classifies remaining unclassified wirth elements
 * @param {WirthElement[]} wirthElementArray
 * @returns {WirthElement[]}
 */
const encapsulateUnclassifiedElements = wirthElementArray => {
  /**
   * @type {WirthElement[]}
   */
  const finalWirthElements = []

  for (let i = 0; i < wirthElementArray.length; i++) {
    const wirthElement = wirthElementArray[i]
    if (wirthElement.class === WIRTH_ELEMENT_CLASSES.Terminal) {
      finalWirthElements.push(wirthElement)
    } else {
      const parsedWirthElements = parseUnclassifiedElementValue(wirthElement.value)
      for (let j = 0; j < parsedWirthElements.length; j++) {
        finalWirthElements.push(parsedWirthElements[j])
      }
    }
  }

  return finalWirthElements
}

/**
 * Parse a wirth rule string into an array with Wirth Notation elements
 * Double quotes are vital for nt and t diferentiation, so a quote in nt must be preceded by double bar
 * @param {String} ruleStr
 * @returns {WirthParserResult}
 */
const createRuleArray = ruleStr => {
  // Replace double quote inside NTs for easier manipulation
  const ntDoubleQuotePlaceholder = getNTDoubleQuotePlaceholder(ruleStr)
  const { rule, name } = extractNameAndRule(ruleStr.replace(/"""/ug, `"${ntDoubleQuotePlaceholder}"`))
  if ((rule.match(/"/ug) || []).length % 2 !== 0) throw new Error('Should have even number of double quotes (")')
  const ruleWithTerminalsClassified = encapsulateTerminalElements(rule, ntDoubleQuotePlaceholder)
  const ruleArray = encapsulateUnclassifiedElements(ruleWithTerminalsClassified)
  return {
    name,
    ruleArray
  }
}

module.exports = { getNTDoubleQuotePlaceholder, extractNameAndRule, createRuleArray }
