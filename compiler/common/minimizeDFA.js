'use strict'
/**
 * Este arquivo implementa a minimização de um automato
 * deterministico, conforme foi visto na disciplina de Lógica Computacional,
 * porém atualizado e recriado a partir do zero para servir para o projeto de compiladores.
 */
/**
 * @typedef {import('./constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').NamedGenericAutomata} NamedGenericAutomata
 */

/**
 * Returns the next state, or 'EMPTY_STATE'
 * @param {GenericAutomata} automata
 * @param {String} state
 * @param {String} symbol
 */
const getNextState = (automata, state, symbol) => {
  const transitions = automata.transitions.get(state)
  if (transitions && transitions.get(symbol) && transitions.get(symbol).values().next().value) {
    return transitions.get(symbol).values().next().value
  } else return 'EMPTY_STATE'
}

/**
 * @typedef {Object} PartitionObj
 * @property {String[][]} partition
 * @property {Map<string, number>} stateMap
 */

/**
 * This function returns a new partition
 * following the dfa minimization algorithm.
 * If for any two states in a same subset of a Partition i,
 * If for all the alphabet at least 1 symbol takes the first state
 * to a distinct partition of the second, it must be partitioned.
 * Also returns a new state map.
 * If no subset will be created, returns undefined.
 * @param {GenericAutomata} automata
 * @param {PartitionObj} partitionObj
 * @returns {PartitionObj}
 */
const getNewPartition = (automata, partitionObj) => {
  const { partition, stateMap } = partitionObj
  const newPartition = []
  let numberNewSets = 0
  const newStateMap = new Map()
  newStateMap.set('EMPTY_STATE', -1)
  for (let i = 0; i < partition.length; i++) {
    const set = partition[i]
    const subSetA = []
    const subSetB = []
    if (set.length > 0) {
      const pivot = set[0]
      subSetA.push(pivot)
      newStateMap.set(pivot, numberNewSets)
      for (let k = 1; k < set.length; k++) {
        const state = set[k]
        let isSetA = true
        for (const symbol of automata.alphabet) {
          const nextStatePivot = getNextState(automata, pivot, symbol)
          const nextState = getNextState(automata, state, symbol)
          if (stateMap.get(nextStatePivot) !== stateMap.get(nextState)) {
            isSetA = false
            break
          }
        }
        if (isSetA) {
          subSetA.push(state)
          newStateMap.set(state, numberNewSets)
        } else {
          subSetB.push(state)
          newStateMap.set(state, numberNewSets + 1)
        }
      }
    }

    newPartition.push(subSetA)
    numberNewSets += 1

    if (subSetB.length > 0) {
      newPartition.push(subSetB)
      numberNewSets += 1
    }
  }
  if (numberNewSets !== partition.length) {
    return {
      partition: newPartition,
      stateMap: newStateMap
    }
  } else return undefined
}

/**
 * Returns the final partition in the dfa minimization algorithm.
 * @param {GenericAutomata} automata
 * @returns {PartitionObj}
 */
const getFinalPartition = (automata) => {
  // Initial partition and stateMap
  let partitionObj = {
    partition: [
      [...automata.states].filter(s => !automata.finalStates.has(s)),
      [...automata.finalStates]
    ],
    stateMap: new Map()
  }
  for (let i = 0; i < partitionObj.partition.length; i++) {
    const set = partitionObj.partition[i]
    for (const state of set) {
      partitionObj.stateMap.set(state, i)
    }
  }
  partitionObj.stateMap.set('EMPTY_STATE', -1)

  while (true) {
    const newPartitionObj = getNewPartition(automata, partitionObj)
    if (newPartitionObj) partitionObj = newPartitionObj
    else break
  }
  return partitionObj
}

const getTransitions = (finalPartition, stateMap, minimizedStates, automata, deadStates) => {
  const rawTransitions = minimizedStates.map(state => { // Pra cada estado minimizado
    return finalPartition[stateMap.get(state)] // Pega transicoes olhando cada estado do set de origem
      .map(s => {
        if (!automata.transitions.has(s)) return []
        return [...automata.transitions.get(s)] // Pega as transicoes do estado
          .map(t => {
            const candidateNext = finalPartition[stateMap.get(t[1].values().next().value)][0]
            if (!deadStates.includes(candidateNext)) {
              return {
                from: state,
                consumes: t[0],
                to: finalPartition[stateMap.get(t[1].values().next().value)][0]
              }
            } else return undefined
          })
          .filter(t => t)
      })
      .reduce((p, v) => p.concat(v))
  }).reduce((p, v) => p.concat(v))

  const transitions = new Map()
  for (const rt of rawTransitions) {
    if (!transitions.has(rt.from)) transitions.set(rt.from, new Map())
    if (!transitions.get(rt.from).has(rt.consumes)) transitions.get(rt.from).set(rt.consumes, new Set())
    transitions.get(rt.from).get(rt.consumes).add(rt.to)
  }
  return transitions
}

/**
 * Minimize the number of states of a given automata
 * @param {GenericAutomata} automata
 * @returns {GenericAutomata}
 */
const minimizeDFA = (automata) => {
  let { partition: finalPartition, stateMap } = getFinalPartition(automata)

  const minimizedStates = []
  const deadStates = []
  // Removes dead states
  for (const set of finalPartition) {
    const representativeState = set[0]
    // Keeps only if is final or has transition to other set
    if (automata.finalStates.has(representativeState) || (
      automata.transitions.has(representativeState) &&
    [...automata.transitions.get(representativeState).values()]
      .map(t => stateMap.get(t.values().next().value))
      .filter(n => n !== stateMap.get(representativeState)).length > 0)) {
      minimizedStates.push(set[0])
    } else {
      deadStates.push(...set)
    }
  }

  /**
   * @type {GenericAutomata}
   */
  return {
    alphabet: automata.alphabet,
    initialState: minimizedStates.filter(s => finalPartition[stateMap.get(s)].includes(automata.initialState))[0],
    finalStates: new Set(minimizedStates.filter(s => automata.finalStates.has(s)).sort()),
    states: new Set(minimizedStates.sort()),
    transitions: getTransitions(finalPartition, stateMap, minimizedStates, automata, deadStates),
    isDFA: true,
    isMinimized: true
  }
}

module.exports = {
  minimizeDFA
}
