'use strict'
/** @typedef {import('./constants').CompilerClassification} CompilerClassification */
const defaultColumns = [
  {
    name: 'Line',
    getValue: r => r.line + ''
  },
  {
    name: 'Column',
    getValue: r => r.column + ''
  },
  {
    name: 'Value',
    getValue: r => r.symbol.replace(/\n/gu, '\\n')
  },
  {
    name: 'Top Classification',
    getValue: r => r.classification + ''
  },
  {
    name: 'Classification(s)',
    getValue: r => r.equivalentClassifications.join(', ') + ''
  },
  {
    name: 'Statement',
    getValue: r => r.classification === 'BStatement' ? r.children[1].classification + '' : ''
  }
]

/**
 * Printa classificações do compilador de forma bonitinha
 * @param {CompilerClassification[]} result
 */
const printClassificationTable = (result, columns = defaultColumns) => {
  const spacing = 4

  const maxLenValues = columns.map((col) => result.map(col.getValue).map(v => v.length).reduce((p, c) => p > c ? p : c, col.name.length))

  console.log(columns.map((col, i) => col.name + ' '.repeat(maxLenValues[i] - col.name.length + spacing)).join(''))
  console.log(`${'‾'.repeat(maxLenValues.reduce((p, c) => p + c, 0) + (maxLenValues.length - 1) * spacing)}`)
  result.map(r => {
    console.log(columns.map((col, i) => col.getValue(r) + ' '.repeat(Math.max(maxLenValues[i], col.getValue(r).length) - col.getValue(r).length + spacing)).join(''))
  })
}

module.exports = { printClassificationTable, defaultColumns }
