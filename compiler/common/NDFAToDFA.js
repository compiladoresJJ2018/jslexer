'use strict'
/**
 * Este arquivo implementa a conversão de um automato
 * não deterministico para um deterministico, conforme foi visto
 * na disciplina de Lógica Computacional, porém atualizado e recriado
 * a partir do zero para servir para o projeto de compiladores.
 */
/**
 * @typedef {import('./constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').NamedAutomata} NamedAutomata
 */

const { EMPTY_SYMBOL } = require('./constants')
const { eqSet } = require('./setUtils')

/**
 * Find e-closure for a state
 * @param {GenericAutomata} automata
 * @param {String} state
 * @returns {Set<String>}
 */
const findEclosure = (automata, state, acc = new Set([])) => {
  // Marks state as visited
  acc.add(state)
  // Adds to other state
  if (automata.transitions.get(state)) {
    const nextStatesFromEmptySymbol = automata.transitions.get(state).get(EMPTY_SYMBOL) || new Set()
    for (const nextStateFromEmptySymbol of nextStatesFromEmptySymbol) {
      if (!acc.has(nextStateFromEmptySymbol)) {
        acc = new Set([...acc, ...findEclosure(automata, nextStateFromEmptySymbol, acc)])
      }
    }
  }
  return acc
}

/**
 * Finds e-closures for all automata states
 * @param {GenericAutomata} automata
 */
const findEclosures = (automata) => {
  return [...automata.states].map(s => {
    return {
      state: s,
      eClosure: findEclosure(automata, s)
    }
  })
}

/**
 * Get set of reachable next states of NDFA for given input
 * @param {GenericAutomata} automata
 * @param {String} state
 * @param {String} input
 * @returns {Set<String>}
 */
const nextStatesNDFA = (automata, state, input) => {
  const eClosure = findEclosure(automata, state)
  if (input === EMPTY_SYMBOL) return eClosure
  else {
    let acc = new Set([])
    for (const eState of eClosure) {
      if (automata.transitions.get(eState)) {
        const reachable = [...(automata.transitions.get(eState).get(input) || new Set())]
          .map(s => [...findEclosure(automata, s)])
          .reduce((p, c) => p.concat(c), [])
        acc = new Set([...acc, ...reachable])
      }
    }
    return acc
  }
}

/**
 * @typedef {Object} NonStdState
 * @property {Set<String>} set - Set of NDFA states to be converted to a DFA state
 * @property {String} index
 */

/**
 * @typedef {Object} NonStdTransition
 * @property {NonStdState} fromState
 * @property {NonStdState} toState
 * @property {String} element
 */

/**
 * @typedef {Object} NonStdTempAutomata
 * @property {Set<String>} alphabet
 * @property {NonStdState} initialState
 * @property {NonStdState[]} states
 * @property {NonStdState[]} finalStates
 * @property {NonStdTransition[]} simpleTransitions
 */

/**
 * Converts a non standard dfa into a proper dfa.
 * It modifies the parameter.
 * @param {NonStdTempAutomata} nonStdAutomata
 * @returns {GenericAutomata}
 */
const convertNonStdDFA = (nonStdAutomata) => {
  /**
   * @type {GenericAutomata}
   */
  const automata = {
    alphabet: nonStdAutomata.alphabet,
    initialState: nonStdAutomata.initialState.index,
    finalStates: new Set(nonStdAutomata.finalStates.map(ns => ns.index)),
    states: new Set(nonStdAutomata.states.map(ns => ns.index)),
    transitions: new Map(),
    isDFA: true,
    isMinimized: false
  }
  for (let i = 0; i < nonStdAutomata.simpleTransitions.length; i++) {
    const simpleTransition = nonStdAutomata.simpleTransitions[i]
    if (!automata.transitions.get(simpleTransition.fromState.index)) {
      automata.transitions.set(simpleTransition.fromState.index, new Map())
    }
    if (!automata.transitions.get(simpleTransition.fromState.index).get(simpleTransition.element)) {
      automata.transitions.get(simpleTransition.fromState.index).set(simpleTransition.element, new Set())
    }
    automata.transitions.get(simpleTransition.fromState.index).get(simpleTransition.element).add(simpleTransition.toState.index)
  }
  return automata
}

/**
 * Converts a NDFA to DFA
 * @param {GenericAutomata} automata
 * @return {GenericAutomata}
 */
const convertNDFAtoDFA = automata => {
  const { alphabet } = automata
  let index = 0
  // Get the new initial state as set
  const initialState = {
    set: findEclosure(automata, automata.initialState),
    index: index + ''
  }
  const states = []
  const finalStates = []

  // Transitions as array of transitions set to set
  const simpleTransitions = []
  index += 1
  const statesRemaining = [ initialState ] // Fila para checar os estados da DFA
  while (statesRemaining.length > 0) {
    // State being verified
    let fromState = statesRemaining[0]
    for (const element of alphabet) {
      let toStateRaw = new Set([''])
      toStateRaw.delete('')
      for (const simpleState of fromState.set) {
        toStateRaw = new Set([...toStateRaw, ...nextStatesNDFA(automata, simpleState, element)])
      }
      // Se existe um proximo macro estado
      if (toStateRaw.size > 0) {
        let toState = [...states, ...statesRemaining].filter(s => eqSet(s.set, toStateRaw))[0]
        // Se o proximo estado nao foi verificado, adiciona para a lista de estados remanescentes
        if (!toState) {
          toState = {
            set: toStateRaw,
            index: index + ''
          }
          statesRemaining.push(toState)
          index += 1
        }
        simpleTransitions.push({
          fromState,
          toState,
          element
        })
      }
    }
    statesRemaining.shift()
    states.push(fromState)
    for (const originalFinalState of automata.finalStates) {
      if (fromState.set.has(originalFinalState)) {
        finalStates.push(fromState)
      }
    }
  }

  return convertNonStdDFA({
    alphabet,
    initialState,
    states,
    finalStates,
    simpleTransitions
  })
}

module.exports = {
  findEclosure,
  findEclosures,
  nextStatesNDFA,
  convertNDFAtoDFA
}
