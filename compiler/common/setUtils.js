'use strict'
/*
  Algumas funções úteis para manipular Sets em Javascript
*/

/**
 * Verifica se 2 sets sao iguais
 * @param {Set} as
 * @param {Set} bs
 */
const eqSet = (as, bs) => {
  if (as.size !== bs.size) return false
  for (const a of as) if (!bs.has(a)) return false
  return true
}

module.exports = {
  eqSet
}
