'use strict'
/*
  Neste arquivo estão escritas algumas funções para tornar um automato serializável
  e poder printá-lo na tela e compará-lo em testes unitários com maior facilidade.
*/
/**
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').SerializableGenericAutomata} SerializableGenericAutomata
 * @typedef {import('./constants').SerializableGenericTransition} SerializableGenericTransition
 * @typedef {import('./constants').SerializableGenericTransitionResult} SerializableGenericResult
 */

/**
 * Pretty print a generic automata
 * @param {GenericAutomata} automata
 */
const prettyPrintAutomata = automata => {
  console.log('{\n alphabet:', automata.alphabet, ',\n', `initialState: '${automata.initialState}',\n finalStates:`,
    automata.finalStates, ',\n states: ', automata.states, ',\n transitions: {')
  for (const transition of automata.transitions) {
    console.log(' \'' + transition[0] + '\' => ', transition[1], ',')
  }
  console.log(` },\n isDFA: ${automata.isDFA},\n isMinimized: ${automata.isMinimized}\n}`)
}

/**
 * Returns a serializable object to generic automata
 * @param {GenericAutomata} automata
 * @returns {SerializableGenericAutomata}
 */
const serializeAutomata = automata => {
  return {
    alphabet: [...automata.alphabet],
    initialState: automata.initialState,
    finalStates: [...automata.finalStates],
    states: [...automata.states],
    transitions: [...automata.transitions].map(t => {
      return {
        fromState: t[0],
        transitions: [...t[1]].map(st => {
          return {
            consumes: st[0],
            toStates: [...st[1]]
          }
        })
      }
    }),
    isDFA: automata.isDFA,
    isMinimized: automata.isMinimized
  }
}

module.exports = {
  prettyPrintAutomata,
  serializeAutomata
}
