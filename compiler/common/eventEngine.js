'use strict'
/**
 * Este arquivo implementa um motor de eventos generico, a fim de ser usado
 * para simular um automato finito.
 */

/**
 * @typedef {import('./constants').EventEngineEvent} EventEngineEvent
 * @typedef {import('./constants').EventEngineEventHandler} EventEngineEventHandler
 * @typedef {import('./constants').EventEngineSimulatorInstance} EventEngineSimulatorInstance
 * @typedef {import('./constants').EventEngineSimulatorSetup} EventEngineSimulatorSetup
 * @typedef {import('./constants').EventEngineOptions} EventEngineOptions
 */

/**
 * @type {EventEngineOptions}
 */
const defaultOptions = {
  pushInitialEvents: (eventList) => { eventList.push({ type: 'EndEvent' }) },
  addToContext: (context) => {},
  handleRoutines: new Map([
    [
      'EndEvent',
      (event, simulatorContext) => {
        simulatorContext.result.message = 'Empty simulation has been run'
      }
    ]
  ])
}

/**
 * Retorna um função para iniciar execução do motor de eventos.
 * @param {EventEngineOptions} options
 * @return {EventEngineSimulatorSetup}
 */
const getEventEngineSimulator = (options = defaultOptions) => {
  const { pushInitialEvents, handleRoutines, addToContext } = options
  return function setup (input) {
    let step = 0
    let simulatorContext = {
      eventQueue: [],
      initialInput: input,
      result: {}
    }
    const simulatorInstance = {
      next: () => {
        if (simulatorContext.eventQueue.length > 0) {
          const event = simulatorContext.eventQueue.shift()
          const handler = handleRoutines.get(event.type)
          handler(event, simulatorContext)
          step += 1
          return step
        }
        return undefined
      },
      result: () => simulatorContext.result,
      step: () => step
    }
    addToContext(simulatorContext)
    pushInitialEvents(simulatorContext.eventQueue)
    return simulatorInstance
  }
}

module.exports = getEventEngineSimulator
