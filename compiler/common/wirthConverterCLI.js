/**
 * CLI (Command Line Interface) para o conversor de notação de wirth.
 * Salva o autômato no formato .gv (https://en.wikipedia.org/wiki/DOT_(graph_description_language))
 * Espera 1 argumento:
 * - Regra da notação de Wirth Escapada (Eg.: 'Eb = "(" Exp ")" | Num | Var | ( "FN" letter | Predef ) "(" Exp ")" .')
 *
 * Tais diagramas, podem por exemplo, ser gerados no website: https://graphs.grevian.org/graph
 */
/**
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').NamedGenericAutomata} NamedGenericAutomata
 * @typedef {import('./constants').SerializableGenericAutomata} SerializableGenericAutomata
 */

const path = require('path')
const fs = require('fs')

const { WIRTH_ELEMENT_CLASSES } = require('./constants')
const { createAutomataFromWirthRule } = require('./wirthConverter')
const { minimizeDFA } = require('./minimizeDFA')
const { serializeAutomata } = require('./automataUtils')
const { download } = require('./graphDownloader')
/**
 * Converte um automato serializavel em notação DOT
 * @param {SerializableGenericAutomata} serializable
 * @returns {String}
 */
const convertSerializableToDotNotation = (serializable) => {
  let resultStr = `digraph dfa {
  "" [shape=none]
`
  for (const state of serializable.states) {
    if (serializable.finalStates.includes(state)) {
      resultStr += `  "${state}" [shape=doublecircle]\n`
    } else {
      resultStr += `  "${state}" [shape=circle]\n`
    }
  }
  resultStr += `  "" -> "${serializable.initialState}"\n`
  for (const transitionState of serializable.transitions) {
    for (const transition of transitionState.transitions) {
      const consumes = JSON.parse(transition.consumes)
      for (const toState of transition.toStates) {
        if (consumes.class === WIRTH_ELEMENT_CLASSES.Terminal) {
          resultStr += `  "${transitionState.fromState}" -> "${toState}" [label="'${consumes.value}'"]\n`
        } else {
          resultStr += `  "${transitionState.fromState}" -> "${toState}" [label="${consumes.value}"]\n`
        }
      }
    }
  }
  resultStr += '}'
  return resultStr
}

const saveContentToFile = (content, file) => {
  try {
    fs.writeFileSync(file, content)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

if (require.main === module) {
  const generate = async () => {
    let mainPath = path.dirname(require.main.filename)

    let wirthRule = process.env.WIRTH_RULE || process.argv[2]

    if (!wirthRule) {
      console.error(new Error('Nenhuma regra especificada'))
      process.exit(1)
    }

    console.log('#### WirthConverterCLI: CLI do gerador de automatos usado nos analisadores léxico e sintático ####')
    console.log(`Path       : ${mainPath}`)
    console.log(`Wirth Rule : ${wirthRule}`)

    const result = createAutomataFromWirthRule(wirthRule)
    result.automata = minimizeDFA(result.automata)
    const serializable = serializeAutomata(result.automata)

    const automatasFolder = path.join(mainPath, '..', 'automatas')
    if (!fs.existsSync(automatasFolder)) {
      fs.mkdirSync(automatasFolder)
    }

    const dotDiagram = convertSerializableToDotNotation(serializable)
    const dotDiagramFile = path.join(automatasFolder, `${result.name}.gz`)
    saveContentToFile(dotDiagram, dotDiagramFile)

    const imageContent = await download(dotDiagram)
    const imageDiagramFile = path.join(automatasFolder, `${result.name}.png`)
    saveContentToFile(imageContent, imageDiagramFile)
  }
  generate()
}

module.exports = {
  convertSerializableToDotNotation
}
