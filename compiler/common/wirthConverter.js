'use strict'
/*
  A fim de não gerar um arquivo de código muito grande, o conversor de Wirth foi dividido
  em 4 arquivos principais, separados por funcionalidade.

  - wirthConverter.js: Contém o código principal, responsável por orquestrar a separaração do
    array de wirth para tratar cada operação individualmente.
*/
/**
 * @typedef {import('./constants').WirthGroupPosition} WirthGroupPosition
 * @typedef {import('./constants').WirthElement} WirthElement
 * @typedef {import('./constants').WirthParserResult} WirthParserResult
 * @typedef {import('./constants').GenericAutomata} GenericAutomata
 * @typedef {import('./constants').NamedGenericAutomata} NamedGenericAutomata
 */

const { WIRTH_ELEMENT_CLASSES, WIRTH_GROUP_TYPES } = require('./constants')
const { createRuleArray } = require('./wirthRuleParser')
const {
  createAutomataFromConcatenationGroup,
  createAutomataFromAlternationGroup,
  handleAutomataFromRepetitionGroup,
  handleAutomataFromOptionalGroup,
  handleAutomataFromSelectionGroup
} = require('./wirthBlockConverter')
const { replaceListOfNt } = require('./wirthAutomataSimplifier')

/**
 * Returns the first innermost ocurrence of a group of wirth elements surrounded
 * by '{}' or '()' or '[]', or undefined. Ignores formed groups wirth element.
 * Array of elements must be have balanced and well formed brackets.
 * @param {WirthElement[]} elements
 * @returns {WirthGroupPosition}
 */
const findInnerGroup = (elements) => {
  const {
    LeftCurlyBracket, LeftParentheses, LeftSquareBracket,
    RightCurlyBracket, RightParentheses, RightSquareBracket
  } = WIRTH_ELEMENT_CLASSES

  const { CurlyBracketsGroup, SquareBracketsGroup, ParenthesesGroup } = WIRTH_GROUP_TYPES

  const leftfromRight = {
    RightCurlyBracket: LeftCurlyBracket,
    RightParentheses: LeftParentheses,
    RightSquareBracket: LeftSquareBracket
  }

  const groupTypeFromRight = {
    RightCurlyBracket: CurlyBracketsGroup,
    RightParentheses: ParenthesesGroup,
    RightSquareBracket: SquareBracketsGroup
  }

  let begin
  let end
  // Find the left-most rightbracket
  for (let i = elements.length - 1; i > -1; i--) {
    const element = elements[i]
    if ([RightCurlyBracket, RightParentheses, RightSquareBracket].includes(element.class)) {
      end = i
    }
  }
  if (!end) return undefined
  const leftPair = leftfromRight[elements[end].class]

  // Find the right-most leftbracket before the selected rightbracket
  for (let i = 0; i < end; i++) {
    const element = elements[i]
    if (leftPair === element.class) {
      begin = i
    }
  }

  return {
    begin,
    end,
    type: groupTypeFromRight[elements[end].class]
  }
}

/**
 * Returns a list of lists of wirth elements with only concatenation operations in between.
 * from Wirth simple rule array (no groups, only alternation and concatenation)
 * @param {WirthElement[]} ruleArray
 * @returns {WirthElement[][]}
 */
const splitIntoConcatenationGroups = ruleArray => {
  const { OrBar } = WIRTH_ELEMENT_CLASSES
  const concatenationGroups = []

  let concatenationGroup = []
  for (let i = 0; i < ruleArray.length; i++) {
    const element = ruleArray[i]
    if (element.class !== OrBar) {
      concatenationGroup.push(element)
    } else {
      concatenationGroups.push(concatenationGroup)
      concatenationGroup = []
    }
  }
  if (concatenationGroup.length > 0) concatenationGroups.push(concatenationGroup)

  return concatenationGroups
}

/**
 * Converts a simple array of wirth elements into automata.
 * Concatenation and alternation are allowed, but no groups.
 * @param {WirthElement[]} ruleArray
 * @returns {GenericAutomata}
 */
const simpleArrayIntoAutomata = ruleArray => {
  const concatenationGroups = splitIntoConcatenationGroups(ruleArray)
  // Todo: generate unique temp nt names to avoid conflict
  const baseName = '--AtUH9YfFNs0b2d8TG8btzxrdWvJy9ma8uSNJcFwK'
  const tempAutomatas = []
  const tempSimplifiedArray = []

  // Resolve all concatenation operations
  for (let i = 0; i < concatenationGroups.length; i++) {
    const concatenatedAutomata = createAutomataFromConcatenationGroup(concatenationGroups[i])
    const concatenatedAutomataName = i + baseName

    // Adds concatenated automata
    tempAutomatas.push({
      name: concatenatedAutomataName,
      automata: concatenatedAutomata
    })

    // Adds artificial NT element
    tempSimplifiedArray.push({
      class: WIRTH_ELEMENT_CLASSES.NonTerminal,
      value: concatenatedAutomataName
    })
  }

  // Resolves all alternation operations
  const singleAutomata = createAutomataFromAlternationGroup(tempSimplifiedArray)

  // Resolve all temp nt substitutions, reversed, since can be nested
  return replaceListOfNt(singleAutomata, [...tempAutomatas].reverse())
}

/**
 * Main function of wirth Converter.
 * Manages all logic of converting a Wirth rule.
 * Returns an finite state automata.
 * @param {String} ruleStr - Wirth rule string
 * @return {NamedGenericAutomata}
 */
const createAutomataFromWirthRule = ruleStr => {
  /**
   * @type {WirthElement[]}
  */
  let { ruleArray: wirthRuleArray, name } = createRuleArray(ruleStr)

  // Todo: generate unique temp nt names to avoid conflict
  const baseName = '--QhmNFoaFVEU1sfWd9MMzXSwUYeL73zEr2xhVv0kb'
  let i = 0
  let tempName
  /**
   * @type {NamedGenericAutomata[]}
   */
  const tempAutomatas = []

  let innerGroupAutomata
  let groupAutomata
  while (true) {
    // Vai substituindo todos grupos, adiciona os NT temporarios em tempAutomatas
    let res = findInnerGroup(wirthRuleArray)
    if (res) {
      innerGroupAutomata = simpleArrayIntoAutomata(wirthRuleArray.slice(res.begin + 1, res.end))
      switch (res.type) {
        case WIRTH_GROUP_TYPES.CurlyBracketsGroup:
          groupAutomata = handleAutomataFromRepetitionGroup(innerGroupAutomata)
          tempName = i + baseName
          tempAutomatas.push({
            name: tempName,
            automata: groupAutomata
          })
          i += 1
          wirthRuleArray = [
            ...wirthRuleArray.slice(0, res.begin),
            {
              value: tempName,
              class: WIRTH_ELEMENT_CLASSES.NonTerminal
            },
            ...wirthRuleArray.slice(res.end + 1)]
          break
        case WIRTH_GROUP_TYPES.ParenthesesGroup:
          groupAutomata = handleAutomataFromSelectionGroup(innerGroupAutomata)
          tempName = i + baseName
          tempAutomatas.push({
            name: tempName,
            automata: groupAutomata
          })
          i += 1
          wirthRuleArray = [
            ...wirthRuleArray.slice(0, res.begin),
            {
              value: tempName,
              class: WIRTH_ELEMENT_CLASSES.NonTerminal
            },
            ...wirthRuleArray.slice(res.end + 1)]
          break
        case WIRTH_GROUP_TYPES.SquareBracketsGroup:
          groupAutomata = handleAutomataFromOptionalGroup(innerGroupAutomata)
          tempName = i + baseName
          tempAutomatas.push({
            name: tempName,
            automata: groupAutomata
          })
          i += 1
          wirthRuleArray = [
            ...wirthRuleArray.slice(0, res.begin),
            {
              value: tempName,
              class: WIRTH_ELEMENT_CLASSES.NonTerminal
            },
            ...wirthRuleArray.slice(res.end + 1)]
          break
      }
    } else break
  }
  // Resolves the remaining alternations and concatenations
  const singleAutomata = simpleArrayIntoAutomata(wirthRuleArray)

  // Resolve all temp nt substitutions, reversed, since can be nested
  const finalAutomata = replaceListOfNt(singleAutomata, [...tempAutomatas].reverse())

  return {
    name,
    automata: finalAutomata
  }
}

module.exports = {
  findInnerGroup,
  splitIntoConcatenationGroups,
  createAutomataFromWirthRule
}
