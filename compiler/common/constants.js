'use strict'
/*
  Este arquivo armazena alguns 'enums' e jsdoc typedefs
  usados no projeto inteiro.
*/

/**
 * Enum for wirth element classes.
 * @readonly
 * @enum {String}
 */
const WIRTH_ELEMENT_CLASSES = {
  Terminal: 'Terminal',
  NonTerminal: 'NonTerminal',
  LeftParentheses: 'LeftParentheses',
  RightParentheses: 'RightParentheses',
  LeftSquareBracket: 'LeftSquareBracket',
  RightSquareBracket: 'RightSquareBracket',
  LeftCurlyBracket: 'LeftCurlyBracket',
  RightCurlyBracket: 'RightCurlyBracket',
  OrBar: 'OrBar',
  Empty: 'Empty',
  Unclassified: 'Unclassified'
}

const EMPTY_SYMBOL = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Empty })

/**
 * Enum for wirth elements groups.
 * @readonly
 * @enum {String}
 */
const WIRTH_GROUP_TYPES = {
  CurlyBracketsGroup: 'CurlyBracketsGroup',
  ParenthesesGroup: 'ParenthesesGroup',
  SquareBracketsGroup: 'SquareBracketsGroup'
}

/**
 * Enum for lexical elements groups.
 * @readonly
 * @enum {String}
 */
const ASCII_CLASSIFICATIONS = {
  control: 'control',
  delimiter: 'delimiter',
  special: 'special',
  digit: 'digit',
  letter: 'letter'
}

/**
 * @typedef {Object} CompilerClassification
 * @property {String} symbol Raw input
 * @property {String} classification Main classification
 * @property {String[]} equivalentClassifications Equivalent classifications
 * @property {Number} line line where input is positioned
 * @property {Number} column column where input begins
 * @property {CompilerClassification[]} children Sub classifications
 */

/**
 * @typedef {Object} LexicalToken
 * @property {String} name
 * @property {String} value
 * @property {Number} position
 */

/**
 * @typedef {Object} WirthElement
 * @property {String} value
 * @property {WIRTH_ELEMENT_CLASSES} class
 */

/**
 * @typedef {Object} WirthGroupPosition
 * @property {Number} begin
 * @property {Number} end
 * @property {WIRTH_GROUP_TYPES} type
 */

/**
 * @typedef {Object} SerializableGenericTransitionResult
 * @property {String} consumes
 * @property {String[]} toStates
 */

/**
 * @typedef {Object} SerializableGenericTransition
 * @property {String} fromState
 * @property {SerializableGenericTransitionResult[]} transitions
 */

/**
 * @typedef {Object} SerializableGenericAutomata
 * @property {String[]} alphabet
 * @property {String} initialState
 * @property {String[]} finalStates
 * @property {String[]} states
 * @property {SerializableGenericTransition[]} transitions
 * @property {Boolean} [isDFA]
 * @property {Boolean} [isMinimized]
 */

// 1. Use hashmap to get the properly transition for state qi
// 2. Use the result of JSON.stringify(wirthElement) as key for the input in the second hashmap.
// 3. Then, to get the nextState will be O(1) instead of O(n), using: transitions.get('A').get('{"class":"Terminal","value":"a"}') // => ['B', 'C']
/**
 * @typedef {Object} GenericAutomata
 * @property {Set<String>} alphabet - Each wirth element will be store as JSON.stringify(wirthElement), first class, then value.
 * @property {String} initialState
 * @property {Set<String>} finalStates
 * @property {Set<String>} states
 * @property {Map<String, Map<String, Set<String>>>} transitions - nextStateStr --> (inputStrValue --> nextStatesStr[])
 * @property {Boolean} [isDFA]
 * @property {Boolean} [isMinimized]
 */

/**
 * @typedef {Object} NamedGenericAutomata
 * @property {String} name
 * @property {GenericAutomata} automata
 */

/**
 * @typedef WirthParserResult
 * @property {WirthElement[]} ruleArray
 * @property {String} name
 */

// Definicoes de tipos para o motor de eventos

/**
 * @typedef {Object} EventEngineEvent
 * @property {String} type
 * @property {Object} context - Contexto do proprio evento
 */

/**
 * @typedef {Object} EventEngineContext
 * @property {EventEngineEvent[]} eventQueue
 * @property {Object} result
 * @property {Object} initialInput
 */

/**
 * @callback EventEngineEventHandler
 * @param {EventEngineEvent} event
 * @param {EventEngineContext} simulatorContext
 */

/**
 * @typedef {Object} EventEngineSimulatorInstance
 * @property {function():Number} next Funcao que executa proximo passo da simulacao, consumindo um evento da fila. Retorna o numero do passo.
 * @property {function():Number} step Retorna o numero do ultimo passo
 * @property {function():Object} result Retorna o objeto de resultado
 */

/**
 * @callback EventEngineSimulatorSetup Instancia um simulador com o objeto de entrada
 * @param {Object} input
 * @return {EventEngineSimulatorInstance}
 */

/**
 * @typedef {Object} EventEngineOptions
 * @property {function(EventEngineEvent[]):Object} pushInitialEvents
 * @property {function(EventEngineContext):Object} addToContext
 * @property {Map<String, EventEngineEventHandler>} handleRoutines
 */

module.exports = { WIRTH_ELEMENT_CLASSES, EMPTY_SYMBOL, WIRTH_GROUP_TYPES, ASCII_CLASSIFICATIONS }
