const https = require('https')
const qs = require('querystring')
const Stream = require('stream').Transform

const getGraphId = async (graphData) => {
  const data = qs.stringify({
    dot: graphData,
    method: 'dot'
  })

  const options = {
    hostname: 'graphs.grevian.org',
    path: '/graph',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': data.length
    }
  }

  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      resolve(res.headers.location.split('/').pop())
    })

    req.on('error', (err) => {
      reject(err)
    })

    req.write(data)
    req.end()
  })
}

const tryDownloadGraphImage = async (id) => {
  const options = {
    hostname: 'graphs.grevian.org',
    path: '/graph/images/' + id,
    method: 'GET'
  }
  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      let content = new Stream()
      res.on('data', (chunk) => {
        content.push(chunk)
      })
      res.on('end', () => {
        resolve(content.read())
      })
    })

    req.on('error', (err) => {
      reject(err)
    })
    req.end()
  })
}

const sleep = (ms) => {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

const downloadGraphImage = async (id) => {
  let result = null
  while (result === null) {
    console.log(`Trying to download: ${id}`)
    result = await tryDownloadGraphImage(id)
    sleep(2000)
  }
  return result
}

const download = async (graph) => {
  const id = await getGraphId(graph)
  console.log('Diagram image id  : ' + id)
  let result = await downloadGraphImage(id)
  console.log(`Download finished : ${id}`)
  return result
}

module.exports = { download }
