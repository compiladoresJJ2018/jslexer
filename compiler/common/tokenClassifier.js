'use strict'
/**
 * @typedef {import('./constants').EventEngineSimulatorSetup} EventEngineSimulatorSetup
 * @typedef {import('./constants').CompilerClassification} CompilerClassification
 */

/**
 * Faz uma classificação sobre items de uma lista usando um automato
 * @param {EventEngineSimulatorSetup} engine
 * @param {CompilerClassification[]} classifications
 */
const classificateWithSimulator = (engine, classifications) => {
  let finalClassifications = []
  let remainingClassifications = classifications
  while (remainingClassifications.length > 0) {
    const simInstance = engine(remainingClassifications)
    let step = simInstance.next()
    while (step) {
      step = simInstance.next()
    }
    const result = simInstance.result()
    finalClassifications = finalClassifications.concat(result.tokens[0])
    remainingClassifications = result.tokens.slice(1)
  }
  return finalClassifications
}

/** Lista de prioridade padrão onde Exp vem depois de Var */
const defaultPriorityList = [
  'Int', 'Num', 'Var', 'FunctionHeader', 'Exp', 'Pitem',
  'Reserved', 'IdentifierLetter', 'IdentifierWithDigit', 'EqualSign',
  'NewLine', 'digit', 'special', 'control', 'letter'
]

/** Lista de prioridade inversa onde Exp vem antes de Var */
const expPriorityList = [
  'Exp', 'Int', 'Num', 'Var',
  'Reserved', 'IdentifierLetter', 'IdentifierWithDigit', 'EqualSign',
  'NewLine', 'digit', 'special', 'control', 'letter'
]

/**
 * Faz uma recategorização apenas selecionando uma classificação equivalente
 * segundo uma lista de prioridades
 * @param {EventEngineSimulatorSetup} engine
 * @param {CompilerClassification[]} classifications
 */
const recategorizeTopClassification = (classifications, priorityList = defaultPriorityList) => {
  return classifications.map(c => {
    if (c.equivalentClassifications.length > 1) {
      c.equivalentClassifications.sort((classA, classB) => {
        return priorityList.indexOf(classA) - priorityList.indexOf(classB)
      })
      c.classification = c.equivalentClassifications[0]
      return c
    } else {
      return c
    }
  })
}
module.exports = {
  classificateWithSimulator,
  recategorizeTopClassification,
  defaultPriorityList,
  expPriorityList
}
