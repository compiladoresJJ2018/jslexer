'use strict'
/**
 * Este arquivo implementa um simulador de automatos, que faz a classificação léxica (transdutor),
 * utilizando um motor de eventos. Possui uma leve alteração que se não houver a transição válida, verifica se
 * possui a transição any_character (útil para REM, string)
 */

/**
 * @typedef {import('./constants').EventEngineEventHandler} EventEngineEventHandler
 * @typedef {import('./constants').EventEngineSimulatorSetup} EventEngineSimulatorSetup
 * @typedef {import('./constants').EventEngineOptions} EventEngineOptions
 * @typedef {import('./constants').NamedGenericAutomata} NamedGenericAutomata
 * @typedef {import('./constants').CompilerClassification} CompilerClassification
 */
const { WIRTH_ELEMENT_CLASSES } = require('./constants')
const eventEngine = require('./eventEngine')

// Faz o mapeamento de classificação léxica para classe (não terminal ou terminal)
const defaultClassificationToClass = new Map([
  ['control', WIRTH_ELEMENT_CLASSES.Terminal],
  ['digit', WIRTH_ELEMENT_CLASSES.Terminal],
  ['delimiter', WIRTH_ELEMENT_CLASSES.Terminal],
  ['letter', WIRTH_ELEMENT_CLASSES.Terminal],
  ['special', WIRTH_ELEMENT_CLASSES.Terminal]
])

// Faz mapeamento de estado final e última transição para classificação (Por default, assume o nome do automato)
/** @type {Map<String, Map<String, String>>} */
const defaultFinalStateToClassification = new Map()

/**
 * @typedef {Object} AutomataSimulatorOptions
 * @property {NamedGenericAutomata} namedAutomata
 * @property {NamedGenericAutomata[]} dependencyAutomatas
 * @property {Map<String, String>} classificationToClass
 * @property {Map<String, Map<String, String>>} finalStateToClassification
 */

/**
 * @param {AutomataSimulatorOptions}
 * @return {EventEngineOptions}
 */
const getCommonEventEngineOptions = ({ namedAutomata, dependencyAutomatas = [], classificationToClass = defaultClassificationToClass,
  finalStateToClassification = defaultFinalStateToClassification }) => {
  dependencyAutomatas.push(namedAutomata)
  return {
    pushInitialEvents: (eventQueue) => {
      eventQueue.push({
        type: 'Partida Inicial'
      })
    },
    handleRoutines: new Map([
      [
        'Partida Inicial',
        /** @type {EventEngineEventHandler} */
        (event, ctx) => {
          // Seta estado atual
          ctx.currentState = ctx.currentAutomata.automata.initialState // estado do automato
          ctx.eventQueue.push({ type: 'Leitura de Símbolo' })
        }
      ],
      [
        'Leitura de Símbolo',
        /** @type {EventEngineEventHandler} */
        (event, ctx) => {
          if (ctx.currentPosition < ctx.initialInput.length) {
            const currentElement = ctx.initialInput[ctx.currentPosition]
            const currentAutomata = ctx.currentAutomata.automata
            // Determina o símbolo para ser consumido na cadeia
            let consumes
            if (classificationToClass.get(currentElement.classification) === WIRTH_ELEMENT_CLASSES.Terminal) {
              consumes = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: currentElement.symbol })
            } else { // Se não for terminal, não lê valor pra transitar
              consumes = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: currentElement.classification })
            }

            // Determina proximo estado
            let nextState
            if (currentAutomata.transitions.get(ctx.currentState)) {
              // Tenta pegar proximo estado a partir da transicao do caractere
              nextState = (currentAutomata.transitions.get(ctx.currentState).get(consumes) || new Set())
                .values().next().value

              // Se não, verifica se existe alguma transição equivalente (Quando Var e Exp são igualmente válidas por exemplo)
              if (!nextState) {
                for (const equivalentClassification of currentElement.equivalentClassifications) {
                  const consumes = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: equivalentClassification })
                  nextState = (currentAutomata.transitions.get(ctx.currentState).get(consumes) || new Set())
                    .values().next().value
                  if (nextState) break
                }
              }
              // Se não, verifica se existe o wildcard anyCharacter como transição
              if (!nextState) {
                nextState = (currentAutomata.transitions.get(ctx.currentState).get(ctx.anyCharacter) || new Set())
                  .values().next().value
              }
              // Se não, verifica se no estado atual o autômato possui uma transição
              // correspondente a uma auto-recursão central, e evita uma recursão a esquerda
              // verificando se o estado atual não é o inicial
              if (!nextState) {
                const automatasWithConsume = dependencyAutomatas.map(a => {
                  return {
                    namedAutomata: a,
                    consumes: JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.NonTerminal, value: a.name })
                  }
                })
                const automataWithConsume = automatasWithConsume.filter(awc => currentAutomata.transitions.get(ctx.currentState).get(awc.consumes))[0]
                if (// ctx.currentState !== currentAutomata.initialState &&
                  automataWithConsume && currentAutomata.transitions.get(ctx.currentState).get(automataWithConsume.consumes)) {
                  // Empilha estado de retorna da recursão e transição anterior
                  // (para backtracking num transdutor com várias classificações)
                  ctx.recursionStack.push({
                    returnState: currentAutomata.transitions.get(ctx.currentState).get(automataWithConsume.consumes).values().next().value,
                    returnAutomata: ctx.currentAutomata,
                    lastTransition: currentAutomata.name
                  })
                  ctx.currentAutomata = automataWithConsume.namedAutomata
                  // Não faz a transição normal: dispara um evento de partida inicial e retorna
                  ctx.eventQueue.push({ type: 'Partida Inicial' })
                  return
                }
              }
            }
            // Se tiver um proximo estado dispara mais um evento de leitura de simbolo
            if (nextState) {
              ctx.currentState = nextState
              ctx.lastTransition = consumes
              ctx.currentPosition += 1
              ctx.eventQueue.push({ type: 'Leitura de Símbolo' })
            } else { // Senao, dispara um evento de fim de leitura
              ctx.eventQueue.push({ type: 'Fim da leitura' })
            }
          } else ctx.eventQueue.push({ type: 'Fim da leitura' })
        }
      ],
      [
        'Fim da leitura',
        /** @type {EventEngineEventHandler} */
        (event, ctx) => {
          const currentAutomata = ctx.currentAutomata.automata
          if (currentAutomata.finalStates.has(ctx.currentState)) { // Se for estado final
            // Se tiver algo na pilha, desempilha continuando a recursão
            if (ctx.recursionStack.length > 0) {
              const restore = ctx.recursionStack.pop()
              ctx.currentState = restore.returnState
              ctx.currentAutomata = restore.returnAutomata
              ctx.lastTransition = restore.lastTransition
              ctx.eventQueue.push({ type: 'Leitura de Símbolo' })
            // Se não, gera a classificação e concatena com o remanescente não classificado
            } else {
              const children = ctx.initialInput.slice(0, ctx.currentPosition)
              const { line, column } = ctx.initialInput[0]
              const classification = finalStateToClassification.get(ctx.currentState)
                ? finalStateToClassification.get(ctx.currentState).get(ctx.lastTransition) : ctx.currentAutomata.name
              // Se for de mesmo tamanho é classificação equivalente
              let equivalentClassifications = [ classification ]
              if (children.length === 1) equivalentClassifications = equivalentClassifications.concat(children[0].equivalentClassifications)
              const newToken = { classification, equivalentClassifications, symbol: children.map(s => s.symbol).join(''), line, column, children }
              ctx.result.tokens = [newToken, ...ctx.initialInput.slice(ctx.currentPosition)]
            }
          } else { // Se não, deixa inalterado rejeitando a cadeia
            ctx.result.tokens = ctx.initialInput
          }
        }
      ]
    ]),
    addToContext: (ctx) => {
      ctx.anyCharacter = JSON.stringify({ class: WIRTH_ELEMENT_CLASSES.Terminal, value: 'any_character' })
      ctx.recursionStack = []
      ctx.currentPosition = 0 // Posicao da entrada
      ctx.currentAutomata = namedAutomata
    }
  }
}

/**
 * Retorna um simulador de automato simples, nao recursivo.
 * @param {AutomataSimulatorOptions}
 * @returns {EventEngineSimulatorSetup}
 */
const getSimulatorFromAutomata = ({ namedAutomata, dependencyAutomatas = [], classificationToClass = defaultClassificationToClass,
  finalStateToClassification = defaultFinalStateToClassification }) => {
  return eventEngine(getCommonEventEngineOptions({ namedAutomata, dependencyAutomatas, classificationToClass, finalStateToClassification }))
}

module.exports = {
  getSimulatorFromAutomata,
  defaultClassificationToClass,
  defaultFinalStateToClassification
}
