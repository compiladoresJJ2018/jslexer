'use strict'
/**
 * @typedef {Object} PseudoInstruction
 * @property {'StaticDeclaration'|'PrintStatic'|'End'} type
 */

const isNum = (str) => /^((\d+)|(\d+\.\d*)|(\d*\.\d+))$/.test(str)
/**
 * Generates assembly code based on NASM with Intel MASM Syntax.
 * https://www.nasm.us/doc/nasmdoc3.html
 * @param {PseudoInstruction[]} statements
 * @returns {String[]} x86 instructions
 */
const generateX86NAsm = ({ statements, symbolTable }) => {
  // console.log(JSON.stringify(semanticResult, undefined, 2))
  // const strings = []

  // Tentativa de construir um binario sem usar um assembler externo
  // const program = require('./elfHeaders').getElfProgram([
  //   'B8', '01', '00', '00', '00', // mov eax, 1
  //   'BB', '2A', '00', '00', '00', // mov ebx, 42,
  //   'CD', '80' // int 0x80 (syscall)
  // ], true)
  // const dump = require('./hexDump').bytesStringToHexDumpString(program, false)
  // console.log(dump)
  // await require('./byteIO').saveAllBytes('testeeee', require('./hexDump').hexDumpStringToBytes(dump))

  let printStaticCount = 0

  let staticCount = 0
  let staticReadCount = 0

  const sectionDataPrefix = 'SECTION .DATA'
  const dataSection = []
  const vars = [...symbolTable].filter(t => !t.startsWith('label'))
  for (const symbol of vars) {
    dataSection.push(`    var${symbol.replace('-', '0')}: dd 0`)
  }
  const mainPrefix = ['SECTION .TEXT', '  GLOBAL _start', '', '_start:']
  const mainSection = []
  // for (const symbol of semanticResult.sy)
  for (const instruction of statements) {
    switch (instruction.type) {
      case 'StaticDeclaration':
        if (instruction.rawStatement.staticArea.length > 0) {
          for (const staticVar of instruction.rawStatement.staticArea) {
            if (staticVar.includes('.')) {
              throw new Error('Not implemented static for float')
            } else if (staticVar.includes('E')) {
              throw new Error('Not implemented static with E')
            } else {
              dataSection.push(`    static${staticCount}:   dd ${staticVar}`)
              staticCount += 1
            }
          }
        }
        break
      case 'AssignValue':
        if (instruction.rawStatement.value.includes('.')) {
          throw new Error('Not implemented assign value for float')
        } else if (instruction.rawStatement.value.includes('E')) {
          throw new Error('Not implemented assign value with E')
        } else {
          mainSection.push(`    mov eax, 42`)
          // mainSection.push(`\n    mov var${instruction.rawStatement.destination.replace('-', '0')}, [al]`)
        }
        break
      case 'AssignOp':
        // console.log(instruction)
        mainSection.push(isNum(instruction.rawStatement.operand1) ? `    mov eax, ${instruction.rawStatement.operand1}`
          : `    mov eax, [var${instruction.rawStatement.operand1.replace('-', '0')}]`)
        mainSection.push(isNum(instruction.rawStatement.operand2) ? `    mov ebx, ${instruction.rawStatement.operand2}`
          : `    mov ebx, [var${instruction.rawStatement.operand2.replace('-', '0')}]`)

        switch (instruction.rawStatement.operator) {
          case '+':
            mainSection.push('    add eax, ebx')
            break
          case '-':
            mainSection.push('    sub eax, ebx')
            break
          case '*':
            mainSection.push('    mul ebx')
            break
          case '/':
            // mainSection.push('    div ebx')
            break
          default:
            throw new Error('Not implemented AssignOp for ' + instruction.rawStatement.operator)
        }
        // mainSection.push(`    mov [var${instruction.rawStatement.destination.replace('-', '0')}], eax`)

        break
      case 'AssignStatic':
        // Try 1
        mainSection.push(`    mov al, [static${staticReadCount}]`)
        // mainSection.push(`    mov byte [var${instruction.rawStatement.destination.replace('-', '0')}], al`)
        // Try 2
        // mainSection.push(`    push [static${staticReadCount}]`)
        // mainSection.push(`    pop var${instruction.rawStatement.destination.replace('-', '0')}`)
        // Try 3
        // mainSection.push(`    mov al, [static${staticReadCount}]`)
        // mainSection.push(`    mov [var${instruction.rawStatement.destination.replace('-', '0')}], al`)
        // Try 4
        // mainSection.push(`    push static${staticReadCount}`)
        // mainSection.push(`    pop byte [var${instruction.rawStatement.destination.replace('-', '0')}]`)
        staticReadCount += 1
        break
      case 'PrintStatic':
        dataSection.push(`    printStatic${printStaticCount}:   dd '${instruction.rawStatement.value.slice(1, instruction.rawStatement.value.length - 1)}', 10`)
        dataSection.push(`    printStatic${printStaticCount}Len:    equ $-printStatic${printStaticCount}`)
        mainSection.push('    mov eax, 4') // Linux specific
        mainSection.push('    mov ebx, 1')
        mainSection.push(`    mov ecx, printStatic${printStaticCount}`)
        mainSection.push(`    mov edx, printStatic${printStaticCount}Len`)
        mainSection.push('    int 80h')
        printStaticCount += 1
        break
      case 'Print':
        if (isNum(instruction.rawStatement.value)) throw new Error('Not implemented print number')
        mainSection.push('    mov eax, 4') // Linux specific
        mainSection.push('    mov ebx, 1')
        mainSection.push('    mov ecx, al')
        mainSection.push('    mov edx, 1')
        mainSection.push('    int 80h')
        break
      case 'End':
        mainSection.push('    mov eax, 1') // Linux specific
        mainSection.push('    mov ebx, 0')
        mainSection.push('    int 80h')
        break
      default:
        console.log(instruction.type)
        break
    }
  }

  const dataSectionStr = [sectionDataPrefix, ...dataSection]
  // console.log(dataSectionStr)
  const mainSectionStr = [...mainPrefix, ...mainSection]

  const fullAsm = [...dataSectionStr, ...mainSectionStr]
  return fullAsm
}

module.exports = {
  generateX86NAsm,
  isNum
}
