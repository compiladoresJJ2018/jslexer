'use strict'
/**
 * Este arquivo faz a chamada para o assembler
 */

const { exec } = require('child_process')
const { output, writeOutput } = require('./config')
/**
 * @typedef {Object} ExecCmdReturn
 * @property {String} stout raw output from stdout
 * @property {String} stderr raw output from stderr
 */

/**
 * Exec command and returns a promise with the stdout, stderr.
 * @param {String} cmd
 * @returns {Promise<ExecCmdReturn>}
 */
const execCmd = async (cmd) => {
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        reject(error)
      }
      // console.log(stdout)
      resolve({ stdout, stderr })
    })
  })
}

const assemble = async (asm) => {
  writeOutput(asm, '.asm')
  await execCmd(`nasm -f elf64 ${output}.asm -o ${output}.o`)
  return execCmd(`ld ${output}.o -o ${output}`)
}

module.exports = {
  assemble
}
