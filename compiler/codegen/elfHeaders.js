'use strict'
/**
 * Este arquivo foi um esforço de engenharia reversa e estudo
 * da especificação do ELF (Executable and Linkable Format)
 * Links:
 * https://www.muppetlabs.com/~breadbox/software/tiny/teensy.html
 * http://www.sco.com/developers/gabi/latest/ch4.eheader.html#elfid
 * http://www.sco.com/developers/gabi/latest/ch5.pheader.html#p_flags
 *
 * Extraido de: /usr/include/linux/elf.h
 * typedef struct elf32_hdr{
 *   unsigned char e_ident[EI_NIDENT];
 *   Elf32_Half e_type;
 *   Elf32_Half e_machine;
 *   Elf32_Word e_version;
 *   Elf32_Addr e_entry;  // Entry point
 *   Elf32_Off  e_phoff;
 *   Elf32_Off  e_shoff;
 *   Elf32_Word e_flags;
 *   Elf32_Half e_ehsize;
 *   Elf32_Half e_phentsize;
 *   Elf32_Half e_phnum;
 *   Elf32_Half e_shentsize;
 *   Elf32_Half e_shnum;
 *   Elf32_Half e_shstrndx;
 * } Elf32_Ehdr;
 *
 * typedef struct elf64_hdr {
 *   unsigned char e_ident[EI_NIDENT]; // ELF "magic number"
 *   Elf64_Half e_type;
 *   Elf64_Half e_machine;
 *   Elf64_Word e_version;
 *   Elf64_Addr e_entry; // Entry point virtual address
 *   Elf64_Off e_phoff; // Program header table file offset
 *   Elf64_Off e_shoff; // Section header table file offset
 *   Elf64_Word e_flags;
 *   Elf64_Half e_ehsize;
 *   Elf64_Half e_phentsize;
 *   Elf64_Half e_phnum;
 *   Elf64_Half e_shentsize;
 *   Elf64_Half e_shnum;
 *   Elf64_Half e_shstrndx;
 * } Elf64_Ehdr;
 */

/*
  BITS 64
  org     0x08048000

  ehdr:                                                 ; Elf32_Ehdr
            db 0x7F, "ELF", 1, 1, 1, 0                  ;   e_ident
    times 8 db 0
            dw 2                                        ;   e_type
            dw 3                                        ;   e_machine
            dd 1                                        ;   e_version
            dd _start                                   ;   e_entry
            dd phdr - $$                                ;   e_phoff
            dd 0                                        ;   e_shoff
            dd 0                                        ;   e_flags
            dw ehdrsize                                 ;   e_ehsize
            dw phdrsize                                 ;   e_phentsize
            dw 1                                        ;   e_phnum
            dw 0                                        ;   e_shentsize
            dw 0                                        ;   e_shnum
            dw 0                                        ;   e_shstrndx
  ehdrsize      equ     $ - ehdr

  phdr:                                                 ; Elf32_Phdr
                dd      1                               ;   p_type
                dd      0                               ;   p_offset
                dd      $$                              ;   p_vaddr
                dd      $$                              ;   p_paddr
                dd      filesize                        ;   p_filesz
                dd      filesize                        ;   p_memsz
                dd      5                               ;   p_flags
                dd      0x1000                          ;   p_align

  phdrsize      equ     $ - phdr

  filesize      equ     $ - $$
*/

const { asciiToByteNumberArray, formatField } = require('./byteUtils')

// ELF Identification (e_ident): Identifies an ELF file, and provides some information about encoding, OS etc
const eiElfMagic = [ 0x7F, ...asciiToByteNumberArray('ELF') ] // EI_MAG: 4 bytes, identifying the file as an ELF object file
const eiClass = { // EI_CLASS: File class
  ELFCLASSNONE: 0, // Invalid class
  ELFCLASS32: 1, // 32-bit objects
  ELFCLASS64: 2 // 64-bit objects
}
const eiData = { // EI_DATA: Endianess
  ELFDATANONE: 0, // Invalid data encoding
  ELFDATA2LSB: 1, // Little endian (Normal for Intel x86-64)
  ELFDATA2MSB: 2 // Big endian (What processor/arch/os uses this?)
}
const eiVersion = { // EI_VERSION: Elf version (always EV_CURRENT)
  EV_NONE: 0, // Invalid version
  EV_CURRENT: 1 // Current version
}
const eiOsAbi = { // EI_OSABI: Identifica OS ou extensoes ABI especificas
  ELFOSABI_NONE: 0, // No extensions or unspecified
  ELFOSABI_HPUX: 1, // Hewlett-Packard HP-UX
  ELFOSABI_NETBSD: 2, // NetBSD
  ELFOSABI_GNU: 3, // GNU
  ELFOSABI_LINUX: 3, // Linux, historical - alias for ELFOSABI_GNU
  ELFOSABI_SOLARIS: 6, // Sun Solaris
  ELFOSABI_AIX: 7, // AIX
  ELFOSABI_IRIX: 8, // IRIX
  ELFOSABI_FREEBSD: 9, // FreeBSD
  ELFOSABI_TRU64: 10, // Compaq TRU64 UNIX
  ELFOSABI_MODESTO: 11, // Novell Modesto
  ELFOSABI_OPENBSD: 12, // Open BSD
  ELFOSABI_OPENVMS: 13, // Open VMS
  ELFOSABI_NSK: 14, // Hewlett-Packard Non-Stop Kernel
  ELFOSABI_AROS: 15, // Amiga Research OS
  ELFOSABI_FENIXOS: 16, // The FenixOS highly scalable multi-core OS
  ELFOSABI_CLOUDABI: 17, // Nuxi CloudABI
  ELFOSABI_OPENVOS: 18 // Stratus Technologies OpenVOS
  // 64-255 Architecture-specific value range
}
const eiAbiVersion = 0 // EI_ABIVERSION: Unspecified
const eiPad = 0 // EI_PAD

// e_type: 2 bytes, in big-endian
const eType = {
  ET_NONE: 0, // No file type
  ET_REL: 1, // Relocatable file
  ET_EXEC: 2, // Executable file
  ET_DYN: 3, // Shared object file
  ET_CORE: 4, // Core file
  ET_LOOS: 0xFE00, // Operating system-specific
  ET_HIOS: 0xFEFF, // Operating system-specific
  ET_LOPROC: 0xFF00, // Processor-specific
  ET_HIPROC: 0xFFFF // Processor-specific
}

// e_machine: 2 bytes, in big-endian
const eMachine = {
  EM_NONE: 0, // No machine
  EM_M32: 1, // AT&T WE 32100
  EM_SPARC: 2, // SPARC
  EM_386: 3, // Intel 80386
  EM_68K: 4, // Motorola 68000
  EM_88K: 5, // Motorola 88000
  EM_IAMCU: 6, // Intel MCU
  EM_860: 7, // Intel 80860
  EM_MIPS: 8, // MIPS I Architecture
  EM_S370: 9, // IBM System/370 Processor
  EM_MIPS_RS3_LE: 10, // MIPS RS3000 Little-endian
  // reserved  11-14  Reserved for future use
  EM_PARISC: 15, // Hewlett-Packard PA-RISC
  reserved: 16, // Reserved for future use
  EM_VPP500: 17, // Fujitsu VPP500
  EM_SPARC32PLUS: 18, // Enhanced instruction set SPARC
  EM_960: 19, // Intel 80960
  EM_PPC: 20, // PowerPC
  EM_PPC64: 21, // 64-bit PowerPC
  EM_S390: 22, // IBM System/390 Processor
  EM_SPU: 23, // IBM SPU/SPC
  // reserved  24-35  Reserved for future use
  EM_V800: 36, // NEC V800
  EM_FR20: 37, // Fujitsu FR20
  EM_RH32: 38, // TRW RH-32
  EM_RCE: 39, // Motorola RCE
  EM_ARM: 40, // ARM 32-bit architecture (AARCH34)
  EM_ALPHA: 41, // Digital Alpha
  EM_SH: 42, // Hitachi SH
  EM_SPARCV9: 43, // SPARC Version 9
  EM_TRICORE: 44, // Siemens TriCore embedded processor
  EM_ARC: 45, // Argonaut RISC Core, Argonaut Technologies Inc.
  EM_H8_300: 46, // Hitachi H8/300
  EM_H8_300H: 47, // Hitachi H8/300H
  EM_H8S: 48, // Hitachi H8S
  EM_H8_500: 49, // Hitachi H8/500
  EM_IA_64: 50, // Intel IA-64 processor architecture
  EM_MIPS_X: 51, // Stanford MIPS-X
  EM_COLDFIRE: 52, // Motorola ColdFire
  EM_68HC12: 53, // Motorola M68HC12
  EM_MMA: 54, // Fujitsu MMA Multimedia Accelerator
  EM_PCP: 55, // Siemens PCP
  EM_NCPU: 56, // Sony nCPU embedded RISC processor
  EM_NDR1: 57, // Denso NDR1 microprocessor
  EM_STARCORE: 58, // Motorola Star*Core processor
  EM_ME16: 59, // Toyota ME16 processor
  EM_ST100: 60, // STMicroelectronics ST100 processor
  EM_TINYJ: 61, // Advanced Logic Corp. TinyJ embedded processor family
  EM_X86_64: 62, // AMD x86-64 architecture
  EM_PDSP: 63, // Sony DSP Processor
  EM_PDP10: 64, // Digital Equipment Corp. PDP-10
  EM_PDP11: 65, // Digital Equipment Corp. PDP-11
  EM_FX66: 66, // Siemens FX66 microcontroller
  EM_ST9PLUS: 67, // STMicroelectronics ST9+ 8/16 bit microcontroller
  EM_ST7: 68, // STMicroelectronics ST7 8-bit microcontroller
  EM_68HC16: 69, // Motorola MC68HC16 Microcontroller
  EM_68HC11: 70, // Motorola MC68HC11 Microcontroller
  EM_68HC08: 71, // Motorola MC68HC08 Microcontroller
  EM_68HC05: 72, // Motorola MC68HC05 Microcontroller
  EM_SVX: 73, // Silicon Graphics SVx
  EM_ST19: 74, // STMicroelectronics ST19 8-bit microcontroller
  EM_VAX: 75, // Digital VAX
  EM_CRIS: 76, // Axis Communications 32-bit embedded processor
  EM_JAVELIN: 77, // Infineon Technologies 32-bit embedded processor
  EM_FIREPATH: 78, // Element 14 64-bit DSP Processor
  EM_ZSP: 79, // LSI Logic 16-bit DSP Processor
  EM_MMIX: 80, // Donald Knuth's educational 64-bit processor
  EM_HUANY: 81, // Harvard University machine-independent object files
  EM_PRISM: 82, // SiTera Prism
  EM_AVR: 83, // Atmel AVR 8-bit microcontroller
  EM_FR30: 84, // Fujitsu FR30
  EM_D10V: 85, // Mitsubishi D10V
  EM_D30V: 86, // Mitsubishi D30V
  EM_V850: 87, // NEC v850
  EM_M32R: 88, // Mitsubishi M32R
  EM_MN10300: 89, // Matsushita MN10300
  EM_MN10200: 90, // Matsushita MN10200
  EM_PJ: 91, // picoJava
  EM_OPENRISC: 92, // OpenRISC 32-bit embedded processor
  EM_ARC_COMPACT: 93, // ARC International ARCompact processor (old spelling/synonym: EM_ARC_A5)
  EM_XTENSA: 94, // Tensilica Xtensa Architecture
  EM_VIDEOCORE: 95, // Alphamosaic VideoCore processor
  EM_TMM_GPP: 96, // Thompson Multimedia General Purpose Processor
  EM_NS32K: 97, // National Semiconductor 32000 series
  EM_TPC: 98, // Tenor Network TPC processor
  EM_SNP1K: 99, // Trebia SNP 1000 processor
  EM_ST200: 100, // STMicroelectronics (www.st.com) ST200 microcontroller
  EM_IP2K: 101, // Ubicom IP2xxx microcontroller family
  EM_MAX: 102, // MAX Processor
  EM_CR: 103, // National Semiconductor CompactRISC microprocessor
  EM_F2MC16: 104, // Fujitsu F2MC16
  EM_MSP430: 105, // Texas Instruments embedded microcontroller msp430
  EM_BLACKFIN: 106, // Analog Devices Blackfin (DSP) processor
  EM_SE_C33: 107, // S1C33 Family of Seiko Epson processors
  EM_SEP: 108, // Sharp embedded microprocessor
  EM_ARCA: 109, // Arca RISC Microprocessor
  EM_UNICORE: 110, // Microprocessor series from PKU-Unity Ltd. and MPRC of Peking University
  EM_EXCESS: 111, // eXcess: 16/32/64-bit configurable embedded CPU
  EM_DXP: 112, // Icera Semiconductor Inc. Deep Execution Processor
  EM_ALTERA_NIOS2: 113, // Altera Nios II soft-core processor
  EM_CRX: 114, // National Semiconductor CompactRISC CRX microprocessor
  EM_XGATE: 115, // Motorola XGATE embedded processor
  EM_C166: 116, // Infineon C16x/XC16x processor
  EM_M16C: 117, // Renesas M16C series microprocessors
  EM_DSPIC30F: 118, // Microchip Technology dsPIC30F Digital Signal Controller
  EM_CE: 119, // Freescale Communication Engine RISC core
  EM_M32C: 120, // Renesas M32C series microprocessors
  // reserved  121-130  Reserved for future use
  EM_TSK3000: 131, // Altium TSK3000 core
  EM_RS08: 132, // Freescale RS08 embedded processor
  EM_SHARC: 133, // Analog Devices SHARC family of 32-bit DSP processors
  EM_ECOG2: 134, // Cyan Technology eCOG2 microprocessor
  EM_SCORE7: 135, // Sunplus S+core7 RISC processor
  EM_DSP24: 136, // New Japan Radio (NJR) 24-bit DSP Processor
  EM_VIDEOCORE3: 137, // Broadcom VideoCore III processor
  EM_LATTICEMICO32: 138, // RISC processor for Lattice FPGA architecture
  EM_SE_C17: 139, // Seiko Epson C17 family
  EM_TI_C6000: 140, // The Texas Instruments TMS320C6000 DSP family
  EM_TI_C2000: 141, // The Texas Instruments TMS320C2000 DSP family
  EM_TI_C5500: 142, // The Texas Instruments TMS320C55x DSP family
  EM_TI_ARP32: 143, // Texas Instruments Application Specific RISC Processor, 32bit fetch
  EM_TI_PRU: 144, // Texas Instruments Programmable Realtime Unit
  // reserved  145-159  Reserved for future use
  EM_MMDSP_PLUS: 160, // STMicroelectronics 64bit VLIW Data Signal Processor
  EM_CYPRESS_M8C: 161, // Cypress M8C microprocessor
  EM_R32C: 162, // Renesas R32C series microprocessors
  EM_TRIMEDIA: 163, // NXP Semiconductors TriMedia architecture family
  EM_QDSP6: 164, // QUALCOMM DSP6 Processor
  EM_8051: 165, // Intel 8051 and variants
  EM_STXP7X: 166, // STMicroelectronics STxP7x family of configurable and extensible RISC processors
  EM_NDS32: 167, // Andes Technology compact code size embedded RISC processor family
  EM_ECOG1: 168, // Cyan Technology eCOG1X family
  EM_ECOG1X: 168, // Cyan Technology eCOG1X family
  EM_MAXQ30: 169, // Dallas Semiconductor MAXQ30 Core Micro-controllers
  EM_XIMO16: 170, // New Japan Radio (NJR) 16-bit DSP Processor
  EM_MANIK: 171, // M2000 Reconfigurable RISC Microprocessor
  EM_CRAYNV2: 172, // Cray Inc. NV2 vector architecture
  EM_RX: 173, // Renesas RX family
  EM_METAG: 174, // Imagination Technologies META processor architecture
  EM_MCST_ELBRUS: 175, // MCST Elbrus general purpose hardware architecture
  EM_ECOG16: 176, // Cyan Technology eCOG16 family
  EM_CR16: 177, // National Semiconductor CompactRISC CR16 16-bit microprocessor
  EM_ETPU: 178, // Freescale Extended Time Processing Unit
  EM_SLE9X: 179, // Infineon Technologies SLE9X core
  EM_L10M: 180, // Intel L10M
  EM_K10M: 181, // Intel K10M
  // reserved: 182, // Reserved for future Intel use
  EM_AARCH64: 183, // ARM 64-bit architecture (AARCH64)
  // reserved: 184, // Reserved for future ARM use
  EM_AVR32: 185, // Atmel Corporation 32-bit microprocessor family
  EM_STM8: 186, // STMicroeletronics STM8 8-bit microcontroller
  EM_TILE64: 187, // Tilera TILE64 multicore architecture family
  EM_TILEPRO: 188, // Tilera TILEPro multicore architecture family
  EM_MICROBLAZE: 189, // Xilinx MicroBlaze 32-bit RISC soft processor core
  EM_CUDA: 190, // NVIDIA CUDA architecture
  EM_TILEGX: 191, // Tilera TILE-Gx multicore architecture family
  EM_CLOUDSHIELD: 192, // CloudShield architecture family
  EM_COREA_1ST: 193, // KIPO-KAIST Core-A 1st generation processor family
  EM_COREA_2ND: 194, // KIPO-KAIST Core-A 2nd generation processor family
  EM_ARC_COMPACT2: 195, // Synopsys ARCompact V2
  EM_OPEN8: 196, // Open8 8-bit RISC soft processor core
  EM_RL78: 197, // Renesas RL78 family
  EM_VIDEOCORE5: 198, // Broadcom VideoCore V processor
  EM_78KOR: 199, // Renesas 78KOR family
  EM_56800EX: 200, // Freescale 56800EX Digital Signal Controller (DSC)
  EM_BA1: 201, // Beyond BA1 CPU architecture
  EM_BA2: 202, // Beyond BA2 CPU architecture
  EM_XCORE: 203, // XMOS xCORE processor family
  EM_MCHP_PIC: 204, // Microchip 8-bit PIC(r) family
  EM_INTEL205: 205, // Reserved by Intel
  EM_INTEL206: 206, // Reserved by Intel
  EM_INTEL207: 207, // Reserved by Intel
  EM_INTEL208: 208, // Reserved by Intel
  EM_INTEL209: 209, // Reserved by Intel
  EM_KM32: 210, // KM211 KM32 32-bit processor
  EM_KMX32: 211, // KM211 KMX32 32-bit processor
  EM_KMX16: 212, // KM211 KMX16 16-bit processor
  EM_KMX8: 213, // KM211 KMX8 8-bit processor
  EM_KVARC: 214, // KM211 KVARC processor
  EM_CDP: 215, // Paneve CDP architecture family
  EM_COGE: 216, // Cognitive Smart Memory Processor
  EM_COOL: 217, // Bluechip Systems CoolEngine
  EM_NORC: 218, // Nanoradio Optimized RISC
  EM_CSR_KALIMBA: 219, // CSR Kalimba architecture family
  EM_Z80: 220, // Zilog Z80
  EM_VISIUM: 221, // Controls and Data Services VISIUMcore processor
  EM_FT32: 222, // FTDI Chip FT32 high performance 32-bit RISC architecture
  EM_MOXIE: 223, // Moxie processor family
  EM_AMDGPU: 224, // AMD GPU architecture
  // 225 - 242
  EM_RISCV: 243 // RISC-V
}
// e_version
const eVersion = { // 4 bytes
  EV_NONE: 0, // Invalid version
  EV_CURRENT: 1 // Current version
}

const specialSectionIndexes = {
  SHN_UNDEF: 0,
  SHN_LORESERVE: 0xff00,
  SHN_LOPROC: 0xff00,
  SHN_HIPROC: 0xff1f,
  SHN_LOOS: 0xff20,
  SHN_HIOS: 0xff3f,
  SHN_ABS: 0xfff1,
  SHN_COMMON: 0xfff2,
  SHN_XINDEX: 0xffff,
  SHN_HIRESERVE: 0xffff
}

const globalAddrOffset = 0x08048000

/**
 * Adds global offset
 * @param {Number} i index in byte array
 */
const computeEffectiveAddress = (i) => {
  return i + globalAddrOffset
}

// /**
//  * Get line address ($$ in MASM)
//  * @param {Number} i index in byte array
//  */
// const getLineAddress = (i) => {
//   return Math.floor(i / 16) * 16 + globalAddrOffset
// }

/**
 * Returns new byte array with replacement
 * @param {String[]} byteArr
 * @param {String[]} value
 * @param {Number} index
 * @param {Number} size
 */
const replaceAtIndex = (byteArr, value, index, size) => {
  return [ ...byteArr.slice(0, index), ...value, ...byteArr.slice(index + size) ]
}

/**
 * Returns a byte string array of an elf header, without section table
 * and some fields blanked.
 * @param {Number} addressSize 4 for 32 bits, 8 for 64 bits
 * @param {Number} eClassNum
 * @param {Number} eMachineNum
 */
const getElf = (addressSize = 4, eClassNum, eMachineNum) => {
  const endianess = eiData.ELFDATA2LSB
  const isBigEndian = endianess === eiData.ELFDATA2MSB

  // Creates elf header byte array
  let elfHeaderArr = [
    // First hexDump line (16 bytes): ei_ident
    ...eiElfMagic.map(byte => formatField(byte)[0]), // bytes 0-3, each byte a field
    ...formatField(eClassNum), // byte 4
    ...formatField(endianess), // byte 5
    ...formatField(eiVersion.EV_CURRENT), // byte 6
    ...formatField(eiOsAbi.ELFOSABI_NONE), // byte 7
    ...formatField(eiAbiVersion), // byte 8
    ...formatField(eiPad), // byte 9
    ...Array(6).fill('00'), // bytes 10-15

    // Second hexDump line (16 bytes)
    ...formatField(eType.ET_EXEC, 2, isBigEndian), // bytes 16-17
    ...formatField(eMachineNum, 2, isBigEndian), // bytes 18-19
    ...formatField(eVersion.EV_CURRENT, 4, isBigEndian), // bytes 20-23
    ...formatField(0, addressSize, isBigEndian) // bytes 24-27 '_start' // main vAddress
  ]
  const startAddressFieldIndex = elfHeaderArr.length - addressSize
  const startAddressFieldSize = addressSize

  // bytes 28-31, 'phoff', offset program header table from current address
  const phoffFieldSize = addressSize
  elfHeaderArr = elfHeaderArr.concat([
    ...formatField(0xFFFF, phoffFieldSize, isBigEndian)
  ])
  const phoffFieldIndex = elfHeaderArr.length - phoffFieldSize

  const shoffFieldSize = addressSize
  elfHeaderArr = elfHeaderArr.concat([
    // Third hexDump line (16 bytes)
    ...Array(shoffFieldSize).fill('00') // bytes 32-35, 'shoff', offset of section header table from current address (0 if dont have section)
  ])
  const shoffFieldIndex = elfHeaderArr.length - shoffFieldSize

  const ehSizeFieldSize = 2
  elfHeaderArr = elfHeaderArr.concat([
    ...Array(4).fill('00'), // bytes 36-39, 'e_flags'
    ...Array(ehSizeFieldSize).fill('00') // bytes 40-41, 'ehsize' placeholder, size of ELF header
  ])
  const ehSizeFieldIndex = elfHeaderArr.length - ehSizeFieldSize

  const phentsizeFieldSize = 2
  elfHeaderArr = elfHeaderArr.concat([
    ...formatField(0, phentsizeFieldSize, isBigEndian) // bytes 42-43, 'phentsize' placeholder, size of entry in program header table
  ])
  const phentsizeFieldIndex = elfHeaderArr.length - phentsizeFieldSize

  elfHeaderArr = elfHeaderArr.concat([
    ...formatField(1, 2, isBigEndian), // bytes 44-45, phnum, number of entries in program Header table (currently implementation will have only one)
    ...formatField(0, 2, isBigEndian), // bytes 46-47, 'shentsize', size of entry in section header table (currently implementation will have none)

    // Four hexDump line (16 bytes)
    ...formatField(0, 2, isBigEndian), // bytes 48-49, shnum, number of entries in section Header table (currently implementation will have none)
    ...formatField(specialSectionIndexes.SHN_UNDEF, 2, isBigEndian) // bytes 50-51, shstrndx, section header table index for string subtable (currently implementation will have none)
  ])

  // Replace ehsize placeholder
  elfHeaderArr = replaceAtIndex(elfHeaderArr, formatField(elfHeaderArr.length, ehSizeFieldSize, isBigEndian), ehSizeFieldIndex, ehSizeFieldSize)
  return {
    elfHeaderArr,
    startAddressFieldIndex,
    startAddressFieldSize,
    phoffFieldIndex,
    phoffFieldSize,
    shoffFieldIndex,
    shoffFieldSize,
    phentsizeFieldIndex,
    phentsizeFieldSize
  }
}

/**
 * Returns a byte array of an elf 32 header, without section table
 */
const getElf32 = () => {
  return getElf(4, eiClass.ELFCLASS32, eMachine.EM_386)
}

/**
 * Returns a byte array of an elf 64 header, without section table
 */
const getElf64 = () => {
  return getElf(8, eiClass.ELFCLASS64, eMachine.EM_X86_64)
}

// p_type
const programType = {
  PT_NULL: 0,
  PT_LOAD: 1,
  PT_DYNAMIC: 2,
  PT_INTERP: 3,
  PT_NOTE: 4,
  PT_SHLIB: 5,
  PT_PHDR: 6,
  PT_TLS: 7,
  PT_LOOS: 0x60000000,
  PT_HIOS: 0x6fffffff,
  PT_LOPROC: 0x70000000,
  PT_HIPROC: 0x7fffffff
}

// p_flags: permissions flags
const programFlags = {
  PF_X: 0x1, // Execute
  PF_W: 0x2, // Write
  PF_R: 0x4, // Read
  PF_MASKOS: 0x0ff00000, // Unspecified
  PF_MASKPROC: 0xf0000000 // Unspecified
}

/**
 * Returns an ELF compliant program header as byte string array format, with some metadata.
 * Os campos p_filesz e p_memz precisam ser preenchidos.
 * http://www.sco.com/developers/gabi/latest/ch5.pheader.html
 * @param {Number} addressSize 4 for 32 bits, 8 for 64 bits
 * @param {Number} vAddress
 */
const getProgramHeader = (addressSize = 4, vAddress = 0x08048000) => {
  const endianess = eiData.ELFDATA2LSB
  const isBigEndian = endianess === eiData.ELFDATA2MSB
  const pAlign = 0x1000
  // Constraint: vAddress = p_offset (mod pAlign)
  const phisicalAddress = vAddress // TODO: Testar com zero
  const filesMemAlignSize = addressSize === 4 ? 4 : 8
  let programHeaderArr = [
    ...formatField(programType.PT_LOAD, 4, isBigEndian), // 4 bytes, p_type: program type
    ...formatField(0, addressSize, isBigEndian), // p_offset: program offset
    ...formatField(vAddress, addressSize, isBigEndian), // p_vaddr: vAddress
    ...formatField(phisicalAddress, addressSize, isBigEndian), // p_paddr: phisical address
    ...formatField(0, filesMemAlignSize, isBigEndian) // p_filesz placeholder: number of bytes in the file image of the segment (precisa ter o tamanho do arquivo)
  ]
  // Salva o index do campo de tamanho de arquivo
  const pFileszIndex = programHeaderArr.length - filesMemAlignSize
  programHeaderArr = programHeaderArr.concat([
    ...formatField(0, filesMemAlignSize, isBigEndian) // p_memz placeholder: number of bytes in the memory image of the segment (precisa ter o tamanho do arquivo)
  ])
  // Salva o index do campo de tamanho de seção de memória
  const pMemzIndex = programHeaderArr.length - filesMemAlignSize
  programHeaderArr = programHeaderArr.concat([
    ...formatField(programFlags.PF_R + programFlags.PF_X, 4, isBigEndian), // p_flags: permission of read/execute dado ao codigo. Usuario nao deve escrever no codigo.
    ...formatField(pAlign, filesMemAlignSize, isBigEndian) // p_align
  ])
  return {
    programHeaderArr,
    pFileszIndex,
    pMemzIndex,
    filesMemAlignSize,
    vAddress
  }
}

/**
 * Retorna os headers ELF, Program concatenados com um programa, formando um ELF-32 executavel ou ELF-64 executavel
 * @param {String[]} program
 * @param {Boolean} is32 Se true, gera 32-bits, senao 64-bits
 * @returns {String[]}
 */
const getElfProgram = (program = [], is32 = true) => {
  let byteStrArray = []

  const elfHeader = is32 ? getElf32() : getElf64()
  let {
    startAddressFieldIndex,
    startAddressFieldSize,
    phentsizeFieldIndex,
    phentsizeFieldSize,
    phoffFieldIndex,
    phoffFieldSize,
    elfHeaderArr
  } = elfHeader

  let {
    programHeaderArr,
    pFileszIndex,
    pMemzIndex,
    filesMemAlignSize
  } = getProgramHeader(is32 ? 4 : 8)

  // Fill startAddress field at ELF header
  const startAddress = computeEffectiveAddress(elfHeaderArr.length + programHeaderArr.length)
  elfHeaderArr = replaceAtIndex(elfHeaderArr, formatField(startAddress, startAddressFieldSize), startAddressFieldIndex, startAddressFieldSize)

  // Fill program header size field at ELF header
  elfHeaderArr = replaceAtIndex(elfHeaderArr, formatField(programHeaderArr.length, phentsizeFieldSize), phentsizeFieldIndex, phentsizeFieldSize)

  // Fill program header offset field at ELF header
  elfHeaderArr = replaceAtIndex(elfHeaderArr, formatField(elfHeaderArr.length, phoffFieldSize), phoffFieldIndex, phoffFieldSize)

  // Adds both headers to result array, updating refering indexes
  byteStrArray = byteStrArray.concat(elfHeaderArr)
  const elfOffset = byteStrArray.length
  byteStrArray = byteStrArray.concat(programHeaderArr)
  pFileszIndex += elfOffset
  pMemzIndex += elfOffset

  // Adds program to array
  byteStrArray = byteStrArray.concat(program)
  const totalSize = byteStrArray.length

  // Fill pFilesZ field at program header
  byteStrArray = replaceAtIndex(byteStrArray, formatField(totalSize, filesMemAlignSize), pFileszIndex, filesMemAlignSize)

  // Fill pMemZ field at program header
  byteStrArray = replaceAtIndex(byteStrArray, formatField(totalSize, filesMemAlignSize), pMemzIndex, filesMemAlignSize)

  return byteStrArray
}

module.exports = {
  getElf32,
  getElf64,
  getProgramHeader,
  getElfProgram
}
