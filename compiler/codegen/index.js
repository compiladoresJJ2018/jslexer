
'use strict'

const { generateAssembly } = require('./generateAssembly')
const { options } = require('./config')
const { assemble } = require('./assemble')
const { printClassificationTable } = require('../common/printClassificationTable')
const doWork = async () => {
  try {
    console.time('Tempo total')
    const result = generateAssembly(options)
    console.timeEnd('Tempo total')
    console.log(`\nX86 ASM Result:\n`)
    const columns = [
      {
        name: 'Line',
        getValue: r => r.line + 1 + ''
      },
      {
        name: 'Statement',
        getValue: r => r.value
      }
    ]
    printClassificationTable(result.map((v, i) => { return { line: i, value: v } }), columns)
    await assemble(result.join('\n'))
  } catch (e) {
    console.error(e)
  }
}
doWork()
