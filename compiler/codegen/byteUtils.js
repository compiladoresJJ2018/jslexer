'use strict'
/**
 * Funcoes simples de formatacao e conversao de hex, ascii e decimais
 */

/**
 * Converts a hex string to a byte number
 * Ex: '7F'// => 127 (7 * 16 + 15)
 * @param {String} hex
 * @returns {Number}
 */
const hexToByte = (hex) => {
  return parseInt(hex.substr(0, 2), 16)
}

/**
 * Formats decimal number into byte string with any length
 *
 * Ex: 26, 2 // => '1A'
 *
 * Ex: 10, 1 // => 'A'
 *
 * Ex: 10, 2 // => '0A'
 * @param {Number} decimal
 * @param {Number} length Minimum length (Padding)
 * @returns {String}
 */
const byteFormat = (decimal, length = 2) => {
  let result = decimal.toString(16).toUpperCase()
  while (result.length < length) result = '0' + result
  return result
}

/**
 * Converts ASCII string into a byteString array (big-endian)
 *
 * Ex: 'E' // => ['45']
 *
 * Ex: 'L' // => ['4C']
 *
 * Ex: 'F' // => ['46']
 *
 * Ex: 'ELF' // => ['45', '4C', '46']
 * @param {String} asciiStr
 */
const asciiToByteFormatArray = (asciiStr) => {
  const byteStrArr = []
  for (const char of asciiStr) {
    byteStrArr.push(byteFormat(char.codePointAt()))
  }
  return byteStrArr
}

/**
 * Converts ASCII string into a byte number array (big-endian)
 * @param {String} asciiStr
 */
const asciiToByteNumberArray = (asciiStr) => {
  const result = []
  for (const char of asciiStr) {
    result.push(char.codePointAt())
  }
  return result
}

/**
 * Format number into array of bytes. Expect value at properly range.
 * Returns in little endian format (least significant byte order).

 * > formatField(127, 2) // => [ '7F', '00' ]

 * > formatField(127, 2, true) // => [ '00', '7F' ]

 * > formatField(16000, 2, true) // => [ '3E', '80' ]

 * > formatField(16000, 2) // => [ '80', '3E' ]

 * > formatField(127, 1) // => [ '7F' ]

 * > formatField(65535, 2) // => [ 'FF', 'FF' ]

 * > formatField(0xFFFF, 2) // => [ 'FF', 'FF' ]

  Example of value out of range of bytes:
 * > formatField(65536, 2) // => [ '00', '00', '01' ]
 * @param {Number} value
 * @param {Number} nBytes
 * @param {Boolean} isBigEndian
 */
const formatField = (value, nBytes, isBigEndian = false) => {
  const formatLength = nBytes * 2
  const rawField = byteFormat(value, formatLength)
  const bytesFormatted = []
  for (let i = rawField.length - 1; i > -1; i -= 2) {
    const firstNibble = rawField[i - 1] === undefined ? '0' : rawField[i - 1]
    const secondNibble = rawField[i]
    bytesFormatted.push(firstNibble + secondNibble)
  }
  if (isBigEndian) {
    return bytesFormatted.reverse()
  } else {
    return bytesFormatted
  }
}

module.exports = {
  hexToByte,
  byteFormat,
  asciiToByteFormatArray,
  asciiToByteNumberArray,
  formatField
}
