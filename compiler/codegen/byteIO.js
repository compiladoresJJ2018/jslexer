'use strict'
const fs = require('fs')

/**
 * Read binary to create byte array
 * @param {String} inputName
 * @returns {Number[]}
 */
const readAllBytes = async (inputName) => {
  return new Promise((resolve, reject) => {
    try {
      const byteArray = []
      fs.open(inputName, 'r', (err, fd) => {
        if (err) {
          throw err
        }
        let buffer = Buffer.alloc(1)
        while (true) {
          var num = fs.readSync(fd, buffer, 0, 1, null)
          if (num === 0) {
            break
          }
          byteArray.push(buffer[0])
        }
        resolve(byteArray)
      })
    } catch (error) {
      reject(error)
    }
  })
}

/**
 * Save byte array into file (creates binary)
 * @param {String} outputName
 * @param {Number[]} bytes
 */
const saveAllBytes = async (outputName, bytes) => {
  return new Promise((resolve, reject) => {
    try {
      fs.open(outputName, 'w', (err, fd) => {
        if (err) {
          throw err
        }
        let buffer = Buffer.from(bytes)
        fs.write(fd, buffer, 0, buffer.length, null, (err) => {
          if (err) throw err
          fs.close(fd, () => {
            resolve()
          })
        })
      })
    } catch (error) {
      reject(error)
    }
  })
}

module.exports = {
  readAllBytes,
  saveAllBytes
}
