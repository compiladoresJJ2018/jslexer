'use strict'

/**
 * @typedef {Object} HexDumpLine
 * @property {String} vAddress
 * @property {String[][]} qWords
 */

const {
  hexToByte,
  byteFormat
} = require('./byteUtils')

/**
 * Formats hexDumpLine object into properly string
 * @param {HexDumpLine} line
 * @returns {String}
 */
const formatDumpLine = line => {
  let dump = line.vAddress + ':'
  for (const qWord of line.qWords) {
    for (let i = 0; i < qWord.length; i += 2) {
      dump += ' ' + qWord[i] + qWord[i + 1]
    }
  }
  return dump
}

/**
 * Converts array of bytes into array of hexDumpLine objects
 * @param {Number[]} arr
 * @returns {HexDumpLine[]}
 */
const convertBytesIntoHexDumpLines = arr => {
  const dumpLines = []
  for (let i = 0; i < arr.length; i += 16) {
    const dumpLine = {
      vAddress: byteFormat(i, 8),
      qWords: [
        [
          byteFormat(arr[i], 2),
          byteFormat(arr[i + 1], 2),
          byteFormat(arr[i + 2], 2),
          byteFormat(arr[i + 3], 2),
          byteFormat(arr[i + 4], 2),
          byteFormat(arr[i + 5], 2),
          byteFormat(arr[i + 6], 2),
          byteFormat(arr[i + 7], 2)
        ],
        [
          byteFormat(arr[i + 8], 2),
          byteFormat(arr[i + 9], 2),
          byteFormat(arr[i + 10], 2),
          byteFormat(arr[i + 11], 2),
          byteFormat(arr[i + 12], 2),
          byteFormat(arr[i + 13], 2),
          byteFormat(arr[i + 14], 2),
          byteFormat(arr[i + 15], 2)
        ]
      ]
    }
    dumpLines.push(dumpLine)
  }
  return dumpLines
}

/**
 * Converts array of bytes strings into array of hexDumpLine objects
 * @param {String[]} arr
 * @returns {HexDumpLine[]}
 */
const convertBytesStringsIntoHexDumpLines = (arr, fillZeros = false) => {
  const dumpLines = []
  for (let i = 0; i < arr.length; i += 16) {
    const dumpLine = {
      vAddress: byteFormat(i, 8)
    }
    if (fillZeros) {
      dumpLine.qWords = [
        [
          arr[i + 0] || '00',
          arr[i + 1] || '00',
          arr[i + 2] || '00',
          arr[i + 3] || '00',
          arr[i + 4] || '00',
          arr[i + 5] || '00',
          arr[i + 6] || '00',
          arr[i + 7] || '00'
        ],
        [
          arr[i + 8] || '00',
          arr[i + 9] || '00',
          arr[i + 10] || '00',
          arr[i + 11] || '00',
          arr[i + 12] || '00',
          arr[i + 13] || '00',
          arr[i + 14] || '00',
          arr[i + 15] || '00'
        ]
      ]
    } else {
      dumpLine.qWords = [
        [
          arr[i + 0],
          arr[i + 1],
          arr[i + 2],
          arr[i + 3],
          arr[i + 4],
          arr[i + 5],
          arr[i + 6],
          arr[i + 7]
        ],
        [
          arr[i + 8],
          arr[i + 9],
          arr[i + 10],
          arr[i + 11],
          arr[i + 12],
          arr[i + 13],
          arr[i + 14],
          arr[i + 15]
        ]
      ]
    }
    dumpLines.push(dumpLine)
  }
  return dumpLines
}
/**
 * Converts byte array into hexDump string
 * @param {Number[]} bytes
 * @returns {String}
 */
const bytesToHexDumpString = (bytes) => {
  return convertBytesIntoHexDumpLines(bytes).map(formatDumpLine).join('\n')
}

/**
 * Converts byte string array into hexDump string
 * @param {Number[]} bytes
 * @returns {String}
 */
const bytesStringToHexDumpString = (bytes, fillZeros = false) => {
  return convertBytesStringsIntoHexDumpLines(bytes, fillZeros).map(formatDumpLine).join('\n')
}

/**
 * Converts hexDump string into byte array
 * @param {String} hexDump
 * @returns {Number[]}
 */
const hexDumpStringToBytes = (hexDump) => {
  return hexDump
    .split('\n')
    .filter(s => s !== '')
    .map(s => {
      const words = s.slice(10).split(' ')
      const bytesStr = []
      for (const word of words) {
        bytesStr.push(word.slice(0, 2))
        bytesStr.push(word.slice(2, 4))
      }
      return bytesStr
    })
    .reduce((p, c) => {
      return p.concat(c)
    }, [])
    .map(str => hexToByte(str))
}

module.exports = {
  hexDumpStringToBytes,
  bytesToHexDumpString,
  bytesStringToHexDumpString
}
