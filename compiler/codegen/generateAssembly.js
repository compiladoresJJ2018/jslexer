'use strict'

const { doSemanticAnalysis } = require('../semantic/semantic')
const { generateX86NAsm } = require('./nasm')
/**
 * Delega a geracao de assembly pro modulo correto
 * @param {*} options - Texto de entrada
 * @returns {String[]} assembly lines
 */
const generateAssembly = (options) => {
  const semanticResult = doSemanticAnalysis(options.inputContent)
  const asm = generateX86NAsm(semanticResult)
  return asm
}

module.exports = {
  generateAssembly
}
