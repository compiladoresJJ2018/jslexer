'use strict'

const path = require('path')
const fs = require('fs')

let mainPath = path.dirname(require.main.filename)

const MAJOR = 0
// 0 -> unfinished work
// 1 -> finished work
const MINOR = 4
// 1 -> adds lexer
// 2 -> adds parser
// 3 -> adds semantic
// 4 -> adds codegen

const getVersion = () => {
  const log = fs.readFileSync(path.join(mainPath, '../../.git/logs/HEAD')).toString()
  const rows = log.split('\n').slice(0, -1)
  let commitCount = 0
  for (const row of rows) {
    const category = row.split(' ')[5]
    if (category.includes('commit')) commitCount++
  }
  return `v${MAJOR}.${MINOR}.${commitCount}`
}

const getUserLanguage = () => {
  const envLocale = process.env.LC_ALL || process.env.LC_MESSAGES || process.env.LANG || process.env.LANGUAGE || 'en_US.UTF-8'
  if (envLocale.includes('en_')) return 'English'
  else if (envLocale.includes('pt_BR')) return 'Portuguese'
}

const helpText = {
  English: `
BasicCodeGen ${getVersion()}
A complete compiler for Dartmouth Basic Programming Language (1964, september edition)
Authors: @danielnorio, @tiagolimone

  Usage: node codegen [options] input[.bas]

  Options:
    -v, --version                 print BasicCodeGen version
    -h, --help                    print BasicCodeGen help page
    -w, --Wall                    enable all warnings during compilation (Currently not implemented) (Default: false)
    -d, --debug                   enable all debug messages during compilation (Currently not implemented) (Default: false)
    -d, --disassembly             outputs compilation in assembly language instead of object (Currently not implemented) (Default: false)
    -o2, --optimizations          enable all optimizations, currently, none (Default: false)
    -a, --arch string             set the output's archtecture ("mvn" | "arm7" | "x86") (Default: "x86"). Currently none were implemented.
    -o, --output string           path to output (Default is the same name as input, without extensions)

  Observations:
    The inputfile must be the name of the file in the inputs folder, or a relative path to file from inputs folder.
`,
  Portuguese: `
BasicCodeGen ${getVersion()}
Um compilador completo para a linguagem de programação Dartmouth Basic (Edição de setembro de 1964)
Autores: @danielnorio, @tiagolimone

  Uso: node codegen [Opções] input[.bas]

  Opções:
    -v, --version                 imprime a versão do BasicCodeGen
    -h, --help                    imprime a página de ajuda do BasicCodeGen
    -w, --Wall                    habilita todos avisos durante a compilação (Ainda não foi implementado) (Padrão: false)
    -d, --debug                   habilita todas mensagens de depuração durante a compilação (Ainda não foi implementado) (Padrão: false)
    -s, --disassembly             gera o resultado em linguagem de montagem invés de linguagem de máquina (Ainda não foi implementado) (Padrão: false)
    -o2, --optimizations          habilita todas otimizações, atualmente, nenhuma (Padrão: false)
    -a, --arch string             define a arquitetura de saída ("mvn" | "arm7" | "x86") (Padrão: "x86"). Atualmente nenhuma foi implementada.
    -o, --output string           diretório para saída (Padrão é o mesmo nome da entrada, sem extensões)

  Observações:
    O input precisa ser um nome de arquivo na pasta inputs, ou um caminho relativo para um arquivo a partir da pasta inputs.
`
}
const givenArgs = {
  disassembly: false,
  wall: false,
  debug: false,
  arch: undefined,
  optimizations: false,
  output: undefined
}

let arch = 'x86'
let input
let output
let lastOptionIndex = 1
for (let i = 0; i < process.argv.length; i++) {
  const arg = process.argv[i].trim()
  if (['-h', '--help'].includes(arg)) {
    const helpMsg = helpText[getUserLanguage()] ? helpText[getUserLanguage()] : helpText['English']
    console.log(helpMsg)
    process.exit(0)
  } else if (['-v', '--version'].includes(arg)) {
    console.log(getVersion())
    process.exit(0)
  } else if (['-w', '--wall'].includes(arg)) {
    givenArgs.wall = true
    lastOptionIndex = i
  } else if (['-d', '--debug'].includes(arg)) {
    givenArgs.debug = true
    lastOptionIndex = i
  } else if (['-s', '--disassembly'].includes(arg)) {
    givenArgs.disassembly = true
    lastOptionIndex = i
  } else if (['-o2', '-O2', '--optimizations'].includes(arg)) {
    givenArgs.optimizations = true
    lastOptionIndex = i
  } else if (['-a', '--arch'].includes(arg)) {
    givenArgs.arch = i
    lastOptionIndex = i + 1
  } else if (['-o', '--output'].includes(arg)) {
    givenArgs.output = i
    lastOptionIndex = i + 1
  }
}

if (givenArgs.arch > -1) {
  if (givenArgs.arch < process.argv.length - 1) {
    switch (process.argv[givenArgs.arch + 1]) {
      case 'arm7':
        arch = 'arm7'
        break
      case 'x86':
        arch = 'x86'
        break
      case 'mvn':
        arch = 'mvn'
        break
      default:
        console.error(new Error(`Arch ${process.argv[givenArgs.arch + 1]} is not a valid option, Valid options: arm7 | x86 | mvn`))
        process.exit(1)
    }
  } else {
    const parameter = process.argv[givenArgs.arch]
    console.error(new Error(`Missing string parameter for option ${parameter}. Example: node codegen ${parameter} mvn`))
    process.exit(1)
  }
}

const outputsFolder = path.join(mainPath, '..', 'outputs')
if (!fs.existsSync(outputsFolder)) {
  fs.mkdirSync(outputsFolder)
}
const inputsFolder = path.join(mainPath, '..', 'inputs')
if (!fs.existsSync(inputsFolder)) {
  fs.mkdirSync(inputsFolder)
}

if (givenArgs.output > -1) {
  if (givenArgs.output < process.argv.length - 1) {
    const givenOutput = process.argv[givenArgs.output + 1]
    if (givenOutput.includes('/')) {
      output = path.join(givenOutput)
    } else {
      output = path.join(mainPath, '../outputs/', givenOutput)
    }
  } else {
    const parameter = process.argv[givenArgs.output]
    console.error(new Error(`Missing string parameter for option ${parameter}.`))
    process.exit(1)
  }
}

if (lastOptionIndex < process.argv.length - 1) {
  if (lastOptionIndex < process.argv.length - 2) {
    console.warn(`Unknown option(s) ${process.argv.slice(lastOptionIndex + 1, process.argv.length - 1)}`)
  }
  const givenInput = process.argv[process.argv.length - 1]
  if (givenInput.includes('/')) {
    input = path.join(givenInput)
  } else {
    input = path.join(mainPath, '../inputs/', givenInput)
  }
} else {
  console.error(new Error('Missing input file arg. File must be in inputs folder, or must be absolute or relative path. Usage: node codegen filename'))
  process.exit(1)
}
if (!output) output = path.join(mainPath, '../outputs/', input.split('/').pop().split('.')[0])
let inputContent
let alternativeInput = input.includes('.bas') ? input.split('.bas')[0] : input + '.bas'
try {
  inputContent = fs.readFileSync(input, 'utf8')
} catch (err1) {
  try {
    inputContent = fs.readFileSync(alternativeInput, 'utf8')
    input = alternativeInput
  } catch (err2) {
    console.error(err1)
    process.exit(1)
  }
}
const options = { ...givenArgs, arch, input, output, inputContent }

console.log('#### Gerador de código objeto de BASIC ####')
console.log(`Input path  : ${options.input}`)
console.log(`Output path : ${options.output}`)
console.log(`Arch        : ${options.arch}`)
console.log(`Input text  :\n${options.inputContent}`)

/**
 * Function to properly handle the output data
 * @param {String} assembly
 */
const writeOutput = (assembly, extension = '') => {
  try {
    fs.writeFileSync(options.output + extension, assembly)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

module.exports = { writeOutput, options, output }
