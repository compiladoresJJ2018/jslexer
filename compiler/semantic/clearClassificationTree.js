'use strict'
/**
 * Este arquivo faz a limpeza da árvore de tokens sintática
 * (não confunda com árvore sintática, ela não foi construída neste programa).
 * Ordena, remove redundâncias obvias e remove classificações primitivas a fim
 * de fazer um processamento mais claro e simples no semântico.
 */

/**
 * Segundo o manual do BASIC, os statements podem estar em qualquer ordem.
 * O compilador original faz o ordenamento deles via o inteiro da linha.
 * @param {CompilerClassification[]} classifications
 */
const sortStatements = (classifications) => {
  return classifications.sort((classA, classB) => Number(classA.children[0].symbol) - Number(classB.children[0].symbol))
}

const flatClassification = classification => {
  if (!classification.children) return classification
  let pointer = classification
  let equivalentClassifications = classification.equivalentClassifications
  // Remove classificacoes equivalentes
  while (pointer.children.length === 1 && !['digit', 'special', 'letter', 'control'].includes(pointer.children[0].classification)) {
    pointer = pointer.children[0]
    equivalentClassifications = [...new Set(equivalentClassifications.concat(pointer.equivalentClassifications))]
  }
  classification.classification = pointer.classification
  classification.equivalentClassifications = equivalentClassifications
  classification.children = pointer.children
  if (classification.children.length > 0) {
    const newChildren = []
    // Aplica aos filhos
    for (const child of classification.children) {
      const newChild = flatClassification(child)
      newChildren.push(newChild)
    }
    classification.children = newChildren
  }
  return classification
}

/**
 * Remove classificações equivalentes intermediárias, a partir do abaixo de BStatement
 * até antes de digit, special, letter e control
 * @param {CompilerClassification[]} classifications
 */
const flattenClassifications = classifications => {
  for (const classification of classifications) {
    classification.children[0] = flatClassification(classification.children[0])
    for (let i = 0; i < classification.children[1].children.length; i++) {
      classification.children[1].children[i] = flatClassification(classification.children[1].children[i])
    }
  }
  return classifications
}

const removePrimitives = classification => {
  let primitiveChildren = false
  for (const child of classification.children) {
    if (['digit', 'special', 'letter', 'control'].includes(child.classification) && !child.children) {
      primitiveChildren = true
      break
    }
  }
  if (primitiveChildren) classification.children = []
  else {
    const newChildren = []
    for (const child of classification.children) {
      newChildren.push(removePrimitives(child))
    }
    classification.children = newChildren
  }
  return classification
}

const removePrimitiveClassifications = classifications => {
  const newClassifications = []
  for (const classification of classifications) {
    newClassifications.push(removePrimitives(classification))
  }
  return newClassifications
}

const clearClassificationTree = classifications => {
  classifications = sortStatements(classifications)
  classifications = flattenClassifications(classifications)
  classifications = removePrimitiveClassifications(classifications)
  return classifications
}

module.exports = { clearClassificationTree }
