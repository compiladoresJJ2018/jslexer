'use strict'
/**
 * Create intermediate code (structures) to assembler.
 */

/**
 * Pega um numero, e retorna uma string considerando os min e max codepoints
 * Ex: 'a', 'b', 'c' ... 'z', 'ba', 'bb', 'bc'... 'zz', 'bba', 'bbb'...
 * @param {Number} number
 */
const getStrFromNumber = (number, minCodePoint, maxCodePoint) => {
  let result = ''
  const baseN = maxCodePoint - minCodePoint + 1 // Faz uma "divisao" base N
  while (true) {
    const div = Math.floor(number / baseN)
    const rem = number % baseN
    number = div
    result = String.fromCodePoint(minCodePoint + rem) + result
    if (number === 0) break
  }
  return result
}
/**
 * Retorna um novo nome disponivel para uma tabela de simbolos
 * e adiciona esse nome na tabela de simbolos
 * @param {Set<String>} symbolTable
 * @param {String} prefix
 */
const getNextName = (symbolTable, prefix, minCodePoint, maxCodePoint) => {
  let i = 0
  let nextName = prefix + getStrFromNumber(i, minCodePoint, maxCodePoint)
  while (symbolTable.has(nextName)) {
    i += 1
    nextName = prefix + getStrFromNumber(i, minCodePoint, maxCodePoint)
  }
  symbolTable.add(nextName)
  return nextName
}

/**
 * Retorna um novo nome de variavel disponivel
 * @param {Set<String>} symbolTable
 */
const getNextVar = (symbolTable) => {
  const minCodePoint = 'a'.codePointAt()
  const maxCodePoint = 'z'.codePointAt()
  return getNextName(symbolTable, 'temp-', minCodePoint, maxCodePoint)
}

/**
 * Retorna um novo nome de variavel disponivel
 * @param {Set<String>} symbolTable
 */
const getNextLabel = (symbolTable) => {
  const minCodePoint = 'A'.codePointAt()
  const maxCodePoint = 'Z'.codePointAt()
  return getNextName(symbolTable, 'label', minCodePoint, maxCodePoint)
}

/**
 * Converte uma expressao postfix para uma lista de statements
 * @param {String[]} expArray
 * @param {Set<String>} symbolTable
 */
const createStatementsFromPostfixArray = (expArray, symbolTable, index) => {
  const tempStack = []
  const statements = []
  // Fix for expArray like ['3']
  if (expArray.length === 1) {
    statements.push({
      type: 'AssignValue',
      indexes: [ index ],
      destination: getNextVar(symbolTable),
      value: expArray[0]
    })
  } else if (expArray.length === 2 && !['+', '-', '/', '*'].includes(expArray[0]) && ['+', '-'].includes(expArray[1])) { // Fix for expArray like ['-', '2']
    statements.push({
      type: 'AssignValue',
      indexes: [ index ],
      destination: getNextVar(symbolTable),
      value: expArray[1] === '+' ? expArray[0] : expArray[1] + expArray[0]
    })
  } else {
    for (let i = 0; i < expArray.length; i++) {
      const element = expArray[i]
      if (['+', '-', '*', '/'].includes(element)) {
        let operand2 = tempStack.pop()
        let operand1 = tempStack.pop()
        // Se tiver com sobra de +, apenas seta o destino anterior como operando (Ex: +(+3))
        if (operand1 === undefined && ['+'].includes(element)) {
          tempStack.push(statements[statements.length - 1].destination)
        } else {
          if (operand1 === undefined) {
            operand1 = '0'
          }
          let temp = getNextVar(symbolTable)
          tempStack.push(temp)
          statements.push({
            type: 'Declare',
            indexes: [ index ],
            variable: temp
          })
          statements.push({
            type: 'AssignOp',
            indexes: [ index ],
            destination: temp,
            operand1,
            operand2,
            operator: element
          })
        }
      } else {
        tempStack.push(element)
      }
    }
  }
  return statements
}

/**
 * Converter um statement intermediario para string
 * @param {Object} stt
 */
const intermediateStatementToString = (stt) => {
  switch (stt.type) {
    case 'Declare':
      return `${stt.variable}`
    case 'AssignOp':
      return `${stt.destination} = ${stt.operand1} ${stt.operator} ${stt.operand2}`
    case 'AssignValue':
      return `${stt.destination} = ${stt.value}`
    case 'Assign':
      return `${stt.destination} = ${stt.origin}`
    case 'AssignStatic':
      return `Load Static to ${stt.destination}`
    case 'Print':
      return `Print variable: ${stt.value}`
    case 'PrintStatic':
      return `Print static string: ${stt.value}`
    case 'StaticDeclaration':
      return `Static: ${stt.staticArea.join(', ')}`
    case 'JumpNotZero':
      return `Jump if ${stt.variable} is not zero to ${stt.destination}`
    case 'JumpLowerOrEqualZero':
      return `Jump if ${stt.variable} is lower or equal zero to ${stt.destination}`
    case 'JumpGreaterOrEqualZero':
      return `Jump if ${stt.variable} is greater or equal zero to ${stt.destination}`
    case 'JumpLowerThanZero':
      return `Jump if ${stt.variable} is lower than zero to ${stt.destination}`
    case 'JumpGreaterThanZero':
      return `Jump if ${stt.variable} is greater than zero to ${stt.destination}`
    case 'JumpZero':
      return `Jump if ${stt.variable} is zero to ${stt.destination}`
    case 'Jump':
      return `Jump to ${stt.destination}`
    case 'Label':
      return ' '
    case 'PushReturnLabelToStack':
      return `Push return label ${stt.destination} to stack`
    case 'PopReturnLabelToStack':
      return `Pop return label from stack into ${stt.destination}`
    case 'End':
      return 'End program'
  }
}

/**
 * Gera um array de codigo intermediario
 * @param {Object[]} preProcessed
 * @param {Set<String>} symbolTable
 */
const generateIntermediateCode = (preProcessed, symbolTable) => {
  let statements = []
  // Processa o Data primeiro
  const datas = preProcessed.filter(p => p.statement === 'Data')
  const staticArea = []
  datas.map(d => {
    d.processed.values.map(v => {
      staticArea.push(v)
    })
  })
  statements.push({
    type: 'StaticDeclaration',
    staticArea,
    indexes: datas.map(d => d.index)
  })
  // Variaveis de controle de for-next loops e labels
  const forStack = [] // { processed, returnLabel,  }[]
  const labels = []
  // Processa o restante
  for (let i = 0; i < preProcessed.length; i++) {
    const p = preProcessed[i]
    let statementsFromPostfix, jumpLabel
    switch (p.statement) {
      case 'Assign':
        statements.push({
          type: 'Declare',
          indexes: [ p.index ],
          variable: p.processed.var
        })
        statementsFromPostfix = createStatementsFromPostfixArray(p.processed.postFix, symbolTable, p.index)
        if (statementsFromPostfix.length > 0) {
          symbolTable.delete(statementsFromPostfix[statementsFromPostfix.length - 1].destination)
          statementsFromPostfix[statementsFromPostfix.length - 1].destination = p.processed.var
          statements = statements.concat(statementsFromPostfix)
        } else {
          throw new Error('Nao caio mais aqui')
        }
        break
      case 'Read':
        p.processed.vars.map(v => {
          statements.push({
            type: 'AssignStatic',
            indexes: [ p.index ],
            destination: v
          })
        })
        break
      case 'Print':
        p.processed.map(pitem => {
          if (pitem.string) {
            statements.push({
              type: 'PrintStatic',
              indexes: [ p.index ],
              value: pitem.string
            })
          } else {
            statementsFromPostfix = createStatementsFromPostfixArray(pitem.postFix, symbolTable, p.index)
            if (statementsFromPostfix.length > 0) {
              if (['AssignValue', 'Assign'].includes(statementsFromPostfix[statementsFromPostfix.length - 1].type)) {
                statements = statements.concat(statementsFromPostfix.slice(0, statementsFromPostfix.length - 1))
                statements.push({
                  type: 'Print',
                  indexes: [ p.index ],
                  value: statementsFromPostfix[statementsFromPostfix.length - 1].value
                })
                symbolTable.delete(statementsFromPostfix[statementsFromPostfix.length - 1].destination)
              } else {
                statements = statements.concat(statementsFromPostfix)
                statements.push({
                  type: 'Print',
                  indexes: [ p.index ],
                  value: statementsFromPostfix[statementsFromPostfix.length - 1].destination
                })
              }
            } else {
              throw new Error('Nao caio mais aqui')
            }
          }
        })
        // statements.push({
        //   type: 'PrintStatic',
        //   indexes: [ p.index ],
        //   value: '"\\n"'
        // })
        break
      case 'For':
        statements.push({
          type: 'Declare',
          indexes: [ p.index ],
          variable: p.processed.control
        })
        statementsFromPostfix = createStatementsFromPostfixArray(p.processed.inferiorLimit, symbolTable, p.index)
        // Seta limite inferior
        if (statementsFromPostfix.length > 0) {
          symbolTable.delete(statementsFromPostfix[statementsFromPostfix.length - 1].destination)
          statementsFromPostfix[statementsFromPostfix.length - 1].destination = p.processed.control
          statements = statements.concat(statementsFromPostfix)
        } else {
          throw new Error('Nao caio mais aqui')
        }
        // Cria label e variavel de limite superior para comparar
        const returnLabel = getNextLabel(symbolTable)
        const endLabel = getNextLabel(symbolTable)
        // Computa limite superior
        statementsFromPostfix = createStatementsFromPostfixArray(p.processed.superiorLimit, symbolTable, p.index)
        statements.push({
          type: 'Label',
          indexes: [ p.index ],
          label: returnLabel
        })
        if (statementsFromPostfix.length > 0) {
          // Computa diferença
          let difVar
          if (['AssignValue', 'Assign'].includes(statementsFromPostfix[statementsFromPostfix.length - 1].type)) {
            statements = statements.concat(statementsFromPostfix.slice(0, statementsFromPostfix.length - 1))
            symbolTable.delete(statementsFromPostfix[statementsFromPostfix.length - 1].destination)
            difVar = getNextVar(symbolTable)
            statements.push({
              type: 'Declare',
              indexes: [ p.index ],
              variable: difVar
            })
            let operand2 = statementsFromPostfix[statementsFromPostfix.length - 1].value
            let operator = '-'
            if (operand2[0] === '-') {
              operand2 = operand2.slice(1)
              operator = '+'
            }
            statements.push({
              type: 'AssignOp',
              indexes: [ p.index ],
              destination: difVar,
              operand1: p.processed.control,
              operand2,
              operator
            })
          } else {
            statements = statements.concat(statementsFromPostfix)
            difVar = getNextVar(symbolTable)
            statements.push({
              type: 'Declare',
              indexes: [ p.index ],
              variable: difVar
            })
            statements.push({
              type: 'AssignOp',
              indexes: [ p.index ],
              destination: difVar,
              operand1: p.processed.control,
              operand2: statementsFromPostfix[statementsFromPostfix.length - 1].destination,
              operator: '-'
            })
          }
          // Adiciona jump statement
          statements.push({
            type: 'JumpZero',
            indexes: [ p.index ],
            destination: endLabel,
            variable: difVar
          })
        } else {
          throw new Error('Nao caio mais aqui')
        }
        // Adiciona para o stack de for
        forStack.push({
          processed: p.processed,
          returnLabel,
          endLabel,
          index: p.index
        })
        break
      case 'Next':
        const forLoop = forStack.pop()
        // Computa step
        if (forLoop.processed.step) {
          statementsFromPostfix = createStatementsFromPostfixArray(forLoop.processed.step, symbolTable, p.index)

          if (statementsFromPostfix.length > 0) {
            if (['AssignValue', 'Assign'].includes(statementsFromPostfix[statementsFromPostfix.length - 1].type)) {
              statements = statements.concat(statementsFromPostfix.slice(0, statementsFromPostfix.length - 1))
              let operand2 = statementsFromPostfix[statementsFromPostfix.length - 1].value
              let operator = '+'
              if (operand2[0] === '-') {
                operand2 = operand2.slice(1)
                operator = '-'
              }
              statements.push({
                type: 'AssignOp',
                indexes: [ p.index ],
                destination: forLoop.processed.control,
                operand1: forLoop.processed.control,
                operand2,
                operator
              })
              symbolTable.delete(statementsFromPostfix[statementsFromPostfix.length - 1].destination)
            } else {
              statements = statements.concat(statementsFromPostfix)
              statements.push({
                type: 'AssignOp',
                indexes: [ p.index ],
                destination: forLoop.processed.control,
                operand1: forLoop.processed.control,
                operand2: statementsFromPostfix[statementsFromPostfix.length - 1].destination,
                operator: '+'
              })
            }
          } else {
            throw new Error('Não caio mais aqui')
          }
        } else { // Se nao tiver step, soma com 1 (valor default segundo BASIC)
          statements.push({
            type: 'AssignOp',
            indexes: [ p.index ],
            destination: forLoop.processed.control,
            operand1: forLoop.processed.control,
            operand2: 1,
            operator: '+'
          })
        }
        // Desvio incondicional
        statements.push({
          type: 'Jump',
          indexes: [ p.index ],
          destination: forLoop.returnLabel
        })
        statements.push({
          type: 'Label',
          indexes: [ p.index ],
          label: forLoop.endLabel
        })
        break
      case 'If':
        jumpLabel = getNextLabel(symbolTable)
        labels.push({ numberLabel: p.processed.jumpTo, label: jumpLabel, originalLine: p.line })
        let leftSideVar, rightSideVar
        // Computa lado esquerdo
        statementsFromPostfix = createStatementsFromPostfixArray(p.processed.leftSide, symbolTable, p.index)
        if (statementsFromPostfix.length > 0) {
          leftSideVar = statementsFromPostfix[statementsFromPostfix.length - 1].destination
          statements = statements.concat(statementsFromPostfix)
        } else {
          throw new Error('Nao caio mais aqui')
        }
        // Computa lado direito
        statementsFromPostfix = createStatementsFromPostfixArray(p.processed.rightSide, symbolTable, p.index)
        if (statementsFromPostfix.length > 0) {
          rightSideVar = statementsFromPostfix[statementsFromPostfix.length - 1].destination
          statements = statements.concat(statementsFromPostfix)
        } else {
          throw new Error('Nao caio mais aqui')
        }
        // Calcula o comparisonVar
        const comparisonVar = getNextVar(symbolTable)
        statements.push({
          type: 'AssignOp',
          indexes: [ p.index ],
          destination: comparisonVar,
          operand1: leftSideVar,
          operand2: rightSideVar,
          operator: '-'
        })
        // Determina o tipo de jump
        let jumpType
        switch (p.processed.comparison) {
          case '<>':
            jumpType = 'JumpNotZero'
            break
          case '<=':
            jumpType = 'JumpLowerOrEqualZero'
            break
          case '>=':
            jumpType = 'JumpGreaterOrEqualZero'
            break
          case '<':
            jumpType = 'JumpLowerThanZero'
            break
          case '>':
            jumpType = 'JumpGreaterThanZero'
            break
          case '=':
            jumpType = 'JumpZero'
            break
        }
        statements.push({
          type: jumpType,
          indexes: [ p.index ],
          destination: jumpLabel,
          variable: comparisonVar
        })
        break
      case 'Gosub':
        const returnGosubLabel = getNextLabel(symbolTable)
        jumpLabel = getNextLabel(symbolTable)
        statements.push({
          type: 'PushReturnLabelToStack',
          indexes: [ p.index ],
          destination: returnGosubLabel
        })
        statements.push({
          type: 'Jump',
          indexes: [ p.index ],
          destination: jumpLabel
        })
        labels.push({
          numberLabel: preProcessed[i + 1] ? preProcessed[i + 1].numberLabel : Number(p.numberLabel) + 1 + '',
          label: returnGosubLabel,
          originalLine: p.line
        })
        labels.push({ numberLabel: p.processed.jumpTo, label: jumpLabel, originalLine: p.line })
        break
      case 'Return':
        const returnVar = getNextVar(symbolTable)
        statements.push({
          type: 'Declare',
          indexes: [ p.index ],
          variable: returnVar
        })
        statements.push({
          type: 'PopReturnLabelToStack',
          indexes: [ p.index ],
          destination: returnVar
        })
        statements.push({
          type: 'Jump',
          indexes: [ p.index ],
          destination: returnVar
        })
        break
      case 'End':
        statements.push({
          type: 'End',
          indexes: [ p.index ]
        })
    }
  }
  // Adiciona os labels criados por if's e gosubs
  for (const label of labels) {
    const indexes = preProcessed.filter(p => p.numberLabel === label.numberLabel)
    if (indexes.length === 0) {
      throw new Error(`Invalid label ${label.numberLabel} at line ${label.originalLine}`)
    }
    let index = indexes[0].index
    const statementPos = statements.indexOf(statements.filter(stt => stt.indexes.includes(index))[0])
    statements.splice(statementPos, 0, {
      type: 'Label',
      indexes: [ index ],
      label: label.label
    })
  }

  console.log('\nSymbol table:', symbolTable)
  return statements
}

module.exports = {
  generateIntermediateCode,
  intermediateStatementToString,
  createStatementsFromPostfixArray
}
