'use strict'

const path = require('path')
const fs = require('fs')

let mainPath = path.dirname(require.main.filename)
let inputName = process.env.INPUT_NAME || process.argv[2]

if (!inputName) {
  console.error(new Error('No input file specified'))
  process.exit(1)
}

const inputFile = path.join(mainPath, '..', 'inputs', inputName)

const partialsFolder = path.join(mainPath, '..', 'partials')
if (!fs.existsSync(partialsFolder)) {
  fs.mkdirSync(partialsFolder)
}
const outputFile = path.join(partialsFolder, `${inputName}.semanticResult.json`)

let inputContent
try {
  inputContent = fs.readFileSync(inputFile, 'utf8')
} catch (err) {
  console.error(err)
  process.exit(1)
}

console.log('#### Analisador semântico de BASIC ####')
console.log(`Path       : ${mainPath}`)
console.log(`Input      : ${inputName}`)
console.log(`Input text : ${inputContent}`)

/**
 * Function to properly handle the output data
 * @param {String[]} statementLines
 */
const writeOutput = (statementLines) => {
  try {
    fs.writeFileSync(outputFile, JSON.stringify(statementLines, undefined, 2))
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

module.exports = { inputContent, writeOutput }
