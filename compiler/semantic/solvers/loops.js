const { solveAnyExp, extractVars } = require('./exp')

const solveFor = (forExp) => {
  const solved = {
    control: forExp.children[1].symbol,
    inferiorLimit: solveAnyExp(forExp.children[3]),
    superiorLimit: solveAnyExp(forExp.children[5]),
    inferiorVars: extractVars(forExp.children[3]),
    superiorVars: extractVars(forExp.children[5]),
    stepVars: []
  }
  if (forExp.children.length === 8) {
    solved.step = solveAnyExp(forExp.children[7])
    solved.stepVars = extractVars(forExp.children[7])
  }
  return solved
}

const solveNext = (next) => {
  return {
    control: next.children[1].symbol
  }
}

const solveIf = (ifExp) => {
  return {
    leftSide: solveAnyExp(ifExp.children[1]),
    rightSide: solveAnyExp(ifExp.children[3]),
    leftSideVars: extractVars(ifExp.children[1]),
    rightSideVars: extractVars(ifExp.children[3]),
    jumpTo: ifExp.children[5].symbol,
    comparison: ifExp.children[2].symbol
  }
}

const solveGoSub = (exp) => {
  return {
    jumpTo: exp.children[1].symbol
  }
}

module.exports = {
  solveFor,
  solveNext,
  solveIf,
  solveGoSub
}
