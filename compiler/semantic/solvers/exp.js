// console.log('Teste infix2postfix')
// const strTeste = '(A + B) * C - (D - E) * (F + G)'
// const strTeste2 = 'A * B + C * D'
// const expArray3 = ['(', 'A', '-', 'sin', '(', 'B', ')', ')', '*', 'C']
// const expArray = strTeste.split('').filter(str => str !== ' ')
// const expArray2 = strTeste2.split('').filter(str => str !== ' ')
const opPrecedence = new Map([
    ['ABS', 5],
    ['ATN', 5],
    ['COS', 5],
    ['EXP', 5],
    ['INT', 5],
	['LOG', 5],
	['RND', 5],
	['SIN', 5],
	['SQR', 5],
	['TAN', 5],
	['+', 1],
	['-', 2],
	['*', 3],
	['/', 4]
])

/**
 * Convert an expression in infix to postfix notation
 * @param {String[]} expArray
 */
const infixToPostfix = (expArray) => {
  const result = []
  const opStack = []
    for (let i = 0; i < expArray.length; i++) {
        const element = expArray[i]
        if (element === '(') {
            opStack.push('(')
        } else if (element === ')') {
            let op = opStack.pop()
            while (op !== '(') {
                result.push(op)
                op = opStack.pop()
            }
        } else if (['+', '-', '*', '/', 'ABS', 'ATN', 'COS', 'EXP', 'INT', 'LOG', 'RND', 'SIN', 'SQR', 'TAN'].includes(element)) {
            let op = opStack.pop()
            while (['+', '-', '*', '/', 'ABS', 'ATN', 'COS', 'EXP', 'INT', 'LOG', 'RND', 'SIN', 'SQR', 'TAN'].includes(op) && opPrecedence.get(element) <= opPrecedence.get(op)) {
                result.push(op)
                op = opStack.pop()
            }
            opStack.push(op)
            opStack.push(element)
       } else { // Operand
        result.push(element)
        }
  }
  let remaining = opStack.pop()
  while (remaining) {
	result.push(remaining)
	remaining = opStack.pop()
  }
  return result
}

const solveAnyExp = (exp) => {
  let expArray
  if (exp.children && exp.children.length > 0) {
	expArray = exp.children.map(c => c.symbol)
  } else {
	expArray = [ exp.symbol ]
	}
  return infixToPostfix(expArray)
}

const extractVars = (exp) => {
  let vars = []
  if (exp.children && exp.children.length > 0) {
	for (const varr of exp.children) {
	  if (['IdentifierWithDigit', 'IdentifierLetter'].includes(varr.classification)) {
		vars.push(varr.symbol)
	  }
	}
  } else {
	if (['IdentifierWithDigit', 'IdentifierLetter'].includes(exp.classification)) {
	  vars.push(exp.symbol)
	}
  }
  return [...new Set(vars)]
}

module.exports = {
  infixToPostfix, solveAnyExp, extractVars
}
