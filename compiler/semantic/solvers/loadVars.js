const { solveAnyExp, extractVars } = require('./exp')

const solveAssign = (assign) => {
  return {
    var: assign.children[1].symbol,
    postFix: solveAnyExp(assign.children[3]),
    loadVars: extractVars(assign.children[3])
  }
}

const solveRead = (read) => {
  const vars = []
  for (let i = 1; i < read.children.length; i += 2) {
    vars.push(read.children[i].symbol)
  }
  return {
    vars
  }
}

const solveData = (data) => {
  const values = data.children.slice(1).reduce((p, c) => {
    if (c.symbol !== ',') {
      if (p.length === 0) {
        p.push(c.symbol)
      } else {
        p[p.length - 1] += c.symbol
      }
    } else {
      p.push('')
    }
    return p
  }, [])
  return {
    values
  }
}

const solvePrint = (print) => {
  const pitems = []
  for (let i = 1; i < print.children.length; i++) {
    const pitem = print.children[i]
    switch (pitem.classification) {
      case 'String':
        pitems.push({
          string: pitem.symbol,
          vars: []
        })
        break
      case 'PontuationSign':
        break
      default:
        pitems.push({
          postFix: solveAnyExp(pitem),
          vars: extractVars(pitem)
        })
    }
  }
  return pitems
}

module.exports = {
  solveAssign,
  solveRead,
  solveData,
  solvePrint
}
