'use strict'
/*
  Este arquivo é o arquivo principal do analisador semântico.
*/
/** @typedef {import('../common/constants').CompilerClassification} CompilerClassification */

const { doSintaticalAnalysis, checkErrors } = require('../parser/parser')
const { clearClassificationTree } = require('./clearClassificationTree')
const { solveAssign, solveData, solveRead, solvePrint } = require('./solvers/loadVars')
const { solveFor, solveNext, solveIf, solveGoSub } = require('./solvers/loops')
const { generateIntermediateCode, intermediateStatementToString } = require('./generateIntermediateCode')
/**
 * Identify well formed FOR...NEXT classifications
 * @param {CompilerClassification[]} classifications
 */
const identifyWellFormedGroups = classifications => {
  const stackForNext = []
  const BStatementGroups = []
  for (let i = 0; i < classifications.length; i++) {
    const classification = classifications[i]
    if (classification.children[1].classification === 'For') {
      const controlVar = classification.children[1].children[1]
      stackForNext.push({ var: controlVar, index: i })
    }
    if (classification.children[1].classification === 'Next') {
      const nextControlVar = classification.children[1].children[1]
      const forControlVar = stackForNext.pop()
      if (forControlVar.var.symbol !== nextControlVar.symbol) {
        const symbol = classification.children[1].children[1].symbol
        const line = classification.children[1].children[1].line
        const column = classification.children[1].children[1].column
        throw new Error(`Missing FOR statement for NEXT control variable ${symbol} at ${line}:${column}`)
      }
      BStatementGroups.push({ begin: forControlVar.index, end: i, controlVar: nextControlVar.symbol })
    }
  }
  if (stackForNext.length > 0) {
    throw new Error(`Missing NEXT statement for FOR control variable at ${stackForNext[0].line}:${stackForNext[0].column}`)
  }
  return BStatementGroups
}

// /**
//  * Verifica se um escopo pode conter um filho e adiciona
//  * Retorna true se adicionou
//  * @param {*} scopeRoot
//  * @param {*} scopeDescent
//  */
// const containScope = (scopeRoot, scopeDescent) => {
//   if (scopeDescent.begin > scopeRoot.begin && scopeDescent.end < scopeRoot.end) {
//     for (const child in scopeRoot.children) {
//       if (containScope(child, scopeDescent)) return true
//     }
//     scopeRoot.children.push(scopeDescent)
//     return true
//   } else return false
// }

// /**
//  * Separa os escopos de for-next
//  * @param {Object[]} wellFormedGroups
//  */
// const splitIntoNestedScopes = (wellFormedGroups) => {
//   const scopes = []
//   for (let i = wellFormedGroups.length - 1; i > -1; i--) {
//     const groupToAdd = wellFormedGroups[i]
//     let added = false
//     for (let j = 0; j < scopes.length; j++) {
//       const addedScope = scopes[j]
//       if (containScope(addedScope, groupToAdd)) {
//         added = true
//         break
//       }
//     }
//     if (!added) scopes.push({ ...groupToAdd, children: [] })
//   }
//   return scopes
// }

/**
 * Seta um valor minimo para a str fornecida
 * @param {Map<String, Number>} map
 * @param {String} str
 * @param {Number} num
 */
const setMin = (map, str, num, debugStr) => {
  if (!map.has(str)) {
    map.set(str, num)
    // console.log(`Setou ${debugStr}: ${str} ${num}`)
  } else {
    const currentValue = map.get(str)
    if (currentValue > num) {
      map.set(str, num)
      // console.log(`Setou ${debugStr}: ${str} ${num}`)
    }
  }
  return map
}

/**
 * Resolve e extrai propriedades especificas de cada statement
 * Retorna uma lista heterogenea e uma tabela de simbolos.
 * Joga erro ao identificar emprego de variaveis nao declaradas
 * Obs: Todas variaveis estao num namespace global, nao ha separacao por escopo.
 * @param {CompilerClassification[]} classifications
 */
const solveAndExtract = (classifications) => {
  const preProcessed = []
  const declaredVars = new Map()
  const loadVars = new Map()
  let nDataVars = 0
  let nReadVars = 0
  for (let i = 0; i < classifications.length; i++) {
    const c = classifications[i].children[1]
    let processed

    switch (c.classification) {
      case 'Assign':
        processed = solveAssign(c)
        preProcessed.push({
          statement: 'Assign',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        setMin(declaredVars, processed.var, i, 'declaredVars')
        processed.loadVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        break
      case 'Read':
        processed = solveRead(c)
        preProcessed.push({
          statement: 'Read',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        nReadVars += processed.vars.length
        processed.vars.map(v => setMin(declaredVars, v, i, 'declaredVars'))
        break
      case 'Data':
        processed = solveData(c)
        preProcessed.push({
          statement: 'Data',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        nDataVars += processed.values.length
        break
      case 'Print':
        processed = solvePrint(c)
        preProcessed.push({
          statement: 'Print',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        processed.map(pitem => pitem.vars.map(v => setMin(loadVars, v, i, 'loadVars')))
        break
      case 'For':
        processed = solveFor(c)
        preProcessed.push({
          statement: 'For',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        setMin(declaredVars, processed.control, i, 'declaredVars')
        processed.inferiorVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        processed.superiorVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        processed.stepVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        break
      case 'Next':
        processed = solveNext(c)
        preProcessed.push({
          statement: 'Next',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        setMin(loadVars, processed.control, i, 'loadVars')
        break
      case 'If':
        processed = solveIf(c)
        preProcessed.push({
          statement: 'If',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        processed.leftSideVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        processed.rightSideVars.map(v => setMin(loadVars, v, i, 'loadVars'))
        break
      case 'Gosub':
        processed = solveGoSub(c)
        preProcessed.push({
          statement: 'Gosub',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol,
          processed
        })
        break
      case 'Return':
        preProcessed.push({
          statement: 'Return',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol
        })
        break
      case 'End':
        preProcessed.push({
          statement: 'End',
          index: i,
          line: c.line,
          numberLabel: classifications[i].children[0].symbol
        })
        break
    }
  }

  for (const loadVar of loadVars.keys()) {
    if (!declaredVars.has(loadVar)) {
      const line = classifications[loadVars.get(loadVar)].line
      throw new Error(`Undeclared variable ${loadVar} at line ${line}`)
    } else {
      const declaredVarIndex = declaredVars.get(loadVar)
      const loadVarIndex = loadVars.get(loadVar)
      const line = classifications[loadVars.get(loadVar)].line
      if (loadVarIndex <= declaredVarIndex) {
        throw new Error(`Variable ${loadVar} was used before it's declaration at line ${line}`)
      }
    }
  }
  if (nReadVars > nDataVars) {
    throw new Error(`Number of read variables (${nReadVars}) unmatch number of data (${nDataVars})`)
  }
  return {
    classifications,
    symbolTable: new Set([...declaredVars.keys()]),
    declaredVars,
    loadVars,
    preProcessed
  }
}

/**
 * Faz a análise semântica
 * @param {String} inputText - Texto de entrada
 */
const doSemanticAnalysis = (inputText) => {
  let classifications = doSintaticalAnalysis(inputText)
  checkErrors(classifications)
  classifications = clearClassificationTree(classifications)
  identifyWellFormedGroups(classifications)
  const solveExtractResult = solveAndExtract(classifications)
  const { symbolTable, preProcessed } = solveExtractResult
  const statements = generateIntermediateCode(preProcessed, symbolTable)
  return {
    statements: statements.map(stt => {
      return {
        type: stt.type,
        statement: intermediateStatementToString(stt),
        origin: stt.indexes.map(i => classifications[i].symbol).join('\n'),
        label: stt.label || '',
        rawStatement: stt
      }
    }).filter(t => t.type !== 'Declare'),
    symbolTable
  }
}

module.exports = {
  doSemanticAnalysis
}
