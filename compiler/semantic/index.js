
'use strict'

const { doSemanticAnalysis } = require('./semantic')
const { inputContent, writeOutput } = require('./config')
const { printClassificationTable } = require('../common/printClassificationTable')
try {
  console.time('Tempo total')
  const result = doSemanticAnalysis(inputContent).statements
  console.timeEnd('Tempo total')
  console.log(`\nSemantic Result:\n\n`)
  const columns = [
    {
      name: 'Parser Origin  ',
      getValue: r => r.origin || []
    },
    {
      name: 'Type',
      getValue: r => r.type
    },
    {
      name: 'Label',
      getValue: r => r.label
    },
    {
      name: 'Statement',
      getValue: r => r.statement
    }
  ]
  printClassificationTable(result, columns)
  writeOutput(result)
} catch (e) {
  console.error(e)
}
