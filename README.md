## Compilador de BASIC
[![version][version-badge]][CHANGELOG]
![version][coverage-badge]

Compilador para a linguagem BASIC (edição de 1964).

### Instruções Lexer
Você precisa de um dos seguintes programas instalados em seu ambiente: NodeJS ou Docker.
Não é necessário ambos.
Faça as seguintes instruções dentro da pasta compiler.

#### 1. Opção de execução no host (NodeJS)
Coloque o arquivo de entrada na pasta lexer (Eg: `blubblesort.bas`).
Execute o analisador léxico em qualquer terminal (CMD no Windows, bash, sh ou zsh no Linux) na pasta lexer da seguinte forma:
```
node lexer bubblesort.bas
```
Observe a saída.

#### 2. Opção de execução em ambiente de container (Docker) (Recomendado)
Execute o arquivo `start.sh`:
```
./start.sh bubblesort.bas
```

#### 3. Testes
Execute o arquivo `test.sh`. 
Se você tiver uma instância local do SonarQube em execução, o relatório gerado será enviado para lá.
Senão, ocorrerá um erro. Todavia, mesmo com o erro é possível ver o relatório do nyc e o resultado do mocha no terminal.

### Instruções Parser
TBD.

[CHANGELOG]: ./CHANGELOG.md
[version-badge]: https://img.shields.io/badge/version-0.1.0-blue.svg
[coverage-badge]: https://img.shields.io/badge/coverage-97.1-green.svg