docker build -t lexer-basic-nodejs .
docker run -v $(pwd)/lexer/coverage:/app/coverage --rm lexer-basic-nodejs /bin/sh -c "npm run coverage; chown -R $(id -u):$(id -g) ./"
sed -i 's|SF:/app/|SF:lexer/|g' lexer/coverage/lcov.info
sonar-scanner